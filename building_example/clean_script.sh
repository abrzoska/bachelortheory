rm -f Makefile
rm -f *.bak
rm -f *~
rm -f *_output*
rm -f CMakeCache.txt
rm -f mutation_pdb
rm -f cmake_install.cmake
rm -f sequence_file.dat
rm -fr CMakeFiles
cd modelling
rm -f *~
rm -fr modelling.log
rm -fr *_file.*
cd ..
