# ifndef MUTATION_PDB
# define MUTATION_PDB

# include <string>
# include <cstdlib>
# include <fstream>
# include <BALL/FORMAT/PDBFile.h>
# include <BALL/KERNEL/system.h>
# include <BALL/KERNEL/residueIterator.h>
# include <BALL/STRUCTURE/peptides.h>

using namespace BALL;
using namespace std;


class mutation_pdb
{

public:
	mutation_pdb(string pdb_infile, string pdb_outfile, int mutated_proteins, double mutation_rate, double threshold);
	~mutation_pdb();

private:
	int mutated_proteins;
	int residue_number;
	double mutation_rate;
	double threshold;
	string pdb_infile;
	string pdb_outfile;
	string* sequences;
	System* base_pdb;

	void load_pdb(string pdb_infile);
	void save_pdb(string pdb_outfile);
	void generate_mutations();
	void random_mutation(int mutated);
	void double_mutation(int mutated);
	void get_simulated_pdb();
	double random_number();

};

# endif
