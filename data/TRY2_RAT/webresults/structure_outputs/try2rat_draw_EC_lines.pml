center
hide all
color grey80
set cartoon_flat_sheets, 0
set cartoon_smooth_loops, 0
show cartoon
set dash_width, 0
set dash_round_ends, off
dist ec1, name ca and resid 64, name ca and resid 48, label=0
set dash_radius, 0.500, ec1
set dash_gap, 0.075, ec1
set dash_length, 0.925, ec1
color green, ec1
dist ec2, name ca and resid 206, name ca and resid 139, label=0
set dash_radius, 0.333, ec2
set dash_gap, 0.075, ec2
set dash_length, 0.925, ec2
color green, ec2
dist ec3, name ca and resid 220, name ca and resid 196, label=0
set dash_radius, 0.325, ec3
set dash_gap, 0.075, ec3
set dash_length, 0.925, ec3
color green, ec3
dist ec4, name ca and resid 185, name ca and resid 171, label=0
set dash_radius, 0.313, ec4
set dash_gap, 0.075, ec4
set dash_length, 0.925, ec4
color green, ec4
dist ec5, name ca and resid 46, name ca and resid 40, label=0
set dash_radius, 0.256, ec5
set dash_gap, 0.075, ec5
set dash_length, 0.925, ec5
color green, ec5
dist ec6, name ca and resid 200, name ca and resid 63, label=0
set dash_radius, 0.255, ec6
set dash_gap, 0.075, ec6
set dash_length, 0.925, ec6
color green, ec6
dist ec7, name ca and resid 58, name ca and resid 50, label=0
set dash_radius, 0.181, ec7
set dash_gap, 0.075, ec7
set dash_length, 0.925, ec7
color green, ec7
dist ec8, name ca and resid 199, name ca and resid 145, label=0
set dash_radius, 0.143, ec8
set dash_gap, 0.075, ec8
set dash_length, 0.925, ec8
color green, ec8
dist ec9, name ca and resid 109, name ca and resid 65, label=0
set dash_radius, 0.138, ec9
set dash_gap, 0.075, ec9
set dash_length, 0.925, ec9
color green, ec9
dist ec10, name ca and resid 227, name ca and resid 194, label=0
set dash_radius, 0.132, ec10
set dash_gap, 0.075, ec10
set dash_length, 0.925, ec10
color green, ec10
dist ec11, name ca and resid 107, name ca and resid 63, label=0
set dash_radius, 0.119, ec11
set dash_gap, 0.075, ec11
set dash_length, 0.925, ec11
color green, ec11
dist ec12, name ca and resid 127, name ca and resid 37, label=0
set dash_radius, 0.111, ec12
set dash_gap, 0.075, ec12
set dash_length, 0.925, ec12
color green, ec12
dist ec13, name ca and resid 160, name ca and resid 34, label=0
set dash_radius, 0.111, ec13
set dash_gap, 0.075, ec13
set dash_length, 0.925, ec13
color green, ec13
dist ec14, name ca and resid 110, name ca and resid 57, label=0
set dash_radius, 0.103, ec14
set dash_gap, 0.075, ec14
set dash_length, 0.925, ec14
color green, ec14
dist ec15, name ca and resid 117, name ca and resid 52, label=0
set dash_radius, 0.092, ec15
set dash_gap, 0.075, ec15
set dash_length, 0.925, ec15
color green, ec15
dist ec16, name ca and resid 142, name ca and resid 38, label=0
set dash_radius, 0.090, ec16
set dash_gap, 0.075, ec16
set dash_length, 0.925, ec16
color green, ec16
dist filt_ec17, name ca and resid 196, name ca and resid 185, label=0
set dash_radius, 0.05, filt_ec17
set dash_gap, 0.825, filt_ec17
set dash_length, 0.175, filt_ec17
color grey80, filt_ec17
disable filt_ec17
dist ec18, name ca and resid 50, name ca and resid 39, label=0
set dash_radius, 0.075, ec18
set dash_gap, 0.075, ec18
set dash_length, 0.925, ec18
color green, ec18
dist ec19, name ca and resid 187, name ca and resid 164, label=0
set dash_radius, 0.074, ec19
set dash_gap, 0.075, ec19
set dash_length, 0.925, ec19
color green, ec19
dist ec20, name ca and resid 222, name ca and resid 149, label=0
set dash_radius, 0.072, ec20
set dash_gap, 0.075, ec20
set dash_length, 0.925, ec20
color green, ec20
dist ec21, name ca and resid 112, name ca and resid 91, label=0
set dash_radius, 0.070, ec21
set dash_gap, 0.075, ec21
set dash_length, 0.925, ec21
color green, ec21
dist ec22, name ca and resid 112, name ca and resid 57, label=0
set dash_radius, 0.067, ec22
set dash_gap, 0.075, ec22
set dash_length, 0.925, ec22
color green, ec22
dist ec23, name ca and resid 117, name ca and resid 73, label=0
set dash_radius, 0.062, ec23
set dash_gap, 0.075, ec23
set dash_length, 0.925, ec23
color green, ec23
dist ec24, name ca and resid 193, name ca and resid 161, label=0
set dash_radius, 0.061, ec24
set dash_gap, 0.075, ec24
set dash_length, 0.925, ec24
color green, ec24
dist ec25, name ca and resid 182, name ca and resid 105, label=0
set dash_radius, 0.060, ec25
set dash_gap, 0.075, ec25
set dash_length, 0.925, ec25
color green, ec25
dist ec26, name ca and resid 160, name ca and resid 35, label=0
set dash_radius, 0.059, ec26
set dash_gap, 0.075, ec26
set dash_length, 0.925, ec26
color green, ec26
dist ec27, name ca and resid 210, name ca and resid 51, label=0
set dash_radius, 0.054, ec27
set dash_gap, 0.075, ec27
set dash_length, 0.925, ec27
color green, ec27
dist ec28, name ca and resid 159, name ca and resid 29, label=0
set dash_radius, 0.051, ec28
set dash_gap, 0.075, ec28
set dash_length, 0.925, ec28
color green, ec28
dist ec29, name ca and resid 214, name ca and resid 195, label=0
set dash_radius, 0.051, ec29
set dash_gap, 0.075, ec29
set dash_length, 0.925, ec29
color green, ec29
dist ec30, name ca and resid 210, name ca and resid 59, label=0
set dash_radius, 0.051, ec30
set dash_gap, 0.075, ec30
set dash_length, 0.925, ec30
color green, ec30
dist ec31, name ca and resid 58, name ca and resid 52, label=0
set dash_radius, 0.049, ec31
set dash_gap, 0.075, ec31
set dash_length, 0.925, ec31
color green, ec31
dist ec32, name ca and resid 214, name ca and resid 141, label=0
set dash_radius, 0.049, ec32
set dash_gap, 0.075, ec32
set dash_length, 0.925, ec32
color green, ec32
dist ec33, name ca and resid 112, name ca and resid 56, label=0
set dash_radius, 0.047, ec33
set dash_gap, 0.075, ec33
set dash_length, 0.925, ec33
color green, ec33
dist ec34, name ca and resid 215, name ca and resid 199, label=0
set dash_radius, 0.046, ec34
set dash_gap, 0.075, ec34
set dash_length, 0.925, ec34
color green, ec34
dist ec35, name ca and resid 210, name ca and resid 126, label=0
set dash_radius, 0.045, ec35
set dash_gap, 0.075, ec35
set dash_length, 0.925, ec35
color green, ec35
dist ec36, name ca and resid 228, name ca and resid 216, label=0
set dash_radius, 0.045, ec36
set dash_gap, 0.075, ec36
set dash_length, 0.925, ec36
color green, ec36
dist ec37, name ca and resid 200, name ca and resid 107, label=0
set dash_radius, 0.044, ec37
set dash_gap, 0.075, ec37
set dash_length, 0.925, ec37
color green, ec37
dist ec38, name ca and resid 212, name ca and resid 201, label=0
set dash_radius, 0.043, ec38
set dash_gap, 0.075, ec38
set dash_length, 0.925, ec38
color green, ec38
dist ec39, name ca and resid 63, name ca and resid 49, label=0
set dash_radius, 0.042, ec39
set dash_gap, 0.075, ec39
set dash_length, 0.925, ec39
color green, ec39
dist ec40, name ca and resid 123, name ca and resid 86, label=0
set dash_radius, 0.041, ec40
set dash_gap, 0.075, ec40
set dash_length, 0.925, ec40
color green, ec40
dist ec41, name ca and resid 204, name ca and resid 139, label=0
set dash_radius, 0.041, ec41
set dash_gap, 0.075, ec41
set dash_length, 0.925, ec41
color green, ec41
dist ec42, name ca and resid 73, name ca and resid 39, label=0
set dash_radius, 0.040, ec42
set dash_gap, 0.075, ec42
set dash_length, 0.925, ec42
color green, ec42
dist ec43, name ca and resid 229, name ca and resid 194, label=0
set dash_radius, 0.040, ec43
set dash_gap, 0.075, ec43
set dash_length, 0.925, ec43
color green, ec43
dist ec44, name ca and resid 126, name ca and resid 51, label=0
set dash_radius, 0.040, ec44
set dash_gap, 0.075, ec44
set dash_length, 0.925, ec44
color green, ec44
dist ec45, name ca and resid 216, name ca and resid 183, label=0
set dash_radius, 0.040, ec45
set dash_gap, 0.075, ec45
set dash_length, 0.925, ec45
color green, ec45
dist ec46, name ca and resid 161, name ca and resid 141, label=0
set dash_radius, 0.039, ec46
set dash_gap, 0.075, ec46
set dash_length, 0.925, ec46
color green, ec46
dist ec47, name ca and resid 84, name ca and resid 77, label=0
set dash_radius, 0.039, ec47
set dash_gap, 0.075, ec47
set dash_length, 0.925, ec47
color green, ec47
dist ec48, name ca and resid 112, name ca and resid 92, label=0
set dash_radius, 0.039, ec48
set dash_gap, 0.075, ec48
set dash_length, 0.925, ec48
color green, ec48
dist ec49, name ca and resid 125, name ca and resid 119, label=0
set dash_radius, 0.038, ec49
set dash_gap, 0.075, ec49
set dash_length, 0.925, ec49
color green, ec49
dist ec50, name ca and resid 58, name ca and resid 39, label=0
set dash_radius, 0.038, ec50
set dash_gap, 0.075, ec50
set dash_length, 0.925, ec50
color green, ec50
dist ec51, name ca and resid 195, name ca and resid 141, label=0
set dash_radius, 0.038, ec51
set dash_gap, 0.075, ec51
set dash_length, 0.925, ec51
color green, ec51
dist ec52, name ca and resid 111, name ca and resid 90, label=0
set dash_radius, 0.037, ec52
set dash_gap, 0.075, ec52
set dash_length, 0.925, ec52
color green, ec52
dist ec53, name ca and resid 113, name ca and resid 52, label=0
set dash_radius, 0.037, ec53
set dash_gap, 0.075, ec53
set dash_length, 0.925, ec53
color green, ec53
dist ec54, name ca and resid 205, name ca and resid 142, label=0
set dash_radius, 0.036, ec54
set dash_gap, 0.075, ec54
set dash_length, 0.925, ec54
color green, ec54
dist ec55, name ca and resid 235, name ca and resid 108, label=0
set dash_radius, 0.036, ec55
set dash_gap, 0.075, ec55
set dash_length, 0.925, ec55
color green, ec55
dist ec56, name ca and resid 214, name ca and resid 200, label=0
set dash_radius, 0.036, ec56
set dash_gap, 0.075, ec56
set dash_length, 0.925, ec56
color green, ec56
dist ec57, name ca and resid 87, name ca and resid 72, label=0
set dash_radius, 0.036, ec57
set dash_gap, 0.075, ec57
set dash_length, 0.925, ec57
color green, ec57
dist ec58, name ca and resid 217, name ca and resid 200, label=0
set dash_radius, 0.035, ec58
set dash_gap, 0.075, ec58
set dash_length, 0.925, ec58
color green, ec58
dist ec59, name ca and resid 114, name ca and resid 89, label=0
set dash_radius, 0.035, ec59
set dash_gap, 0.075, ec59
set dash_length, 0.925, ec59
color green, ec59
dist ec60, name ca and resid 165, name ca and resid 139, label=0
set dash_radius, 0.035, ec60
set dash_gap, 0.075, ec60
set dash_length, 0.925, ec60
color green, ec60
dist filt_ec61, name ca and resid 216, name ca and resid 210, label=0
set dash_radius, 0.05, filt_ec61
set dash_gap, 0.825, filt_ec61
set dash_length, 0.175, filt_ec61
color grey80, filt_ec61
disable filt_ec61
dist ec62, name ca and resid 217, name ca and resid 194, label=0
set dash_radius, 0.034, ec62
set dash_gap, 0.075, ec62
set dash_length, 0.925, ec62
color green, ec62
dist ec63, name ca and resid 76, name ca and resid 29, label=0
set dash_radius, 0.034, ec63
set dash_gap, 0.075, ec63
set dash_length, 0.925, ec63
color green, ec63
dist ec64, name ca and resid 142, name ca and resid 35, label=0
set dash_radius, 0.034, ec64
set dash_gap, 0.075, ec64
set dash_length, 0.925, ec64
color green, ec64
dist ec65, name ca and resid 204, name ca and resid 184, label=0
set dash_radius, 0.033, ec65
set dash_gap, 0.075, ec65
set dash_length, 0.925, ec65
color green, ec65
dist filt_ec66, name ca and resid 233, name ca and resid 227, label=0
set dash_radius, 0.05, filt_ec66
set dash_gap, 0.825, filt_ec66
set dash_length, 0.175, filt_ec66
color grey80, filt_ec66
disable filt_ec66
dist ec67, name ca and resid 231, name ca and resid 168, label=0
set dash_radius, 0.033, ec67
set dash_gap, 0.075, ec67
set dash_length, 0.925, ec67
color green, ec67
dist ec68, name ca and resid 85, name ca and resid 75, label=0
set dash_radius, 0.033, ec68
set dash_gap, 0.075, ec68
set dash_length, 0.925, ec68
color green, ec68
dist ec69, name ca and resid 180, name ca and resid 105, label=0
set dash_radius, 0.032, ec69
set dash_gap, 0.075, ec69
set dash_length, 0.925, ec69
color green, ec69
dist ec70, name ca and resid 116, name ca and resid 56, label=0
set dash_radius, 0.032, ec70
set dash_gap, 0.075, ec70
set dash_length, 0.925, ec70
color green, ec70
dist ec71, name ca and resid 204, name ca and resid 186, label=0
set dash_radius, 0.032, ec71
set dash_gap, 0.075, ec71
set dash_length, 0.925, ec71
color green, ec71
dist ec72, name ca and resid 239, name ca and resid 232, label=0
set dash_radius, 0.032, ec72
set dash_gap, 0.075, ec72
set dash_length, 0.925, ec72
color green, ec72
dist ec73, name ca and resid 211, name ca and resid 139, label=0
set dash_radius, 0.032, ec73
set dash_gap, 0.075, ec73
set dash_length, 0.925, ec73
color green, ec73
dist ec74, name ca and resid 229, name ca and resid 223, label=0
set dash_radius, 0.031, ec74
set dash_gap, 0.075, ec74
set dash_length, 0.925, ec74
color green, ec74
dist ec75, name ca and resid 49, name ca and resid 38, label=0
set dash_radius, 0.031, ec75
set dash_gap, 0.075, ec75
set dash_length, 0.925, ec75
color green, ec75
dist ec76, name ca and resid 217, name ca and resid 195, label=0
set dash_radius, 0.031, ec76
set dash_gap, 0.075, ec76
set dash_length, 0.925, ec76
color green, ec76
dist ec77, name ca and resid 106, name ca and resid 96, label=0
set dash_radius, 0.031, ec77
set dash_gap, 0.075, ec77
set dash_length, 0.925, ec77
color green, ec77
dist ec78, name ca and resid 215, name ca and resid 200, label=0
set dash_radius, 0.031, ec78
set dash_gap, 0.075, ec78
set dash_length, 0.925, ec78
color green, ec78
dist ec79, name ca and resid 163, name ca and resid 141, label=0
set dash_radius, 0.030, ec79
set dash_gap, 0.075, ec79
set dash_length, 0.925, ec79
color green, ec79
dist ec80, name ca and resid 194, name ca and resid 186, label=0
set dash_radius, 0.030, ec80
set dash_gap, 0.075, ec80
set dash_length, 0.925, ec80
color green, ec80
dist ec81, name ca and resid 61, name ca and resid 49, label=0
set dash_radius, 0.030, ec81
set dash_gap, 0.075, ec81
set dash_length, 0.925, ec81
color green, ec81
dist ec82, name ca and resid 230, name ca and resid 108, label=0
set dash_radius, 0.030, ec82
set dash_gap, 0.075, ec82
set dash_length, 0.925, ec82
color green, ec82
dist ec83, name ca and resid 96, name ca and resid 62, label=0
set dash_radius, 0.030, ec83
set dash_gap, 0.075, ec83
set dash_length, 0.925, ec83
color green, ec83
dist ec84, name ca and resid 226, name ca and resid 220, label=0
set dash_radius, 0.030, ec84
set dash_gap, 0.075, ec84
set dash_length, 0.925, ec84
color green, ec84
dist ec85, name ca and resid 196, name ca and resid 145, label=0
set dash_radius, 0.029, ec85
set dash_gap, 0.075, ec85
set dash_length, 0.925, ec85
color green, ec85
dist ec86, name ca and resid 199, name ca and resid 143, label=0
set dash_radius, 0.029, ec86
set dash_gap, 0.075, ec86
set dash_length, 0.925, ec86
color green, ec86
dist ec87, name ca and resid 221, name ca and resid 194, label=0
set dash_radius, 0.029, ec87
set dash_gap, 0.075, ec87
set dash_length, 0.925, ec87
color green, ec87
dist ec88, name ca and resid 235, name ca and resid 106, label=0
set dash_radius, 0.029, ec88
set dash_gap, 0.075, ec88
set dash_length, 0.925, ec88
color green, ec88
dist ec89, name ca and resid 229, name ca and resid 204, label=0
set dash_radius, 0.028, ec89
set dash_gap, 0.075, ec89
set dash_length, 0.925, ec89
color green, ec89
dist ec90, name ca and resid 73, name ca and resid 52, label=0
set dash_radius, 0.028, ec90
set dash_gap, 0.075, ec90
set dash_length, 0.925, ec90
color green, ec90
disable ec90
dist ec91, name ca and resid 155, name ca and resid 147, label=0
set dash_radius, 0.028, ec91
set dash_gap, 0.075, ec91
set dash_length, 0.925, ec91
color green, ec91
disable ec91
dist filt_ec92, name ca and resid 45, name ca and resid 39, label=0
set dash_radius, 0.05, filt_ec92
set dash_gap, 0.825, filt_ec92
set dash_length, 0.175, filt_ec92
color grey80, filt_ec92
disable filt_ec92
dist ec93, name ca and resid 158, name ca and resid 38, label=0
set dash_radius, 0.028, ec93
set dash_gap, 0.075, ec93
set dash_length, 0.925, ec93
color green, ec93
disable ec93
dist ec94, name ca and resid 117, name ca and resid 88, label=0
set dash_radius, 0.028, ec94
set dash_gap, 0.075, ec94
set dash_length, 0.925, ec94
color green, ec94
disable ec94
dist ec95, name ca and resid 160, name ca and resid 142, label=0
set dash_radius, 0.027, ec95
set dash_gap, 0.075, ec95
set dash_length, 0.925, ec95
color green, ec95
disable ec95
dist ec96, name ca and resid 83, name ca and resid 76, label=0
set dash_radius, 0.027, ec96
set dash_gap, 0.075, ec96
set dash_length, 0.925, ec96
color green, ec96
disable ec96
dist ec97, name ca and resid 160, name ca and resid 30, label=0
set dash_radius, 0.027, ec97
set dash_gap, 0.075, ec97
set dash_length, 0.925, ec97
color green, ec97
disable ec97
dist ec98, name ca and resid 230, name ca and resid 215, label=0
set dash_radius, 0.027, ec98
set dash_gap, 0.075, ec98
set dash_length, 0.925, ec98
color green, ec98
disable ec98
dist ec99, name ca and resid 234, name ca and resid 182, label=0
set dash_radius, 0.027, ec99
set dash_gap, 0.075, ec99
set dash_length, 0.925, ec99
color green, ec99
disable ec99
dist ec100, name ca and resid 188, name ca and resid 164, label=0
set dash_radius, 0.026, ec100
set dash_gap, 0.075, ec100
set dash_length, 0.925, ec100
color green, ec100
disable ec100
dist ec101, name ca and resid 171, name ca and resid 144, label=0
set dash_radius, 0.026, ec101
set dash_gap, 0.075, ec101
set dash_length, 0.925, ec101
color green, ec101
disable ec101
dist ec102, name ca and resid 140, name ca and resid 35, label=0
set dash_radius, 0.026, ec102
set dash_gap, 0.075, ec102
set dash_length, 0.925, ec102
color green, ec102
disable ec102
dist ec103, name ca and resid 66, name ca and resid 60, label=0
set dash_radius, 0.026, ec103
set dash_gap, 0.075, ec103
set dash_length, 0.925, ec103
color green, ec103
disable ec103
dist ec104, name ca and resid 227, name ca and resid 221, label=0
set dash_radius, 0.026, ec104
set dash_gap, 0.075, ec104
set dash_length, 0.925, ec104
color green, ec104
disable ec104
dist ec105, name ca and resid 109, name ca and resid 95, label=0
set dash_radius, 0.026, ec105
set dash_gap, 0.075, ec105
set dash_length, 0.925, ec105
color green, ec105
disable ec105
dist ec106, name ca and resid 205, name ca and resid 37, label=0
set dash_radius, 0.026, ec106
set dash_gap, 0.075, ec106
set dash_length, 0.925, ec106
color green, ec106
disable ec106
dist ec107, name ca and resid 59, name ca and resid 53, label=0
set dash_radius, 0.025, ec107
set dash_gap, 0.075, ec107
set dash_length, 0.925, ec107
color green, ec107
disable ec107
dist ec108, name ca and resid 67, name ca and resid 61, label=0
set dash_radius, 0.025, ec108
set dash_gap, 0.075, ec108
set dash_length, 0.925, ec108
color green, ec108
disable ec108
dist ec109, name ca and resid 119, name ca and resid 52, label=0
set dash_radius, 0.025, ec109
set dash_gap, 0.075, ec109
set dash_length, 0.925, ec109
color green, ec109
disable ec109
dist ec110, name ca and resid 171, name ca and resid 158, label=0
set dash_radius, 0.025, ec110
set dash_gap, 0.075, ec110
set dash_length, 0.925, ec110
color green, ec110
disable ec110
dist ec111, name ca and resid 109, name ca and resid 93, label=0
set dash_radius, 0.025, ec111
set dash_gap, 0.075, ec111
set dash_length, 0.925, ec111
color green, ec111
disable ec111
dist ec112, name ca and resid 201, name ca and resid 143, label=0
set dash_radius, 0.025, ec112
set dash_gap, 0.075, ec112
set dash_length, 0.925, ec112
color green, ec112
disable ec112
dist ec113, name ca and resid 123, name ca and resid 73, label=0
set dash_radius, 0.024, ec113
set dash_gap, 0.075, ec113
set dash_length, 0.925, ec113
color green, ec113
disable ec113
dist ec114, name ca and resid 215, name ca and resid 107, label=0
set dash_radius, 0.024, ec114
set dash_gap, 0.075, ec114
set dash_length, 0.925, ec114
color green, ec114
disable ec114
dist filt_ec115, name ca and resid 220, name ca and resid 185, label=0
set dash_radius, 0.05, filt_ec115
set dash_gap, 0.825, filt_ec115
set dash_length, 0.175, filt_ec115
color grey80, filt_ec115
disable filt_ec115
dist ec116, name ca and resid 185, name ca and resid 62, label=0
set dash_radius, 0.024, ec116
set dash_gap, 0.075, ec116
set dash_length, 0.925, ec116
color green, ec116
disable ec116
dist ec117, name ca and resid 229, name ca and resid 220, label=0
set dash_radius, 0.024, ec117
set dash_gap, 0.075, ec117
set dash_length, 0.925, ec117
color green, ec117
disable ec117
dist ec118, name ca and resid 114, name ca and resid 88, label=0
set dash_radius, 0.024, ec118
set dash_gap, 0.075, ec118
set dash_length, 0.925, ec118
color green, ec118
disable ec118
dist ec119, name ca and resid 67, name ca and resid 60, label=0
set dash_radius, 0.023, ec119
set dash_gap, 0.075, ec119
set dash_length, 0.925, ec119
color green, ec119
disable ec119
dist ec120, name ca and resid 228, name ca and resid 217, label=0
set dash_radius, 0.023, ec120
set dash_gap, 0.075, ec120
set dash_length, 0.925, ec120
color green, ec120
disable ec120
dist ec121, name ca and resid 116, name ca and resid 55, label=0
set dash_radius, 0.023, ec121
set dash_gap, 0.075, ec121
set dash_length, 0.925, ec121
color green, ec121
disable ec121
dist ec122, name ca and resid 57, name ca and resid 38, label=0
set dash_radius, 0.023, ec122
set dash_gap, 0.075, ec122
set dash_length, 0.925, ec122
color green, ec122
disable ec122
dist filt_ec123, name ca and resid 217, name ca and resid 210, label=0
set dash_radius, 0.05, filt_ec123
set dash_gap, 0.825, filt_ec123
set dash_length, 0.175, filt_ec123
color grey80, filt_ec123
disable filt_ec123
dist ec124, name ca and resid 76, name ca and resid 33, label=0
set dash_radius, 0.023, ec124
set dash_gap, 0.075, ec124
set dash_length, 0.925, ec124
color green, ec124
disable ec124
dist ec125, name ca and resid 201, name ca and resid 60, label=0
set dash_radius, 0.023, ec125
set dash_gap, 0.075, ec125
set dash_length, 0.925, ec125
color green, ec125
disable ec125
dist ec126, name ca and resid 114, name ca and resid 91, label=0
set dash_radius, 0.023, ec126
set dash_gap, 0.075, ec126
set dash_length, 0.925, ec126
color green, ec126
disable ec126
dist ec127, name ca and resid 183, name ca and resid 105, label=0
set dash_radius, 0.023, ec127
set dash_gap, 0.075, ec127
set dash_length, 0.925, ec127
color green, ec127
disable ec127
dist filt_ec128, name ca and resid 54, name ca and resid 48, label=0
set dash_radius, 0.05, filt_ec128
set dash_gap, 0.825, filt_ec128
set dash_length, 0.175, filt_ec128
color grey80, filt_ec128
disable filt_ec128
dist ec129, name ca and resid 198, name ca and resid 63, label=0
set dash_radius, 0.022, ec129
set dash_gap, 0.075, ec129
set dash_length, 0.925, ec129
color green, ec129
disable ec129
dist ec130, name ca and resid 227, name ca and resid 219, label=0
set dash_radius, 0.022, ec130
set dash_gap, 0.075, ec130
set dash_length, 0.925, ec130
color green, ec130
disable ec130
dist ec131, name ca and resid 127, name ca and resid 35, label=0
set dash_radius, 0.022, ec131
set dash_gap, 0.075, ec131
set dash_length, 0.925, ec131
color green, ec131
disable ec131
dist ec132, name ca and resid 226, name ca and resid 166, label=0
set dash_radius, 0.022, ec132
set dash_gap, 0.075, ec132
set dash_length, 0.925, ec132
color green, ec132
disable ec132
dist ec133, name ca and resid 182, name ca and resid 106, label=0
set dash_radius, 0.022, ec133
set dash_gap, 0.075, ec133
set dash_length, 0.925, ec133
color green, ec133
disable ec133
dist ec134, name ca and resid 179, name ca and resid 168, label=0
set dash_radius, 0.021, ec134
set dash_gap, 0.075, ec134
set dash_length, 0.925, ec134
color green, ec134
disable ec134
dist filt_ec135, name ca and resid 43, name ca and resid 37, label=0
set dash_radius, 0.05, filt_ec135
set dash_gap, 0.825, filt_ec135
set dash_length, 0.175, filt_ec135
color grey80, filt_ec135
disable filt_ec135
dist ec136, name ca and resid 205, name ca and resid 126, label=0
set dash_radius, 0.021, ec136
set dash_gap, 0.075, ec136
set dash_length, 0.925, ec136
color green, ec136
disable ec136
dist ec137, name ca and resid 211, name ca and resid 129, label=0
set dash_radius, 0.021, ec137
set dash_gap, 0.075, ec137
set dash_length, 0.925, ec137
color green, ec137
disable ec137
dist filt_ec138, name ca and resid 217, name ca and resid 211, label=0
set dash_radius, 0.05, filt_ec138
set dash_gap, 0.825, filt_ec138
set dash_length, 0.175, filt_ec138
color grey80, filt_ec138
disable filt_ec138
dist ec139, name ca and resid 235, name ca and resid 182, label=0
set dash_radius, 0.021, ec139
set dash_gap, 0.075, ec139
set dash_length, 0.925, ec139
color green, ec139
disable ec139
dist ec140, name ca and resid 60, name ca and resid 50, label=0
set dash_radius, 0.021, ec140
set dash_gap, 0.075, ec140
set dash_length, 0.925, ec140
color green, ec140
disable ec140
dist filt_ec141, name ca and resid 196, name ca and resid 171, label=0
set dash_radius, 0.05, filt_ec141
set dash_gap, 0.825, filt_ec141
set dash_length, 0.175, filt_ec141
color grey80, filt_ec141
disable filt_ec141
dist ec142, name ca and resid 229, name ca and resid 186, label=0
set dash_radius, 0.020, ec142
set dash_gap, 0.075, ec142
set dash_length, 0.925, ec142
color green, ec142
disable ec142
dist ec143, name ca and resid 229, name ca and resid 163, label=0
set dash_radius, 0.020, ec143
set dash_gap, 0.075, ec143
set dash_length, 0.925, ec143
color green, ec143
disable ec143
dist ec144, name ca and resid 198, name ca and resid 61, label=0
set dash_radius, 0.020, ec144
set dash_gap, 0.075, ec144
set dash_length, 0.925, ec144
color green, ec144
disable ec144
dist filt_ec145, name ca and resid 44, name ca and resid 38, label=0
set dash_radius, 0.05, filt_ec145
set dash_gap, 0.825, filt_ec145
set dash_length, 0.175, filt_ec145
color grey80, filt_ec145
disable filt_ec145
dist ec146, name ca and resid 213, name ca and resid 51, label=0
set dash_radius, 0.020, ec146
set dash_gap, 0.075, ec146
set dash_length, 0.925, ec146
color green, ec146
disable ec146
dist ec147, name ca and resid 229, name ca and resid 212, label=0
set dash_radius, 0.019, ec147
set dash_gap, 0.075, ec147
set dash_length, 0.925, ec147
color green, ec147
disable ec147
dist ec148, name ca and resid 145, name ca and resid 49, label=0
set dash_radius, 0.019, ec148
set dash_gap, 0.075, ec148
set dash_length, 0.925, ec148
color green, ec148
disable ec148
dist ec149, name ca and resid 106, name ca and resid 98, label=0
set dash_radius, 0.019, ec149
set dash_gap, 0.075, ec149
set dash_length, 0.925, ec149
color green, ec149
disable ec149
dist ec150, name ca and resid 85, name ca and resid 76, label=0
set dash_radius, 0.019, ec150
set dash_gap, 0.075, ec150
set dash_length, 0.925, ec150
color green, ec150
disable ec150
dist ec151, name ca and resid 236, name ca and resid 129, label=0
set dash_radius, 0.019, ec151
set dash_gap, 0.075, ec151
set dash_length, 0.925, ec151
color green, ec151
disable ec151
dist ec152, name ca and resid 123, name ca and resid 74, label=0
set dash_radius, 0.019, ec152
set dash_gap, 0.075, ec152
set dash_length, 0.925, ec152
color green, ec152
disable ec152
dist ec153, name ca and resid 238, name ca and resid 110, label=0
set dash_radius, 0.019, ec153
set dash_gap, 0.075, ec153
set dash_length, 0.925, ec153
color green, ec153
disable ec153
dist ec154, name ca and resid 213, name ca and resid 61, label=0
set dash_radius, 0.019, ec154
set dash_gap, 0.075, ec154
set dash_length, 0.925, ec154
color green, ec154
disable ec154
dist ec155, name ca and resid 117, name ca and resid 86, label=0
set dash_radius, 0.019, ec155
set dash_gap, 0.075, ec155
set dash_length, 0.925, ec155
color green, ec155
disable ec155
dist filt_ec156, name ca and resid 143, name ca and resid 136, label=0
set dash_radius, 0.05, filt_ec156
set dash_gap, 0.825, filt_ec156
set dash_length, 0.175, filt_ec156
color grey80, filt_ec156
disable filt_ec156
dist ec157, name ca and resid 229, name ca and resid 221, label=0
set dash_radius, 0.019, ec157
set dash_gap, 0.075, ec157
set dash_length, 0.925, ec157
color green, ec157
disable ec157
dist ec158, name ca and resid 158, name ca and resid 144, label=0
set dash_radius, 0.019, ec158
set dash_gap, 0.075, ec158
set dash_length, 0.925, ec158
color green, ec158
disable ec158
dist ec159, name ca and resid 93, name ca and resid 65, label=0
set dash_radius, 0.019, ec159
set dash_gap, 0.075, ec159
set dash_length, 0.925, ec159
color green, ec159
disable ec159
dist ec160, name ca and resid 201, name ca and resid 27, label=0
set dash_radius, 0.019, ec160
set dash_gap, 0.075, ec160
set dash_length, 0.925, ec160
color green, ec160
disable ec160
dist filt_ec161, name ca and resid 64, name ca and resid 58, label=0
set dash_radius, 0.05, filt_ec161
set dash_gap, 0.825, filt_ec161
set dash_length, 0.175, filt_ec161
color grey80, filt_ec161
disable filt_ec161
dist ec162, name ca and resid 94, name ca and resid 57, label=0
set dash_radius, 0.019, ec162
set dash_gap, 0.075, ec162
set dash_length, 0.925, ec162
color green, ec162
disable ec162
dist ec163, name ca and resid 230, name ca and resid 213, label=0
set dash_radius, 0.019, ec163
set dash_gap, 0.075, ec163
set dash_length, 0.925, ec163
color green, ec163
disable ec163
dist ec164, name ca and resid 99, name ca and resid 62, label=0
set dash_radius, 0.018, ec164
set dash_gap, 0.075, ec164
set dash_length, 0.925, ec164
color green, ec164
disable ec164
dist ec165, name ca and resid 116, name ca and resid 88, label=0
set dash_radius, 0.018, ec165
set dash_gap, 0.075, ec165
set dash_length, 0.925, ec165
color green, ec165
disable ec165
dist ec166, name ca and resid 194, name ca and resid 183, label=0
set dash_radius, 0.018, ec166
set dash_gap, 0.075, ec166
set dash_length, 0.925, ec166
color green, ec166
disable ec166
dist ec167, name ca and resid 129, name ca and resid 37, label=0
set dash_radius, 0.018, ec167
set dash_gap, 0.075, ec167
set dash_length, 0.925, ec167
color green, ec167
disable ec167
dist ec168, name ca and resid 230, name ca and resid 223, label=0
set dash_radius, 0.018, ec168
set dash_gap, 0.075, ec168
set dash_length, 0.925, ec168
color green, ec168
disable ec168
dist ec169, name ca and resid 226, name ca and resid 204, label=0
set dash_radius, 0.018, ec169
set dash_gap, 0.075, ec169
set dash_length, 0.925, ec169
color green, ec169
disable ec169
dist ec170, name ca and resid 164, name ca and resid 138, label=0
set dash_radius, 0.018, ec170
set dash_gap, 0.075, ec170
set dash_length, 0.925, ec170
color green, ec170
disable ec170
dist ec171, name ca and resid 76, name ca and resid 32, label=0
set dash_radius, 0.018, ec171
set dash_gap, 0.075, ec171
set dash_length, 0.925, ec171
color green, ec171
disable ec171
dist ec172, name ca and resid 162, name ca and resid 140, label=0
set dash_radius, 0.018, ec172
set dash_gap, 0.075, ec172
set dash_length, 0.925, ec172
color green, ec172
disable ec172
dist filt_ec173, name ca and resid 185, name ca and resid 64, label=0
set dash_radius, 0.05, filt_ec173
set dash_gap, 0.825, filt_ec173
set dash_length, 0.175, filt_ec173
color grey80, filt_ec173
disable filt_ec173
dist ec174, name ca and resid 228, name ca and resid 183, label=0
set dash_radius, 0.018, ec174
set dash_gap, 0.075, ec174
set dash_length, 0.925, ec174
color green, ec174
disable ec174
dist ec175, name ca and resid 227, name ca and resid 200, label=0
set dash_radius, 0.018, ec175
set dash_gap, 0.075, ec175
set dash_length, 0.925, ec175
color green, ec175
disable ec175
dist ec176, name ca and resid 214, name ca and resid 202, label=0
set dash_radius, 0.018, ec176
set dash_gap, 0.075, ec176
set dash_length, 0.925, ec176
color green, ec176
disable ec176
dist ec177, name ca and resid 160, name ca and resid 109, label=0
set dash_radius, 0.018, ec177
set dash_gap, 0.075, ec177
set dash_length, 0.925, ec177
color green, ec177
disable ec177
dist ec178, name ca and resid 198, name ca and resid 143, label=0
set dash_radius, 0.018, ec178
set dash_gap, 0.075, ec178
set dash_length, 0.925, ec178
color green, ec178
disable ec178
dist ec179, name ca and resid 227, name ca and resid 217, label=0
set dash_radius, 0.018, ec179
set dash_gap, 0.075, ec179
set dash_length, 0.925, ec179
color green, ec179
disable ec179
dist ec180, name ca and resid 111, name ca and resid 65, label=0
set dash_radius, 0.017, ec180
set dash_gap, 0.075, ec180
set dash_length, 0.925, ec180
color green, ec180
disable ec180
dist ec181, name ca and resid 201, name ca and resid 24, label=0
set dash_radius, 0.017, ec181
set dash_gap, 0.075, ec181
set dash_length, 0.925, ec181
color green, ec181
disable ec181
dist ec182, name ca and resid 95, name ca and resid 65, label=0
set dash_radius, 0.017, ec182
set dash_gap, 0.075, ec182
set dash_length, 0.925, ec182
color green, ec182
disable ec182
dist ec183, name ca and resid 203, name ca and resid 38, label=0
set dash_radius, 0.017, ec183
set dash_gap, 0.075, ec183
set dash_length, 0.925, ec183
color green, ec183
disable ec183
dist ec184, name ca and resid 72, name ca and resid 42, label=0
set dash_radius, 0.017, ec184
set dash_gap, 0.075, ec184
set dash_length, 0.925, ec184
color green, ec184
disable ec184
dist ec185, name ca and resid 212, name ca and resid 206, label=0
set dash_radius, 0.017, ec185
set dash_gap, 0.075, ec185
set dash_length, 0.925, ec185
color green, ec185
disable ec185
dist ec186, name ca and resid 228, name ca and resid 222, label=0
set dash_radius, 0.017, ec186
set dash_gap, 0.075, ec186
set dash_length, 0.925, ec186
color green, ec186
disable ec186
dist ec187, name ca and resid 167, name ca and resid 135, label=0
set dash_radius, 0.017, ec187
set dash_gap, 0.075, ec187
set dash_length, 0.925, ec187
color green, ec187
disable ec187
dist filt_ec188, name ca and resid 45, name ca and resid 38, label=0
set dash_radius, 0.05, filt_ec188
set dash_gap, 0.825, filt_ec188
set dash_length, 0.175, filt_ec188
color grey80, filt_ec188
disable filt_ec188
dist ec189, name ca and resid 144, name ca and resid 49, label=0
set dash_radius, 0.017, ec189
set dash_gap, 0.075, ec189
set dash_length, 0.925, ec189
color green, ec189
disable ec189
dist ec190, name ca and resid 112, name ca and resid 94, label=0
set dash_radius, 0.017, ec190
set dash_gap, 0.075, ec190
set dash_length, 0.925, ec190
color green, ec190
disable ec190
dist ec191, name ca and resid 194, name ca and resid 127, label=0
set dash_radius, 0.017, ec191
set dash_gap, 0.075, ec191
set dash_length, 0.925, ec191
color green, ec191
disable ec191
dist ec192, name ca and resid 144, name ca and resid 38, label=0
set dash_radius, 0.017, ec192
set dash_gap, 0.075, ec192
set dash_length, 0.925, ec192
color green, ec192
disable ec192
dist ec193, name ca and resid 198, name ca and resid 145, label=0
set dash_radius, 0.017, ec193
set dash_gap, 0.075, ec193
set dash_length, 0.925, ec193
color green, ec193
disable ec193
dist ec194, name ca and resid 203, name ca and resid 64, label=0
set dash_radius, 0.017, ec194
set dash_gap, 0.075, ec194
set dash_length, 0.925, ec194
color green, ec194
disable ec194
dist ec195, name ca and resid 84, name ca and resid 75, label=0
set dash_radius, 0.017, ec195
set dash_gap, 0.075, ec195
set dash_length, 0.925, ec195
color green, ec195
disable ec195
dist ec196, name ca and resid 212, name ca and resid 27, label=0
set dash_radius, 0.017, ec196
set dash_gap, 0.075, ec196
set dash_length, 0.925, ec196
color green, ec196
disable ec196
dist ec197, name ca and resid 212, name ca and resid 204, label=0
set dash_radius, 0.016, ec197
set dash_gap, 0.075, ec197
set dash_length, 0.925, ec197
color green, ec197
disable ec197
dist ec198, name ca and resid 227, name ca and resid 186, label=0
set dash_radius, 0.016, ec198
set dash_gap, 0.075, ec198
set dash_length, 0.925, ec198
color green, ec198
disable ec198
dist ec199, name ca and resid 215, name ca and resid 61, label=0
set dash_radius, 0.016, ec199
set dash_gap, 0.075, ec199
set dash_length, 0.925, ec199
color green, ec199
disable ec199
dist ec200, name ca and resid 123, name ca and resid 117, label=0
set dash_radius, 0.016, ec200
set dash_gap, 0.075, ec200
set dash_length, 0.925, ec200
color green, ec200
disable ec200
dist ec201, name ca and resid 127, name ca and resid 117, label=0
set dash_radius, 0.016, ec201
set dash_gap, 0.075, ec201
set dash_length, 0.925, ec201
color green, ec201
disable ec201
dist ec202, name ca and resid 195, name ca and resid 185, label=0
set dash_radius, 0.016, ec202
set dash_gap, 0.075, ec202
set dash_length, 0.925, ec202
color green, ec202
disable ec202
dist ec203, name ca and resid 115, name ca and resid 88, label=0
set dash_radius, 0.016, ec203
set dash_gap, 0.075, ec203
set dash_length, 0.925, ec203
color green, ec203
disable ec203
dist ec204, name ca and resid 202, name ca and resid 143, label=0
set dash_radius, 0.016, ec204
set dash_gap, 0.075, ec204
set dash_length, 0.925, ec204
color green, ec204
disable ec204
dist ec205, name ca and resid 157, name ca and resid 29, label=0
set dash_radius, 0.016, ec205
set dash_gap, 0.075, ec205
set dash_length, 0.925, ec205
color green, ec205
disable ec205
dist ec206, name ca and resid 204, name ca and resid 198, label=0
set dash_radius, 0.016, ec206
set dash_gap, 0.075, ec206
set dash_length, 0.925, ec206
color green, ec206
disable ec206
dist ec207, name ca and resid 212, name ca and resid 185, label=0
set dash_radius, 0.016, ec207
set dash_gap, 0.075, ec207
set dash_length, 0.925, ec207
color green, ec207
disable ec207
dist filt_ec208, name ca and resid 55, name ca and resid 48, label=0
set dash_radius, 0.05, filt_ec208
set dash_gap, 0.825, filt_ec208
set dash_length, 0.175, filt_ec208
color grey80, filt_ec208
disable filt_ec208
dist ec209, name ca and resid 211, name ca and resid 203, label=0
set dash_radius, 0.016, ec209
set dash_gap, 0.075, ec209
set dash_length, 0.925, ec209
color green, ec209
disable ec209
dist ec210, name ca and resid 238, name ca and resid 201, label=0
set dash_radius, 0.016, ec210
set dash_gap, 0.075, ec210
set dash_length, 0.925, ec210
color green, ec210
disable ec210
dist ec211, name ca and resid 57, name ca and resid 35, label=0
set dash_radius, 0.016, ec211
set dash_gap, 0.075, ec211
set dash_length, 0.925, ec211
color green, ec211
disable ec211
dist ec212, name ca and resid 109, name ca and resid 60, label=0
set dash_radius, 0.016, ec212
set dash_gap, 0.075, ec212
set dash_length, 0.925, ec212
color green, ec212
disable ec212
dist filt_ec213, name ca and resid 215, name ca and resid 209, label=0
set dash_radius, 0.05, filt_ec213
set dash_gap, 0.825, filt_ec213
set dash_length, 0.175, filt_ec213
color grey80, filt_ec213
disable filt_ec213
dist ec214, name ca and resid 223, name ca and resid 191, label=0
set dash_radius, 0.015, ec214
set dash_gap, 0.075, ec214
set dash_length, 0.925, ec214
color green, ec214
disable ec214
dist ec215, name ca and resid 161, name ca and resid 24, label=0
set dash_radius, 0.015, ec215
set dash_gap, 0.075, ec215
set dash_length, 0.925, ec215
color green, ec215
disable ec215
dist ec216, name ca and resid 197, name ca and resid 146, label=0
set dash_radius, 0.015, ec216
set dash_gap, 0.075, ec216
set dash_length, 0.925, ec216
color green, ec216
disable ec216
dist filt_ec217, name ca and resid 220, name ca and resid 171, label=0
set dash_radius, 0.05, filt_ec217
set dash_gap, 0.825, filt_ec217
set dash_length, 0.175, filt_ec217
color grey80, filt_ec217
disable filt_ec217
dist ec218, name ca and resid 71, name ca and resid 58, label=0
set dash_radius, 0.015, ec218
set dash_gap, 0.075, ec218
set dash_length, 0.925, ec218
color green, ec218
disable ec218
dist ec219, name ca and resid 87, name ca and resid 75, label=0
set dash_radius, 0.015, ec219
set dash_gap, 0.075, ec219
set dash_length, 0.925, ec219
color green, ec219
disable ec219
dist ec220, name ca and resid 150, name ca and resid 140, label=0
set dash_radius, 0.015, ec220
set dash_gap, 0.075, ec220
set dash_length, 0.925, ec220
color green, ec220
disable ec220
dist ec221, name ca and resid 154, name ca and resid 145, label=0
set dash_radius, 0.015, ec221
set dash_gap, 0.075, ec221
set dash_length, 0.925, ec221
color green, ec221
disable ec221
dist ec222, name ca and resid 236, name ca and resid 128, label=0
set dash_radius, 0.015, ec222
set dash_gap, 0.075, ec222
set dash_length, 0.925, ec222
color green, ec222
disable ec222
dist ec223, name ca and resid 165, name ca and resid 136, label=0
set dash_radius, 0.015, ec223
set dash_gap, 0.075, ec223
set dash_length, 0.925, ec223
color green, ec223
disable ec223
dist ec224, name ca and resid 113, name ca and resid 88, label=0
set dash_radius, 0.015, ec224
set dash_gap, 0.075, ec224
set dash_length, 0.925, ec224
color green, ec224
disable ec224
dist ec225, name ca and resid 188, name ca and resid 182, label=0
set dash_radius, 0.015, ec225
set dash_gap, 0.075, ec225
set dash_length, 0.925, ec225
color green, ec225
disable ec225
dist ec226, name ca and resid 220, name ca and resid 212, label=0
set dash_radius, 0.015, ec226
set dash_gap, 0.075, ec226
set dash_length, 0.925, ec226
color green, ec226
disable ec226
dist ec227, name ca and resid 127, name ca and resid 109, label=0
set dash_radius, 0.015, ec227
set dash_gap, 0.075, ec227
set dash_length, 0.925, ec227
color green, ec227
disable ec227
dist ec228, name ca and resid 201, name ca and resid 145, label=0
set dash_radius, 0.015, ec228
set dash_gap, 0.075, ec228
set dash_length, 0.925, ec228
color green, ec228
disable ec228
dist ec229, name ca and resid 225, name ca and resid 218, label=0
set dash_radius, 0.015, ec229
set dash_gap, 0.075, ec229
set dash_length, 0.925, ec229
color green, ec229
disable ec229
dist ec230, name ca and resid 140, name ca and resid 127, label=0
set dash_radius, 0.015, ec230
set dash_gap, 0.075, ec230
set dash_length, 0.925, ec230
color green, ec230
disable ec230
dist ec231, name ca and resid 120, name ca and resid 86, label=0
set dash_radius, 0.015, ec231
set dash_gap, 0.075, ec231
set dash_length, 0.925, ec231
color green, ec231
disable ec231
dist ec232, name ca and resid 139, name ca and resid 109, label=0
set dash_radius, 0.015, ec232
set dash_gap, 0.075, ec232
set dash_length, 0.925, ec232
color green, ec232
disable ec232
dist ec233, name ca and resid 85, name ca and resid 78, label=0
set dash_radius, 0.015, ec233
set dash_gap, 0.075, ec233
set dash_length, 0.925, ec233
color green, ec233
disable ec233
dist filt_ec234, name ca and resid 55, name ca and resid 49, label=0
set dash_radius, 0.05, filt_ec234
set dash_gap, 0.825, filt_ec234
set dash_length, 0.175, filt_ec234
color grey80, filt_ec234
disable filt_ec234
dist ec235, name ca and resid 65, name ca and resid 59, label=0
set dash_radius, 0.015, ec235
set dash_gap, 0.075, ec235
set dash_length, 0.925, ec235
color green, ec235
disable ec235
hide lines
recolor
