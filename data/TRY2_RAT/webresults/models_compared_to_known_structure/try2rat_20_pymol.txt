Alignment of try2rat_20_1_hMIN to try2rat_pdb3tgi:     Initial RMS: 22.449 for 216 atoms     Final RMS: 18.883 for 198 atoms after 5 cycles
Alignment of try2rat_20_2_hMIN to try2rat_pdb3tgi:     Initial RMS: 22.852 for 216 atoms     Final RMS: 19.366 for 198 atoms after 5 cycles
Alignment of try2rat_20_3_hMIN to try2rat_pdb3tgi:     Initial RMS: 22.532 for 216 atoms     Final RMS: 19.472 for 200 atoms after 5 cycles
Alignment of try2rat_20_4_hMIN to try2rat_pdb3tgi:     Initial RMS: 22.458 for 216 atoms     Final RMS: 19.476 for 200 atoms after 5 cycles
Alignment of try2rat_20_5_hMIN to try2rat_pdb3tgi:     Initial RMS: 22.659 for 216 atoms     Final RMS: 18.742 for 196 atoms after 5 cycles
Alignment of try2rat_20_2_hMIN to try2rat_20_1_hMIN:     Initial RMS:  0.000 for 0 atoms     Final RMS:  0.000 for 0 atoms after 0 cycles
Alignment of try2rat_20_3_hMIN to try2rat_20_1_hMIN:     Initial RMS:  0.000 for 0 atoms     Final RMS:  0.000 for 0 atoms after 0 cycles
Alignment of try2rat_20_4_hMIN to try2rat_20_1_hMIN:     Initial RMS:  0.000 for 0 atoms     Final RMS:  0.000 for 0 atoms after 0 cycles
Alignment of try2rat_20_5_hMIN to try2rat_20_1_hMIN:     Initial RMS:  0.000 for 0 atoms     Final RMS:  0.000 for 0 atoms after 0 cycles
Alignment of try2rat_20_3_hMIN to try2rat_20_2_hMIN:     Initial RMS:  0.000 for 0 atoms     Final RMS:  0.000 for 0 atoms after 0 cycles
Alignment of try2rat_20_4_hMIN to try2rat_20_2_hMIN:     Initial RMS:  0.000 for 0 atoms     Final RMS:  0.000 for 0 atoms after 0 cycles
Alignment of try2rat_20_5_hMIN to try2rat_20_2_hMIN:     Initial RMS:  0.000 for 0 atoms     Final RMS:  0.000 for 0 atoms after 0 cycles
Alignment of try2rat_20_4_hMIN to try2rat_20_3_hMIN:     Initial RMS:  0.000 for 0 atoms     Final RMS:  0.000 for 0 atoms after 0 cycles
Alignment of try2rat_20_5_hMIN to try2rat_20_3_hMIN:     Initial RMS:  0.000 for 0 atoms     Final RMS:  0.000 for 0 atoms after 0 cycles
Alignment of try2rat_20_5_hMIN to try2rat_20_4_hMIN:     Initial RMS:  0.000 for 0 atoms     Final RMS:  0.000 for 0 atoms after 0 cycles
     
try2rat_20_1_hMIN try2rat_20_2_hMIN try2rat_20_3_hMIN try2rat_20_4_hMIN try2rat_20_5_hMIN 
try2rat_pdb3tgi 18.883 19.366 19.472 19.476 18.742 
try2rat_20_1_hMIN         0.000  0.000  0.000  0.000 
try2rat_20_2_hMIN                0.000  0.000  0.000 
try2rat_20_3_hMIN                       0.000  0.000 
try2rat_20_4_hMIN                              0.000 
try2rat_20_5_hMIN                                    
