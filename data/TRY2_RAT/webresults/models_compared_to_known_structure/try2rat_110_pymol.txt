Alignment of try2rat_110_1_hMIN to try2rat_pdb3tgi:     Initial RMS: 15.822 for 216 atoms     Final RMS: 12.935 for 198 atoms after 5 cycles
Alignment of try2rat_110_2_hMIN to try2rat_pdb3tgi:     Initial RMS: 15.876 for 216 atoms     Final RMS: 13.116 for 198 atoms after 5 cycles
Alignment of try2rat_110_3_hMIN to try2rat_pdb3tgi:     Initial RMS: 15.339 for 216 atoms     Final RMS: 12.423 for 197 atoms after 5 cycles
Alignment of try2rat_110_4_hMIN to try2rat_pdb3tgi:     Initial RMS:  7.846 for 216 atoms     Final RMS:  5.279 for 182 atoms after 5 cycles
Alignment of try2rat_110_5_hMIN to try2rat_pdb3tgi:     Initial RMS: 15.520 for 216 atoms     Final RMS: 12.800 for 199 atoms after 4 cycles
Alignment of try2rat_110_2_hMIN to try2rat_110_1_hMIN:     Initial RMS:  0.000 for 0 atoms     Final RMS:  0.000 for 0 atoms after 0 cycles
Alignment of try2rat_110_3_hMIN to try2rat_110_1_hMIN:     Initial RMS:  0.000 for 0 atoms     Final RMS:  0.000 for 0 atoms after 0 cycles
Alignment of try2rat_110_4_hMIN to try2rat_110_1_hMIN:     Initial RMS:  0.000 for 0 atoms     Final RMS:  0.000 for 0 atoms after 0 cycles
Alignment of try2rat_110_5_hMIN to try2rat_110_1_hMIN:     Initial RMS:  0.000 for 0 atoms     Final RMS:  0.000 for 0 atoms after 0 cycles
Alignment of try2rat_110_3_hMIN to try2rat_110_2_hMIN:     Initial RMS:  0.000 for 0 atoms     Final RMS:  0.000 for 0 atoms after 0 cycles
Alignment of try2rat_110_4_hMIN to try2rat_110_2_hMIN:     Initial RMS:  0.000 for 0 atoms     Final RMS:  0.000 for 0 atoms after 0 cycles
Alignment of try2rat_110_5_hMIN to try2rat_110_2_hMIN:     Initial RMS:  0.000 for 0 atoms     Final RMS:  0.000 for 0 atoms after 0 cycles
Alignment of try2rat_110_4_hMIN to try2rat_110_3_hMIN:     Initial RMS:  0.000 for 0 atoms     Final RMS:  0.000 for 0 atoms after 0 cycles
Alignment of try2rat_110_5_hMIN to try2rat_110_3_hMIN:     Initial RMS:  0.000 for 0 atoms     Final RMS:  0.000 for 0 atoms after 0 cycles
Alignment of try2rat_110_5_hMIN to try2rat_110_4_hMIN:     Initial RMS:  0.000 for 0 atoms     Final RMS:  0.000 for 0 atoms after 0 cycles
     
try2rat_110_1_hMIN try2rat_110_2_hMIN try2rat_110_3_hMIN try2rat_110_4_hMIN try2rat_110_5_hMIN 
try2rat_pdb3tgi 12.935 13.116 12.423  5.279 12.800 
try2rat_110_1_hMIN         0.000  0.000  0.000  0.000 
try2rat_110_2_hMIN                0.000  0.000  0.000 
try2rat_110_3_hMIN                       0.000  0.000 
try2rat_110_4_hMIN                              0.000 
try2rat_110_5_hMIN                                    
