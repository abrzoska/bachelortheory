fetch 1bkr, async=0
center chain A
hide all
color grey80, chain A
set cartoon_flat_sheets, 0
set cartoon_smooth_loops, 0
show cartoon, chain A
set dash_width, 0
set dash_round_ends, off
dist ec1, chain A and name ca and resid 95, chain A and name ca and resid 37, label=0
set dash_radius, 0.500, ec1
set dash_gap, 0.075, ec1
set dash_length, 0.925, ec1
color green, ec1
dist ec2, chain A and name ca and resid 98, chain A and name ca and resid 66, label=0
set dash_radius, 0.495, ec2
set dash_gap, 0.075, ec2
set dash_length, 0.925, ec2
color green, ec2
dist ec3, chain A and name ca and resid 78, chain A and name ca and resid 71, label=0
set dash_radius, 0.430, ec3
set dash_gap, 0.075, ec3
set dash_length, 0.925, ec3
color green, ec3
dist filt_ec4, chain A and name ca and resid 100, chain A and name ca and resid 94, label=0
set dash_radius, 0.05, filt_ec4
set dash_gap, 0.825, filt_ec4
set dash_length, 0.175, filt_ec4
color grey80, filt_ec4
disable filt_ec4
dist ec5, chain A and name ca and resid 98, chain A and name ca and resid 87, label=0
set dash_radius, 0.396, ec5
set dash_gap, 0.075, ec5
set dash_length, 0.925, ec5
color red, ec5
dist ec6, chain A and name ca and resid 86, chain A and name ca and resid 63, label=0
set dash_radius, 0.391, ec6
set dash_gap, 0.075, ec6
set dash_length, 0.925, ec6
color green, ec6
dist ec7, chain A and name ca and resid 82, chain A and name ca and resid 60, label=0
set dash_radius, 0.382, ec7
set dash_gap, 0.075, ec7
set dash_length, 0.925, ec7
color green, ec7
dist ec8, chain A and name ca and resid 30, chain A and name ca and resid 13, label=0
set dash_radius, 0.371, ec8
set dash_gap, 0.075, ec8
set dash_length, 0.925, ec8
color pink, ec8
dist ec9, chain A and name ca and resid 94, chain A and name ca and resid 81, label=0
set dash_radius, 0.370, ec9
set dash_gap, 0.075, ec9
set dash_length, 0.925, ec9
color pink, ec9
dist ec10, chain A and name ca and resid 100, chain A and name ca and resid 8, label=0
set dash_radius, 0.362, ec10
set dash_gap, 0.075, ec10
set dash_length, 0.925, ec10
color green, ec10
dist ec11, chain A and name ca and resid 19, chain A and name ca and resid 13, label=0
set dash_radius, 0.361, ec11
set dash_gap, 0.075, ec11
set dash_length, 0.925, ec11
color pink, ec11
dist ec12, chain A and name ca and resid 66, chain A and name ca and resid 38, label=0
set dash_radius, 0.359, ec12
set dash_gap, 0.075, ec12
set dash_length, 0.925, ec12
color green, ec12
dist filt_ec13, chain A and name ca and resid 103, chain A and name ca and resid 97, label=0
set dash_radius, 0.05, filt_ec13
set dash_gap, 0.825, filt_ec13
set dash_length, 0.175, filt_ec13
color grey80, filt_ec13
disable filt_ec13
dist ec14, chain A and name ca and resid 69, chain A and name ca and resid 41, label=0
set dash_radius, 0.339, ec14
set dash_gap, 0.075, ec14
set dash_length, 0.925, ec14
color green, ec14
dist ec15, chain A and name ca and resid 95, chain A and name ca and resid 85, label=0
set dash_radius, 0.335, ec15
set dash_gap, 0.075, ec15
set dash_length, 0.925, ec15
color green, ec15
dist ec16, chain A and name ca and resid 39, chain A and name ca and resid 19, label=0
set dash_radius, 0.332, ec16
set dash_gap, 0.075, ec16
set dash_length, 0.925, ec16
color green, ec16
dist ec17, chain A and name ca and resid 98, chain A and name ca and resid 80, label=0
set dash_radius, 0.329, ec17
set dash_gap, 0.075, ec17
set dash_length, 0.925, ec17
color green, ec17
dist ec18, chain A and name ca and resid 86, chain A and name ca and resid 59, label=0
set dash_radius, 0.326, ec18
set dash_gap, 0.075, ec18
set dash_length, 0.925, ec18
color green, ec18
dist ec19, chain A and name ca and resid 62, chain A and name ca and resid 34, label=0
set dash_radius, 0.325, ec19
set dash_gap, 0.075, ec19
set dash_length, 0.925, ec19
color green, ec19
dist ec20, chain A and name ca and resid 34, chain A and name ca and resid 11, label=0
set dash_radius, 0.322, ec20
set dash_gap, 0.075, ec20
set dash_length, 0.925, ec20
color red, ec20
dist ec21, chain A and name ca and resid 98, chain A and name ca and resid 85, label=0
set dash_radius, 0.314, ec21
set dash_gap, 0.075, ec21
set dash_length, 0.925, ec21
color pink, ec21
dist ec22, chain A and name ca and resid 39, chain A and name ca and resid 18, label=0
set dash_radius, 0.313, ec22
set dash_gap, 0.075, ec22
set dash_length, 0.925, ec22
color pink, ec22
dist ec23, chain A and name ca and resid 63, chain A and name ca and resid 37, label=0
set dash_radius, 0.309, ec23
set dash_gap, 0.075, ec23
set dash_length, 0.925, ec23
color green, ec23
dist ec24, chain A and name ca and resid 59, chain A and name ca and resid 32, label=0
set dash_radius, 0.307, ec24
set dash_gap, 0.075, ec24
set dash_length, 0.925, ec24
color green, ec24
dist ec25, chain A and name ca and resid 42, chain A and name ca and resid 19, label=0
set dash_radius, 0.304, ec25
set dash_gap, 0.075, ec25
set dash_length, 0.925, ec25
color green, ec25
dist ec26, chain A and name ca and resid 67, chain A and name ca and resid 37, label=0
set dash_radius, 0.302, ec26
set dash_gap, 0.075, ec26
set dash_length, 0.925, ec26
color green, ec26
dist ec27, chain A and name ca and resid 87, chain A and name ca and resid 13, label=0
set dash_radius, 0.301, ec27
set dash_gap, 0.075, ec27
set dash_length, 0.925, ec27
color red, ec27
dist ec28, chain A and name ca and resid 92, chain A and name ca and resid 28, label=0
set dash_radius, 0.291, ec28
set dash_gap, 0.075, ec28
set dash_length, 0.925, ec28
color green, ec28
dist ec29, chain A and name ca and resid 94, chain A and name ca and resid 84, label=0
set dash_radius, 0.288, ec29
set dash_gap, 0.075, ec29
set dash_length, 0.925, ec29
color pink, ec29
dist ec30, chain A and name ca and resid 93, chain A and name ca and resid 84, label=0
set dash_radius, 0.288, ec30
set dash_gap, 0.075, ec30
set dash_length, 0.925, ec30
color pink, ec30
dist ec31, chain A and name ca and resid 80, chain A and name ca and resid 37, label=0
set dash_radius, 0.287, ec31
set dash_gap, 0.075, ec31
set dash_length, 0.925, ec31
color green, ec31
dist ec32, chain A and name ca and resid 100, chain A and name ca and resid 80, label=0
set dash_radius, 0.285, ec32
set dash_gap, 0.075, ec32
set dash_length, 0.925, ec32
color pink, ec32
dist ec33, chain A and name ca and resid 43, chain A and name ca and resid 15, label=0
set dash_radius, 0.283, ec33
set dash_gap, 0.075, ec33
set dash_length, 0.925, ec33
color green, ec33
dist ec34, chain A and name ca and resid 97, chain A and name ca and resid 80, label=0
set dash_radius, 0.278, ec34
set dash_gap, 0.075, ec34
set dash_length, 0.925, ec34
color green, ec34
dist ec35, chain A and name ca and resid 83, chain A and name ca and resid 60, label=0
set dash_radius, 0.277, ec35
set dash_gap, 0.075, ec35
set dash_length, 0.925, ec35
color green, ec35
dist ec36, chain A and name ca and resid 101, chain A and name ca and resid 80, label=0
set dash_radius, 0.276, ec36
set dash_gap, 0.075, ec36
set dash_length, 0.925, ec36
color pink, ec36
dist ec37, chain A and name ca and resid 97, chain A and name ca and resid 90, label=0
set dash_radius, 0.272, ec37
set dash_gap, 0.075, ec37
set dash_length, 0.925, ec37
color pink, ec37
dist ec38, chain A and name ca and resid 98, chain A and name ca and resid 90, label=0
set dash_radius, 0.271, ec38
set dash_gap, 0.075, ec38
set dash_length, 0.925, ec38
color pink, ec38
dist ec39, chain A and name ca and resid 48, chain A and name ca and resid 42, label=0
set dash_radius, 0.269, ec39
set dash_gap, 0.075, ec39
set dash_length, 0.925, ec39
color green, ec39
dist filt_ec40, chain A and name ca and resid 100, chain A and name ca and resid 92, label=0
set dash_radius, 0.05, filt_ec40
set dash_gap, 0.825, filt_ec40
set dash_length, 0.175, filt_ec40
color grey80, filt_ec40
disable filt_ec40
dist ec41, chain A and name ca and resid 103, chain A and name ca and resid 8, label=0
set dash_radius, 0.263, ec41
set dash_gap, 0.075, ec41
set dash_length, 0.925, ec41
color green, ec41
dist ec42, chain A and name ca and resid 65, chain A and name ca and resid 38, label=0
set dash_radius, 0.260, ec42
set dash_gap, 0.075, ec42
set dash_length, 0.925, ec42
color green, ec42
dist ec43, chain A and name ca and resid 66, chain A and name ca and resid 42, label=0
set dash_radius, 0.259, ec43
set dash_gap, 0.075, ec43
set dash_length, 0.925, ec43
color pink, ec43
dist ec44, chain A and name ca and resid 19, chain A and name ca and resid 12, label=0
set dash_radius, 0.257, ec44
set dash_gap, 0.075, ec44
set dash_length, 0.925, ec44
color pink, ec44
dist filt_ec45, chain A and name ca and resid 103, chain A and name ca and resid 92, label=0
set dash_radius, 0.05, filt_ec45
set dash_gap, 0.825, filt_ec45
set dash_length, 0.175, filt_ec45
color grey80, filt_ec45
disable filt_ec45
dist ec46, chain A and name ca and resid 100, chain A and name ca and resid 79, label=0
set dash_radius, 0.252, ec46
set dash_gap, 0.075, ec46
set dash_length, 0.925, ec46
color pink, ec46
dist ec47, chain A and name ca and resid 90, chain A and name ca and resid 32, label=0
set dash_radius, 0.250, ec47
set dash_gap, 0.075, ec47
set dash_length, 0.925, ec47
color green, ec47
dist ec48, chain A and name ca and resid 100, chain A and name ca and resid 81, label=0
set dash_radius, 0.249, ec48
set dash_gap, 0.075, ec48
set dash_length, 0.925, ec48
color red, ec48
dist ec49, chain A and name ca and resid 87, chain A and name ca and resid 66, label=0
set dash_radius, 0.248, ec49
set dash_gap, 0.075, ec49
set dash_length, 0.925, ec49
color red, ec49
dist ec50, chain A and name ca and resid 62, chain A and name ca and resid 38, label=0
set dash_radius, 0.245, ec50
set dash_gap, 0.075, ec50
set dash_length, 0.925, ec50
color green, ec50
disable ec50
dist ec51, chain A and name ca and resid 39, chain A and name ca and resid 31, label=0
set dash_radius, 0.243, ec51
set dash_gap, 0.075, ec51
set dash_length, 0.925, ec51
color pink, ec51
disable ec51
dist filt_ec52, chain A and name ca and resid 16, chain A and name ca and resid 10, label=0
set dash_radius, 0.05, filt_ec52
set dash_gap, 0.825, filt_ec52
set dash_length, 0.175, filt_ec52
color grey80, filt_ec52
disable filt_ec52
dist ec53, chain A and name ca and resid 82, chain A and name ca and resid 63, label=0
set dash_radius, 0.240, ec53
set dash_gap, 0.075, ec53
set dash_length, 0.925, ec53
color green, ec53
disable ec53
dist ec54, chain A and name ca and resid 28, chain A and name ca and resid 9, label=0
set dash_radius, 0.240, ec54
set dash_gap, 0.075, ec54
set dash_length, 0.925, ec54
color pink, ec54
disable ec54
dist filt_ec55, chain A and name ca and resid 100, chain A and name ca and resid 93, label=0
set dash_radius, 0.05, filt_ec55
set dash_gap, 0.825, filt_ec55
set dash_length, 0.175, filt_ec55
color grey80, filt_ec55
disable filt_ec55
dist ec56, chain A and name ca and resid 69, chain A and name ca and resid 38, label=0
set dash_radius, 0.239, ec56
set dash_gap, 0.075, ec56
set dash_length, 0.925, ec56
color green, ec56
disable ec56
dist ec57, chain A and name ca and resid 95, chain A and name ca and resid 87, label=0
set dash_radius, 0.238, ec57
set dash_gap, 0.075, ec57
set dash_length, 0.925, ec57
color pink, ec57
disable ec57
dist filt_ec58, chain A and name ca and resid 17, chain A and name ca and resid 11, label=0
set dash_radius, 0.05, filt_ec58
set dash_gap, 0.825, filt_ec58
set dash_length, 0.175, filt_ec58
color grey80, filt_ec58
disable filt_ec58
dist ec59, chain A and name ca and resid 100, chain A and name ca and resid 90, label=0
set dash_radius, 0.235, ec59
set dash_gap, 0.075, ec59
set dash_length, 0.925, ec59
color red, ec59
disable ec59
dist ec60, chain A and name ca and resid 34, chain A and name ca and resid 5, label=0
set dash_radius, 0.235, ec60
set dash_gap, 0.075, ec60
set dash_length, 0.925, ec60
color red, ec60
disable ec60
dist filt_ec61, chain A and name ca and resid 98, chain A and name ca and resid 92, label=0
set dash_radius, 0.05, filt_ec61
set dash_gap, 0.825, filt_ec61
set dash_length, 0.175, filt_ec61
color grey80, filt_ec61
disable filt_ec61
dist ec62, chain A and name ca and resid 100, chain A and name ca and resid 78, label=0
set dash_radius, 0.233, ec62
set dash_gap, 0.075, ec62
set dash_length, 0.925, ec62
color red, ec62
disable ec62
dist filt_ec63, chain A and name ca and resid 101, chain A and name ca and resid 94, label=0
set dash_radius, 0.05, filt_ec63
set dash_gap, 0.825, filt_ec63
set dash_length, 0.175, filt_ec63
color grey80, filt_ec63
disable filt_ec63
dist ec64, chain A and name ca and resid 18, chain A and name ca and resid 12, label=0
set dash_radius, 0.232, ec64
set dash_gap, 0.075, ec64
set dash_length, 0.925, ec64
color pink, ec64
disable ec64
dist ec65, chain A and name ca and resid 69, chain A and name ca and resid 48, label=0
set dash_radius, 0.230, ec65
set dash_gap, 0.075, ec65
set dash_length, 0.925, ec65
color green, ec65
disable ec65
dist ec66, chain A and name ca and resid 63, chain A and name ca and resid 34, label=0
set dash_radius, 0.227, ec66
set dash_gap, 0.075, ec66
set dash_length, 0.925, ec66
color green, ec66
disable ec66
dist ec67, chain A and name ca and resid 45, chain A and name ca and resid 38, label=0
set dash_radius, 0.226, ec67
set dash_gap, 0.075, ec67
set dash_length, 0.925, ec67
color green, ec67
disable ec67
dist ec68, chain A and name ca and resid 45, chain A and name ca and resid 28, label=0
set dash_radius, 0.224, ec68
set dash_gap, 0.075, ec68
set dash_length, 0.925, ec68
color red, ec68
disable ec68
dist filt_ec69, chain A and name ca and resid 16, chain A and name ca and resid 9, label=0
set dash_radius, 0.05, filt_ec69
set dash_gap, 0.825, filt_ec69
set dash_length, 0.175, filt_ec69
color grey80, filt_ec69
disable filt_ec69
dist ec70, chain A and name ca and resid 90, chain A and name ca and resid 81, label=0
set dash_radius, 0.223, ec70
set dash_gap, 0.075, ec70
set dash_length, 0.925, ec70
color pink, ec70
disable ec70
dist ec71, chain A and name ca and resid 101, chain A and name ca and resid 79, label=0
set dash_radius, 0.223, ec71
set dash_gap, 0.075, ec71
set dash_length, 0.925, ec71
color green, ec71
disable ec71
dist ec72, chain A and name ca and resid 54, chain A and name ca and resid 38, label=0
set dash_radius, 0.223, ec72
set dash_gap, 0.075, ec72
set dash_length, 0.925, ec72
color green, ec72
disable ec72
dist ec73, chain A and name ca and resid 96, chain A and name ca and resid 87, label=0
set dash_radius, 0.222, ec73
set dash_gap, 0.075, ec73
set dash_length, 0.925, ec73
color red, ec73
disable ec73
dist ec74, chain A and name ca and resid 30, chain A and name ca and resid 24, label=0
set dash_radius, 0.221, ec74
set dash_gap, 0.075, ec74
set dash_length, 0.925, ec74
color green, ec74
disable ec74
dist ec75, chain A and name ca and resid 100, chain A and name ca and resid 89, label=0
set dash_radius, 0.219, ec75
set dash_gap, 0.075, ec75
set dash_length, 0.925, ec75
color red, ec75
disable ec75
dist ec76, chain A and name ca and resid 86, chain A and name ca and resid 60, label=0
set dash_radius, 0.218, ec76
set dash_gap, 0.075, ec76
set dash_length, 0.925, ec76
color green, ec76
disable ec76
dist ec77, chain A and name ca and resid 94, chain A and name ca and resid 80, label=0
set dash_radius, 0.217, ec77
set dash_gap, 0.075, ec77
set dash_length, 0.925, ec77
color green, ec77
disable ec77
dist ec78, chain A and name ca and resid 87, chain A and name ca and resid 59, label=0
set dash_radius, 0.216, ec78
set dash_gap, 0.075, ec78
set dash_length, 0.925, ec78
color green, ec78
disable ec78
dist ec79, chain A and name ca and resid 30, chain A and name ca and resid 22, label=0
set dash_radius, 0.216, ec79
set dash_gap, 0.075, ec79
set dash_length, 0.925, ec79
color green, ec79
disable ec79
dist ec80, chain A and name ca and resid 103, chain A and name ca and resid 78, label=0
set dash_radius, 0.215, ec80
set dash_gap, 0.075, ec80
set dash_length, 0.925, ec80
color red, ec80
disable ec80
dist ec81, chain A and name ca and resid 76, chain A and name ca and resid 70, label=0
set dash_radius, 0.215, ec81
set dash_gap, 0.075, ec81
set dash_length, 0.925, ec81
color green, ec81
disable ec81
dist filt_ec82, chain A and name ca and resid 14, chain A and name ca and resid 8, label=0
set dash_radius, 0.05, filt_ec82
set dash_gap, 0.825, filt_ec82
set dash_length, 0.175, filt_ec82
color grey80, filt_ec82
disable filt_ec82
dist ec83, chain A and name ca and resid 100, chain A and name ca and resid 87, label=0
set dash_radius, 0.214, ec83
set dash_gap, 0.075, ec83
set dash_length, 0.925, ec83
color red, ec83
disable ec83
dist ec84, chain A and name ca and resid 97, chain A and name ca and resid 79, label=0
set dash_radius, 0.214, ec84
set dash_gap, 0.075, ec84
set dash_length, 0.925, ec84
color green, ec84
disable ec84
dist ec85, chain A and name ca and resid 82, chain A and name ca and resid 64, label=0
set dash_radius, 0.213, ec85
set dash_gap, 0.075, ec85
set dash_length, 0.925, ec85
color green, ec85
disable ec85
dist ec86, chain A and name ca and resid 93, chain A and name ca and resid 86, label=0
set dash_radius, 0.211, ec86
set dash_gap, 0.075, ec86
set dash_length, 0.925, ec86
color red, ec86
disable ec86
dist ec87, chain A and name ca and resid 92, chain A and name ca and resid 86, label=0
set dash_radius, 0.209, ec87
set dash_gap, 0.075, ec87
set dash_length, 0.925, ec87
color red, ec87
disable ec87
dist ec88, chain A and name ca and resid 103, chain A and name ca and resid 79, label=0
set dash_radius, 0.207, ec88
set dash_gap, 0.075, ec88
set dash_length, 0.925, ec88
color pink, ec88
disable ec88
dist ec89, chain A and name ca and resid 99, chain A and name ca and resid 8, label=0
set dash_radius, 0.207, ec89
set dash_gap, 0.075, ec89
set dash_length, 0.925, ec89
color green, ec89
disable ec89
dist ec90, chain A and name ca and resid 69, chain A and name ca and resid 42, label=0
set dash_radius, 0.207, ec90
set dash_gap, 0.075, ec90
set dash_length, 0.925, ec90
color pink, ec90
disable ec90
dist filt_ec91, chain A and name ca and resid 98, chain A and name ca and resid 91, label=0
set dash_radius, 0.05, filt_ec91
set dash_gap, 0.825, filt_ec91
set dash_length, 0.175, filt_ec91
color grey80, filt_ec91
disable filt_ec91
dist ec92, chain A and name ca and resid 74, chain A and name ca and resid 47, label=0
set dash_radius, 0.204, ec92
set dash_gap, 0.075, ec92
set dash_length, 0.925, ec92
color pink, ec92
disable ec92
dist ec93, chain A and name ca and resid 22, chain A and name ca and resid 13, label=0
set dash_radius, 0.204, ec93
set dash_gap, 0.075, ec93
set dash_length, 0.925, ec93
color pink, ec93
disable ec93
dist ec94, chain A and name ca and resid 30, chain A and name ca and resid 19, label=0
set dash_radius, 0.204, ec94
set dash_gap, 0.075, ec94
set dash_length, 0.925, ec94
color pink, ec94
disable ec94
dist ec95, chain A and name ca and resid 86, chain A and name ca and resid 80, label=0
set dash_radius, 0.203, ec95
set dash_gap, 0.075, ec95
set dash_length, 0.925, ec95
color pink, ec95
disable ec95
dist ec96, chain A and name ca and resid 42, chain A and name ca and resid 22, label=0
set dash_radius, 0.202, ec96
set dash_gap, 0.075, ec96
set dash_length, 0.925, ec96
color pink, ec96
disable ec96
dist ec97, chain A and name ca and resid 66, chain A and name ca and resid 30, label=0
set dash_radius, 0.202, ec97
set dash_gap, 0.075, ec97
set dash_length, 0.925, ec97
color red, ec97
disable ec97
dist ec98, chain A and name ca and resid 62, chain A and name ca and resid 35, label=0
set dash_radius, 0.202, ec98
set dash_gap, 0.075, ec98
set dash_length, 0.925, ec98
color green, ec98
disable ec98
dist ec99, chain A and name ca and resid 97, chain A and name ca and resid 81, label=0
set dash_radius, 0.202, ec99
set dash_gap, 0.075, ec99
set dash_length, 0.925, ec99
color red, ec99
disable ec99
dist ec100, chain A and name ca and resid 66, chain A and name ca and resid 31, label=0
set dash_radius, 0.202, ec100
set dash_gap, 0.075, ec100
set dash_length, 0.925, ec100
color pink, ec100
disable ec100
dist ec101, chain A and name ca and resid 46, chain A and name ca and resid 38, label=0
set dash_radius, 0.202, ec101
set dash_gap, 0.075, ec101
set dash_length, 0.925, ec101
color pink, ec101
disable ec101
dist ec102, chain A and name ca and resid 28, chain A and name ca and resid 19, label=0
set dash_radius, 0.202, ec102
set dash_gap, 0.075, ec102
set dash_length, 0.925, ec102
color red, ec102
disable ec102
dist ec103, chain A and name ca and resid 93, chain A and name ca and resid 79, label=0
set dash_radius, 0.201, ec103
set dash_gap, 0.075, ec103
set dash_length, 0.925, ec103
color pink, ec103
disable ec103
dist ec104, chain A and name ca and resid 58, chain A and name ca and resid 28, label=0
set dash_radius, 0.200, ec104
set dash_gap, 0.075, ec104
set dash_length, 0.925, ec104
color red, ec104
disable ec104
dist ec105, chain A and name ca and resid 51, chain A and name ca and resid 39, label=0
set dash_radius, 0.199, ec105
set dash_gap, 0.075, ec105
set dash_length, 0.925, ec105
color green, ec105
disable ec105
dist ec106, chain A and name ca and resid 92, chain A and name ca and resid 59, label=0
set dash_radius, 0.199, ec106
set dash_gap, 0.075, ec106
set dash_length, 0.925, ec106
color red, ec106
disable ec106
dist ec107, chain A and name ca and resid 80, chain A and name ca and resid 74, label=0
set dash_radius, 0.198, ec107
set dash_gap, 0.075, ec107
set dash_length, 0.925, ec107
color red, ec107
disable ec107
dist filt_ec108, chain A and name ca and resid 11, chain A and name ca and resid 5, label=0
set dash_radius, 0.05, filt_ec108
set dash_gap, 0.825, filt_ec108
set dash_length, 0.175, filt_ec108
color grey80, filt_ec108
disable filt_ec108
dist ec109, chain A and name ca and resid 80, chain A and name ca and resid 63, label=0
set dash_radius, 0.198, ec109
set dash_gap, 0.075, ec109
set dash_length, 0.925, ec109
color green, ec109
disable ec109
dist ec110, chain A and name ca and resid 94, chain A and name ca and resid 87, label=0
set dash_radius, 0.197, ec110
set dash_gap, 0.075, ec110
set dash_length, 0.925, ec110
color pink, ec110
disable ec110
dist ec111, chain A and name ca and resid 98, chain A and name ca and resid 70, label=0
set dash_radius, 0.197, ec111
set dash_gap, 0.075, ec111
set dash_length, 0.925, ec111
color green, ec111
disable ec111
dist ec112, chain A and name ca and resid 77, chain A and name ca and resid 70, label=0
set dash_radius, 0.197, ec112
set dash_gap, 0.075, ec112
set dash_length, 0.925, ec112
color green, ec112
disable ec112
dist ec113, chain A and name ca and resid 48, chain A and name ca and resid 41, label=0
set dash_radius, 0.196, ec113
set dash_gap, 0.075, ec113
set dash_length, 0.925, ec113
color green, ec113
disable ec113
dist ec114, chain A and name ca and resid 92, chain A and name ca and resid 81, label=0
set dash_radius, 0.196, ec114
set dash_gap, 0.075, ec114
set dash_length, 0.925, ec114
color red, ec114
disable ec114
dist ec115, chain A and name ca and resid 101, chain A and name ca and resid 28, label=0
set dash_radius, 0.196, ec115
set dash_gap, 0.075, ec115
set dash_length, 0.925, ec115
color red, ec115
disable ec115
dist ec116, chain A and name ca and resid 73, chain A and name ca and resid 38, label=0
set dash_radius, 0.196, ec116
set dash_gap, 0.075, ec116
set dash_length, 0.925, ec116
color pink, ec116
disable ec116
dist filt_ec117, chain A and name ca and resid 101, chain A and name ca and resid 92, label=0
set dash_radius, 0.05, filt_ec117
set dash_gap, 0.825, filt_ec117
set dash_length, 0.175, filt_ec117
color grey80, filt_ec117
disable filt_ec117
dist ec118, chain A and name ca and resid 30, chain A and name ca and resid 7, label=0
set dash_radius, 0.195, ec118
set dash_gap, 0.075, ec118
set dash_length, 0.925, ec118
color red, ec118
disable ec118
dist filt_ec119, chain A and name ca and resid 101, chain A and name ca and resid 93, label=0
set dash_radius, 0.05, filt_ec119
set dash_gap, 0.825, filt_ec119
set dash_length, 0.175, filt_ec119
color grey80, filt_ec119
disable filt_ec119
dist ec120, chain A and name ca and resid 58, chain A and name ca and resid 38, label=0
set dash_radius, 0.194, ec120
set dash_gap, 0.075, ec120
set dash_length, 0.925, ec120
color pink, ec120
disable ec120
dist ec121, chain A and name ca and resid 92, chain A and name ca and resid 5, label=0
set dash_radius, 0.194, ec121
set dash_gap, 0.075, ec121
set dash_length, 0.925, ec121
color green, ec121
disable ec121
dist ec122, chain A and name ca and resid 87, chain A and name ca and resid 81, label=0
set dash_radius, 0.193, ec122
set dash_gap, 0.075, ec122
set dash_length, 0.925, ec122
color pink, ec122
disable ec122
dist ec123, chain A and name ca and resid 90, chain A and name ca and resid 83, label=0
set dash_radius, 0.193, ec123
set dash_gap, 0.075, ec123
set dash_length, 0.925, ec123
color pink, ec123
disable ec123
dist filt_ec124, chain A and name ca and resid 65, chain A and name ca and resid 58, label=0
set dash_radius, 0.05, filt_ec124
set dash_gap, 0.825, filt_ec124
set dash_length, 0.175, filt_ec124
color grey80, filt_ec124
disable filt_ec124
dist filt_ec125, chain A and name ca and resid 69, chain A and name ca and resid 61, label=0
set dash_radius, 0.05, filt_ec125
set dash_gap, 0.825, filt_ec125
set dash_length, 0.175, filt_ec125
color grey80, filt_ec125
disable filt_ec125
dist ec126, chain A and name ca and resid 91, chain A and name ca and resid 84, label=0
set dash_radius, 0.192, ec126
set dash_gap, 0.075, ec126
set dash_length, 0.925, ec126
color green, ec126
disable ec126
dist ec127, chain A and name ca and resid 74, chain A and name ca and resid 48, label=0
set dash_radius, 0.191, ec127
set dash_gap, 0.075, ec127
set dash_length, 0.925, ec127
color green, ec127
disable ec127
dist ec128, chain A and name ca and resid 80, chain A and name ca and resid 70, label=0
set dash_radius, 0.191, ec128
set dash_gap, 0.075, ec128
set dash_length, 0.925, ec128
color pink, ec128
disable ec128
dist ec129, chain A and name ca and resid 81, chain A and name ca and resid 64, label=0
set dash_radius, 0.191, ec129
set dash_gap, 0.075, ec129
set dash_length, 0.925, ec129
color pink, ec129
disable ec129
dist ec130, chain A and name ca and resid 38, chain A and name ca and resid 30, label=0
set dash_radius, 0.191, ec130
set dash_gap, 0.075, ec130
set dash_length, 0.925, ec130
color pink, ec130
disable ec130
hide lines
recolor
