fetch 2hda, async=0
center chain A
hide all
color grey80, chain A
set cartoon_flat_sheets, 0
set cartoon_smooth_loops, 0
show cartoon, chain A
set dash_width, 0
set dash_round_ends, off
dist ec1, chain A and name ca and resid 142, chain A and name ca and resid 131, label=0
set dash_radius, 0.500, ec1
set dash_gap, 0.075, ec1
set dash_length, 0.925, ec1
color green, ec1
dist ec2, chain A and name ca and resid 143, chain A and name ca and resid 128, label=0
set dash_radius, 0.309, ec2
set dash_gap, 0.075, ec2
set dash_length, 0.925, ec2
color green, ec2
dist ec3, chain A and name ca and resid 131, chain A and name ca and resid 118, label=0
set dash_radius, 0.293, ec3
set dash_gap, 0.075, ec3
set dash_length, 0.925, ec3
color green, ec3
dist ec4, chain A and name ca and resid 140, chain A and name ca and resid 128, label=0
set dash_radius, 0.223, ec4
set dash_gap, 0.075, ec4
set dash_length, 0.925, ec4
color pink, ec4
dist ec5, chain A and name ca and resid 133, chain A and name ca and resid 116, label=0
set dash_radius, 0.150, ec5
set dash_gap, 0.075, ec5
set dash_length, 0.925, ec5
color green, ec5
dist ec6, chain A and name ca and resid 111, chain A and name ca and resid 104, label=0
set dash_radius, 0.132, ec6
set dash_gap, 0.075, ec6
set dash_length, 0.925, ec6
color green, ec6
dist ec7, chain A and name ca and resid 118, chain A and name ca and resid 112, label=0
set dash_radius, 0.130, ec7
set dash_gap, 0.075, ec7
set dash_length, 0.925, ec7
color green, ec7
dist ec8, chain A and name ca and resid 112, chain A and name ca and resid 98, label=0
set dash_radius, 0.126, ec8
set dash_gap, 0.075, ec8
set dash_length, 0.925, ec8
color green, ec8
dist ec9, chain A and name ca and resid 132, chain A and name ca and resid 119, label=0
set dash_radius, 0.123, ec9
set dash_gap, 0.075, ec9
set dash_length, 0.925, ec9
color green, ec9
dist ec10, chain A and name ca and resid 129, chain A and name ca and resid 123, label=0
set dash_radius, 0.121, ec10
set dash_gap, 0.075, ec10
set dash_length, 0.925, ec10
color pink, ec10
dist ec11, chain A and name ca and resid 130, chain A and name ca and resid 122, label=0
set dash_radius, 0.119, ec11
set dash_gap, 0.075, ec11
set dash_length, 0.925, ec11
color green, ec11
dist ec12, chain A and name ca and resid 128, chain A and name ca and resid 98, label=0
set dash_radius, 0.114, ec12
set dash_gap, 0.075, ec12
set dash_length, 0.925, ec12
color red, ec12
dist ec13, chain A and name ca and resid 139, chain A and name ca and resid 132, label=0
set dash_radius, 0.110, ec13
set dash_gap, 0.075, ec13
set dash_length, 0.925, ec13
color green, ec13
dist ec14, chain A and name ca and resid 144, chain A and name ca and resid 129, label=0
set dash_radius, 0.103, ec14
set dash_gap, 0.075, ec14
set dash_length, 0.925, ec14
color green, ec14
dist ec15, chain A and name ca and resid 143, chain A and name ca and resid 98, label=0
set dash_radius, 0.101, ec15
set dash_gap, 0.075, ec15
set dash_length, 0.925, ec15
color pink, ec15
dist ec16, chain A and name ca and resid 131, chain A and name ca and resid 112, label=0
set dash_radius, 0.097, ec16
set dash_gap, 0.075, ec16
set dash_length, 0.925, ec16
color green, ec16
dist ec17, chain A and name ca and resid 128, chain A and name ca and resid 102, label=0
set dash_radius, 0.097, ec17
set dash_gap, 0.075, ec17
set dash_length, 0.925, ec17
color green, ec17
dist ec18, chain A and name ca and resid 113, chain A and name ca and resid 101, label=0
set dash_radius, 0.094, ec18
set dash_gap, 0.075, ec18
set dash_length, 0.925, ec18
color green, ec18
dist ec19, chain A and name ca and resid 128, chain A and name ca and resid 115, label=0
set dash_radius, 0.091, ec19
set dash_gap, 0.075, ec19
set dash_length, 0.925, ec19
color red, ec19
dist ec20, chain A and name ca and resid 139, chain A and name ca and resid 130, label=0
set dash_radius, 0.090, ec20
set dash_gap, 0.075, ec20
set dash_length, 0.925, ec20
color green, ec20
dist ec21, chain A and name ca and resid 140, chain A and name ca and resid 110, label=0
set dash_radius, 0.084, ec21
set dash_gap, 0.075, ec21
set dash_length, 0.925, ec21
color green, ec21
dist ec22, chain A and name ca and resid 133, chain A and name ca and resid 118, label=0
set dash_radius, 0.083, ec22
set dash_gap, 0.075, ec22
set dash_length, 0.925, ec22
color green, ec22
dist ec23, chain A and name ca and resid 140, chain A and name ca and resid 115, label=0
set dash_radius, 0.079, ec23
set dash_gap, 0.075, ec23
set dash_length, 0.925, ec23
color red, ec23
dist ec24, chain A and name ca and resid 140, chain A and name ca and resid 98, label=0
set dash_radius, 0.079, ec24
set dash_gap, 0.075, ec24
set dash_length, 0.925, ec24
color red, ec24
disable ec24
dist ec25, chain A and name ca and resid 131, chain A and name ca and resid 110, label=0
set dash_radius, 0.073, ec25
set dash_gap, 0.075, ec25
set dash_length, 0.925, ec25
color green, ec25
disable ec25
dist ec26, chain A and name ca and resid 142, chain A and name ca and resid 118, label=0
set dash_radius, 0.071, ec26
set dash_gap, 0.075, ec26
set dash_length, 0.925, ec26
color green, ec26
disable ec26
dist ec27, chain A and name ca and resid 128, chain A and name ca and resid 110, label=0
set dash_radius, 0.067, ec27
set dash_gap, 0.075, ec27
set dash_length, 0.925, ec27
color pink, ec27
disable ec27
dist ec28, chain A and name ca and resid 143, chain A and name ca and resid 102, label=0
set dash_radius, 0.066, ec28
set dash_gap, 0.075, ec28
set dash_length, 0.925, ec28
color green, ec28
disable ec28
dist ec29, chain A and name ca and resid 142, chain A and name ca and resid 112, label=0
set dash_radius, 0.064, ec29
set dash_gap, 0.075, ec29
set dash_length, 0.925, ec29
color green, ec29
disable ec29
dist ec30, chain A and name ca and resid 140, chain A and name ca and resid 120, label=0
set dash_radius, 0.064, ec30
set dash_gap, 0.075, ec30
set dash_length, 0.925, ec30
color pink, ec30
disable ec30
dist ec31, chain A and name ca and resid 141, chain A and name ca and resid 127, label=0
set dash_radius, 0.063, ec31
set dash_gap, 0.075, ec31
set dash_length, 0.925, ec31
color pink, ec31
disable ec31
dist ec32, chain A and name ca and resid 131, chain A and name ca and resid 120, label=0
set dash_radius, 0.063, ec32
set dash_gap, 0.075, ec32
set dash_length, 0.925, ec32
color green, ec32
disable ec32
dist ec33, chain A and name ca and resid 128, chain A and name ca and resid 100, label=0
set dash_radius, 0.062, ec33
set dash_gap, 0.075, ec33
set dash_length, 0.925, ec33
color red, ec33
disable ec33
dist ec34, chain A and name ca and resid 143, chain A and name ca and resid 110, label=0
set dash_radius, 0.060, ec34
set dash_gap, 0.075, ec34
set dash_length, 0.925, ec34
color green, ec34
disable ec34
dist ec35, chain A and name ca and resid 141, chain A and name ca and resid 109, label=0
set dash_radius, 0.055, ec35
set dash_gap, 0.075, ec35
set dash_length, 0.925, ec35
color green, ec35
disable ec35
dist ec36, chain A and name ca and resid 129, chain A and name ca and resid 122, label=0
set dash_radius, 0.054, ec36
set dash_gap, 0.075, ec36
set dash_length, 0.925, ec36
color pink, ec36
disable ec36
dist ec37, chain A and name ca and resid 141, chain A and name ca and resid 124, label=0
set dash_radius, 0.053, ec37
set dash_gap, 0.075, ec37
set dash_length, 0.925, ec37
color pink, ec37
disable ec37
dist ec38, chain A and name ca and resid 140, chain A and name ca and resid 102, label=0
set dash_radius, 0.053, ec38
set dash_gap, 0.075, ec38
set dash_length, 0.925, ec38
color pink, ec38
disable ec38
dist ec39, chain A and name ca and resid 127, chain A and name ca and resid 109, label=0
set dash_radius, 0.053, ec39
set dash_gap, 0.075, ec39
set dash_length, 0.925, ec39
color pink, ec39
disable ec39
dist ec40, chain A and name ca and resid 143, chain A and name ca and resid 120, label=0
set dash_radius, 0.053, ec40
set dash_gap, 0.075, ec40
set dash_length, 0.925, ec40
color pink, ec40
disable ec40
dist ec41, chain A and name ca and resid 117, chain A and name ca and resid 97, label=0
set dash_radius, 0.052, ec41
set dash_gap, 0.075, ec41
set dash_length, 0.925, ec41
color green, ec41
disable ec41
dist ec42, chain A and name ca and resid 143, chain A and name ca and resid 115, label=0
set dash_radius, 0.052, ec42
set dash_gap, 0.075, ec42
set dash_length, 0.925, ec42
color red, ec42
disable ec42
dist ec43, chain A and name ca and resid 133, chain A and name ca and resid 112, label=0
set dash_radius, 0.051, ec43
set dash_gap, 0.075, ec43
set dash_length, 0.925, ec43
color green, ec43
disable ec43
dist ec44, chain A and name ca and resid 143, chain A and name ca and resid 100, label=0
set dash_radius, 0.051, ec44
set dash_gap, 0.075, ec44
set dash_length, 0.925, ec44
color pink, ec44
disable ec44
dist ec45, chain A and name ca and resid 134, chain A and name ca and resid 117, label=0
set dash_radius, 0.048, ec45
set dash_gap, 0.075, ec45
set dash_length, 0.925, ec45
color green, ec45
disable ec45
dist ec46, chain A and name ca and resid 128, chain A and name ca and resid 120, label=0
set dash_radius, 0.045, ec46
set dash_gap, 0.075, ec46
set dash_length, 0.925, ec46
color pink, ec46
disable ec46
dist ec47, chain A and name ca and resid 116, chain A and name ca and resid 98, label=0
set dash_radius, 0.045, ec47
set dash_gap, 0.075, ec47
set dash_length, 0.925, ec47
color green, ec47
disable ec47
dist ec48, chain A and name ca and resid 133, chain A and name ca and resid 113, label=0
set dash_radius, 0.044, ec48
set dash_gap, 0.075, ec48
set dash_length, 0.925, ec48
color pink, ec48
disable ec48
dist ec49, chain A and name ca and resid 140, chain A and name ca and resid 116, label=0
set dash_radius, 0.044, ec49
set dash_gap, 0.075, ec49
set dash_length, 0.925, ec49
color pink, ec49
disable ec49
dist ec50, chain A and name ca and resid 141, chain A and name ca and resid 130, label=0
set dash_radius, 0.043, ec50
set dash_gap, 0.075, ec50
set dash_length, 0.925, ec50
color green, ec50
disable ec50
dist ec51, chain A and name ca and resid 114, chain A and name ca and resid 101, label=0
set dash_radius, 0.042, ec51
set dash_gap, 0.075, ec51
set dash_length, 0.925, ec51
color green, ec51
disable ec51
dist ec52, chain A and name ca and resid 130, chain A and name ca and resid 124, label=0
set dash_radius, 0.042, ec52
set dash_gap, 0.075, ec52
set dash_length, 0.925, ec52
color green, ec52
disable ec52
dist ec53, chain A and name ca and resid 128, chain A and name ca and resid 97, label=0
set dash_radius, 0.042, ec53
set dash_gap, 0.075, ec53
set dash_length, 0.925, ec53
color red, ec53
disable ec53
dist ec54, chain A and name ca and resid 141, chain A and name ca and resid 106, label=0
set dash_radius, 0.041, ec54
set dash_gap, 0.075, ec54
set dash_length, 0.925, ec54
color green, ec54
disable ec54
dist ec55, chain A and name ca and resid 140, chain A and name ca and resid 131, label=0
set dash_radius, 0.040, ec55
set dash_gap, 0.075, ec55
set dash_length, 0.925, ec55
color green, ec55
disable ec55
dist ec56, chain A and name ca and resid 134, chain A and name ca and resid 119, label=0
set dash_radius, 0.039, ec56
set dash_gap, 0.075, ec56
set dash_length, 0.925, ec56
color green, ec56
disable ec56
dist ec57, chain A and name ca and resid 132, chain A and name ca and resid 121, label=0
set dash_radius, 0.038, ec57
set dash_gap, 0.075, ec57
set dash_length, 0.925, ec57
color green, ec57
disable ec57
dist ec58, chain A and name ca and resid 129, chain A and name ca and resid 120, label=0
set dash_radius, 0.037, ec58
set dash_gap, 0.075, ec58
set dash_length, 0.925, ec58
color green, ec58
disable ec58
hide lines
recolor
