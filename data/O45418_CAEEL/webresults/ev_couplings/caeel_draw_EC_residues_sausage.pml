fetch 1r9h, async=0
center chain A
hide all
color grey80, chain A
set cartoon_flat_sheets, 0
set cartoon_smooth_loops, 0
show cartoon, chain A
alter 1r9h, b=0.0
select resi 42 and chain A
alter sele, b=11.0770494088
color red, sele
select resi 86 and chain A
alter sele, b=9.9756725988
color red, sele
select resi 50 and chain A
alter sele, b=8.20864340133
color red, sele
select resi 74 and chain A
alter sele, b=8.20057018698
color orange, sele
select resi 80 and chain A
alter sele, b=6.97492228276
color orange, sele
select resi 39 and chain A
alter sele, b=6.77774547989
color orange, sele
select resi 30 and chain A
alter sele, b=5.74696522771
color orange, sele
select resi 79 and chain A
alter sele, b=5.70656390175
color orange, sele
select resi 65 and chain A
alter sele, b=5.35144823302
color orange, sele
select resi 73 and chain A
alter sele, b=5.08249385629
color orange, sele
select resi 116 and chain A
alter sele, b=4.91928448585
color yelloworange, sele
select resi 85 and chain A
alter sele, b=4.52204003482
color yelloworange, sele
select resi 76 and chain A
alter sele, b=4.16290538601
color yelloworange, sele
select resi 94 and chain A
alter sele, b=4.07002816678
color yelloworange, sele
select resi 41 and chain A
alter sele, b=3.9747536612
color yelloworange, sele
select resi 100 and chain A
alter sele, b=3.87304526208
color yelloworange, sele
select resi 88 and chain A
alter sele, b=3.85659917259
color yelloworange, sele
select resi 70 and chain A
alter sele, b=3.79751311473
color yelloworange, sele
select resi 45 and chain A
alter sele, b=3.60382647877
color yelloworange, sele
select resi 108 and chain A
alter sele, b=3.5538888887
color yelloworange, sele
select resi 49 and chain A
alter sele, b=3.51227129246
color yelloworange, sele
select resi 64 and chain A
alter sele, b=3.48655334544
color yelloworange, sele
select resi 117 and chain A
alter sele, b=3.39127883986
color yelloworange, sele
select resi 114 and chain A
alter sele, b=3.37061987213
color yelloworange, sele
select resi 96 and chain A
alter sele, b=3.28676773096
color yelloworange, sele
select resi 92 and chain A
alter sele, b=3.26660232219
color yelloworange, sele
select resi 53 and chain A
alter sele, b=2.88446430062
color yelloworange, sele
select resi 110 and chain A
alter sele, b=2.78289691835
color yelloworange, sele
select resi 107 and chain A
alter sele, b=2.77494709374
color yelloworange, sele
select resi 69 and chain A
alter sele, b=2.75226100888
color yelloworange, sele
select resi 54 and chain A
alter sele, b=2.57465029319
color yelloworange, sele
select resi 44 and chain A
alter sele, b=2.55711132315
color yelloworange, sele
select resi 67 and chain A
alter sele, b=2.52901371688
color yelloworange, sele
select resi 51 and chain A
alter sele, b=2.4670015595
color yelloworange, sele
select resi 40 and chain A
alter sele, b=2.44888089498
color yelloworange, sele
select resi 33 and chain A
alter sele, b=2.42952633307
color yelloworange, sele
select resi 101 and chain A
alter sele, b=2.3881378892
color yelloworange, sele
select resi 90 and chain A
alter sele, b=2.34301249895
color yelloworange, sele
select resi 109 and chain A
alter sele, b=2.31191828473
color yelloworange, sele
select resi 72 and chain A
alter sele, b=2.22799563513
color yelloworange, sele
select resi 62 and chain A
alter sele, b=2.21609733887
color yelloworange, sele
select resi 89 and chain A
alter sele, b=2.20201328153
color yelloworange, sele
select resi 52 and chain A
alter sele, b=2.16242280242
color yelloworange, sele
select resi 66 and chain A
alter sele, b=2.11937741063
color yelloworange, sele
select resi 61 and chain A
alter sele, b=2.03085408648
color yelloworange, sele
select resi 78 and chain A
alter sele, b=1.86344946492
color yelloworange, sele
select resi 113 and chain A
alter sele, b=1.7712420757
color yelloworange, sele
select resi 38 and chain A
alter sele, b=1.74354989286
color yelloworange, sele
select resi 35 and chain A
alter sele, b=1.72174516327
color yelloworange, sele
select resi 55 and chain A
alter sele, b=1.66710113602
color yelloworange, sele
select resi 106 and chain A
alter sele, b=1.54351749889
color yelloworange, sele
select resi 60 and chain A
alter sele, b=1.51848700899
color yelloworange, sele
select resi 37 and chain A
alter sele, b=1.47979551227
color yelloworange, sele
select resi 75 and chain A
alter sele, b=1.45067553388
color yelloworange, sele
select resi 112 and chain A
alter sele, b=1.44452367403
color yelloworange, sele
select resi 63 and chain A
alter sele, b=1.17068658907
color yelloworange, sele
select resi 32 and chain A
alter sele, b=1.16127371469
color yelloworange, sele
select resi 95 and chain A
alter sele, b=1.06807920757
color yelloworange, sele
select resi 77 and chain A
alter sele, b=1.0637605667
color yelloworange, sele
select resi 102 and chain A
alter sele, b=1.05861345188
color yelloworange, sele
select resi 84 and chain A
alter sele, b=1.04742023984
color yelloworange, sele
select resi 91 and chain A
alter sele, b=1.02097958149
color yelloworange, sele
select none
cartoon putty
recolor

