fetch 1r9h, async=0
center chain A
hide all
color grey80, chain A
set cartoon_flat_sheets, 0
set cartoon_smooth_loops, 0
show cartoon, chain A
set dash_width, 0
set dash_round_ends, off
dist ec1, chain A and name ca and resid 50, chain A and name ca and resid 42, label=0
set dash_radius, 0.500, ec1
set dash_gap, 0.075, ec1
set dash_length, 0.925, ec1
color green, ec1
dist ec2, chain A and name ca and resid 86, chain A and name ca and resid 80, label=0
set dash_radius, 0.281, ec2
set dash_gap, 0.075, ec2
set dash_length, 0.925, ec2
color green, ec2
dist ec3, chain A and name ca and resid 74, chain A and name ca and resid 65, label=0
set dash_radius, 0.253, ec3
set dash_gap, 0.075, ec3
set dash_length, 0.925, ec3
color green, ec3
dist ec4, chain A and name ca and resid 116, chain A and name ca and resid 85, label=0
set dash_radius, 0.235, ec4
set dash_gap, 0.075, ec4
set dash_length, 0.925, ec4
color green, ec4
dist ec5, chain A and name ca and resid 86, chain A and name ca and resid 79, label=0
set dash_radius, 0.181, ec5
set dash_gap, 0.075, ec5
set dash_length, 0.925, ec5
color green, ec5
dist ec6, chain A and name ca and resid 88, chain A and name ca and resid 76, label=0
set dash_radius, 0.148, ec6
set dash_gap, 0.075, ec6
set dash_length, 0.925, ec6
color green, ec6
dist ec7, chain A and name ca and resid 117, chain A and name ca and resid 80, label=0
set dash_radius, 0.142, ec7
set dash_gap, 0.075, ec7
set dash_length, 0.925, ec7
color green, ec7
dist ec8, chain A and name ca and resid 74, chain A and name ca and resid 67, label=0
set dash_radius, 0.133, ec8
set dash_gap, 0.075, ec8
set dash_length, 0.925, ec8
color green, ec8
dist ec9, chain A and name ca and resid 107, chain A and name ca and resid 100, label=0
set dash_radius, 0.132, ec9
set dash_gap, 0.075, ec9
set dash_length, 0.925, ec9
color green, ec9
dist ec10, chain A and name ca and resid 79, chain A and name ca and resid 30, label=0
set dash_radius, 0.122, ec10
set dash_gap, 0.075, ec10
set dash_length, 0.925, ec10
color green, ec10
dist ec11, chain A and name ca and resid 53, chain A and name ca and resid 39, label=0
set dash_radius, 0.109, ec11
set dash_gap, 0.075, ec11
set dash_length, 0.925, ec11
color green, ec11
dist ec12, chain A and name ca and resid 55, chain A and name ca and resid 49, label=0
set dash_radius, 0.109, ec12
set dash_gap, 0.075, ec12
set dash_length, 0.925, ec12
color green, ec12
dist ec13, chain A and name ca and resid 90, chain A and name ca and resid 72, label=0
set dash_radius, 0.106, ec13
set dash_gap, 0.075, ec13
set dash_length, 0.925, ec13
color green, ec13
dist ec14, chain A and name ca and resid 88, chain A and name ca and resid 73, label=0
set dash_radius, 0.103, ec14
set dash_gap, 0.075, ec14
set dash_length, 0.925, ec14
color green, ec14
dist ec15, chain A and name ca and resid 61, chain A and name ca and resid 37, label=0
set dash_radius, 0.096, ec15
set dash_gap, 0.075, ec15
set dash_length, 0.925, ec15
color green, ec15
dist ec16, chain A and name ca and resid 94, chain A and name ca and resid 75, label=0
set dash_radius, 0.094, ec16
set dash_gap, 0.075, ec16
set dash_length, 0.925, ec16
color green, ec16
dist ec17, chain A and name ca and resid 96, chain A and name ca and resid 70, label=0
set dash_radius, 0.089, ec17
set dash_gap, 0.075, ec17
set dash_length, 0.925, ec17
color green, ec17
dist ec18, chain A and name ca and resid 108, chain A and name ca and resid 92, label=0
set dash_radius, 0.081, ec18
set dash_gap, 0.075, ec18
set dash_length, 0.925, ec18
color green, ec18
dist ec19, chain A and name ca and resid 52, chain A and name ca and resid 41, label=0
set dash_radius, 0.080, ec19
set dash_gap, 0.075, ec19
set dash_length, 0.925, ec19
color green, ec19
dist ec20, chain A and name ca and resid 117, chain A and name ca and resid 30, label=0
set dash_radius, 0.079, ec20
set dash_gap, 0.075, ec20
set dash_length, 0.925, ec20
color green, ec20
dist ec21, chain A and name ca and resid 63, chain A and name ca and resid 35, label=0
set dash_radius, 0.076, ec21
set dash_gap, 0.075, ec21
set dash_length, 0.925, ec21
color green, ec21
dist ec22, chain A and name ca and resid 78, chain A and name ca and resid 32, label=0
set dash_radius, 0.076, ec22
set dash_gap, 0.075, ec22
set dash_length, 0.925, ec22
color green, ec22
dist ec23, chain A and name ca and resid 51, chain A and name ca and resid 40, label=0
set dash_radius, 0.073, ec23
set dash_gap, 0.075, ec23
set dash_length, 0.925, ec23
color green, ec23
dist ec24, chain A and name ca and resid 109, chain A and name ca and resid 100, label=0
set dash_radius, 0.071, ec24
set dash_gap, 0.075, ec24
set dash_length, 0.925, ec24
color green, ec24
dist ec25, chain A and name ca and resid 108, chain A and name ca and resid 102, label=0
set dash_radius, 0.069, ec25
set dash_gap, 0.075, ec25
set dash_length, 0.925, ec25
color pink, ec25
dist ec26, chain A and name ca and resid 64, chain A and name ca and resid 30, label=0
set dash_radius, 0.069, ec26
set dash_gap, 0.075, ec26
set dash_length, 0.925, ec26
color green, ec26
dist ec27, chain A and name ca and resid 69, chain A and name ca and resid 62, label=0
set dash_radius, 0.069, ec27
set dash_gap, 0.075, ec27
set dash_length, 0.925, ec27
color green, ec27
dist ec28, chain A and name ca and resid 49, chain A and name ca and resid 41, label=0
set dash_radius, 0.067, ec28
set dash_gap, 0.075, ec28
set dash_length, 0.925, ec28
color green, ec28
dist ec29, chain A and name ca and resid 114, chain A and name ca and resid 41, label=0
set dash_radius, 0.067, ec29
set dash_gap, 0.075, ec29
set dash_length, 0.925, ec29
color green, ec29
dist ec30, chain A and name ca and resid 65, chain A and name ca and resid 33, label=0
set dash_radius, 0.064, ec30
set dash_gap, 0.075, ec30
set dash_length, 0.925, ec30
color green, ec30
dist ec31, chain A and name ca and resid 94, chain A and name ca and resid 86, label=0
set dash_radius, 0.062, ec31
set dash_gap, 0.075, ec31
set dash_length, 0.925, ec31
color red, ec31
dist ec32, chain A and name ca and resid 114, chain A and name ca and resid 87, label=0
set dash_radius, 0.062, ec32
set dash_gap, 0.075, ec32
set dash_length, 0.925, ec32
color green, ec32
dist ec33, chain A and name ca and resid 110, chain A and name ca and resid 92, label=0
set dash_radius, 0.061, ec33
set dash_gap, 0.075, ec33
set dash_length, 0.925, ec33
color green, ec33
dist ec34, chain A and name ca and resid 52, chain A and name ca and resid 39, label=0
set dash_radius, 0.061, ec34
set dash_gap, 0.075, ec34
set dash_length, 0.925, ec34
color green, ec34
dist ec35, chain A and name ca and resid 114, chain A and name ca and resid 85, label=0
set dash_radius, 0.059, ec35
set dash_gap, 0.075, ec35
set dash_length, 0.925, ec35
color green, ec35
dist ec36, chain A and name ca and resid 54, chain A and name ca and resid 39, label=0
set dash_radius, 0.059, ec36
set dash_gap, 0.075, ec36
set dash_length, 0.925, ec36
color green, ec36
dist ec37, chain A and name ca and resid 36, chain A and name ca and resid 30, label=0
set dash_radius, 0.059, ec37
set dash_gap, 0.075, ec37
set dash_length, 0.925, ec37
color green, ec37
dist ec38, chain A and name ca and resid 51, chain A and name ca and resid 42, label=0
set dash_radius, 0.056, ec38
set dash_gap, 0.075, ec38
set dash_length, 0.925, ec38
color green, ec38
dist ec39, chain A and name ca and resid 49, chain A and name ca and resid 43, label=0
set dash_radius, 0.053, ec39
set dash_gap, 0.075, ec39
set dash_length, 0.925, ec39
color green, ec39
dist ec40, chain A and name ca and resid 116, chain A and name ca and resid 39, label=0
set dash_radius, 0.052, ec40
set dash_gap, 0.075, ec40
set dash_length, 0.925, ec40
color green, ec40
dist ec41, chain A and name ca and resid 106, chain A and name ca and resid 44, label=0
set dash_radius, 0.052, ec41
set dash_gap, 0.075, ec41
set dash_length, 0.925, ec41
color green, ec41
dist ec42, chain A and name ca and resid 112, chain A and name ca and resid 89, label=0
set dash_radius, 0.051, ec42
set dash_gap, 0.075, ec42
set dash_length, 0.925, ec42
color green, ec42
disable ec42
dist ec43, chain A and name ca and resid 109, chain A and name ca and resid 44, label=0
set dash_radius, 0.049, ec43
set dash_gap, 0.075, ec43
set dash_length, 0.925, ec43
color green, ec43
disable ec43
dist ec44, chain A and name ca and resid 106, chain A and name ca and resid 100, label=0
set dash_radius, 0.049, ec44
set dash_gap, 0.075, ec44
set dash_length, 0.925, ec44
color green, ec44
disable ec44
dist ec45, chain A and name ca and resid 107, chain A and name ca and resid 101, label=0
set dash_radius, 0.048, ec45
set dash_gap, 0.075, ec45
set dash_length, 0.925, ec45
color green, ec45
disable ec45
dist ec46, chain A and name ca and resid 86, chain A and name ca and resid 76, label=0
set dash_radius, 0.048, ec46
set dash_gap, 0.075, ec46
set dash_length, 0.925, ec46
color green, ec46
disable ec46
dist ec47, chain A and name ca and resid 90, chain A and name ca and resid 70, label=0
set dash_radius, 0.047, ec47
set dash_gap, 0.075, ec47
set dash_length, 0.925, ec47
color green, ec47
disable ec47
dist ec48, chain A and name ca and resid 59, chain A and name ca and resid 39, label=0
set dash_radius, 0.047, ec48
set dash_gap, 0.075, ec48
set dash_length, 0.925, ec48
color green, ec48
disable ec48
dist ec49, chain A and name ca and resid 56, chain A and name ca and resid 39, label=0
set dash_radius, 0.047, ec49
set dash_gap, 0.075, ec49
set dash_length, 0.925, ec49
color green, ec49
disable ec49
dist ec50, chain A and name ca and resid 113, chain A and name ca and resid 42, label=0
set dash_radius, 0.047, ec50
set dash_gap, 0.075, ec50
set dash_length, 0.925, ec50
color green, ec50
disable ec50
dist ec51, chain A and name ca and resid 110, chain A and name ca and resid 89, label=0
set dash_radius, 0.047, ec51
set dash_gap, 0.075, ec51
set dash_length, 0.925, ec51
color green, ec51
disable ec51
dist ec52, chain A and name ca and resid 73, chain A and name ca and resid 38, label=0
set dash_radius, 0.046, ec52
set dash_gap, 0.075, ec52
set dash_length, 0.925, ec52
color green, ec52
disable ec52
dist ec53, chain A and name ca and resid 86, chain A and name ca and resid 30, label=0
set dash_radius, 0.046, ec53
set dash_gap, 0.075, ec53
set dash_length, 0.925, ec53
color red, ec53
disable ec53
dist ec54, chain A and name ca and resid 78, chain A and name ca and resid 64, label=0
set dash_radius, 0.046, ec54
set dash_gap, 0.075, ec54
set dash_length, 0.925, ec54
color green, ec54
disable ec54
dist ec55, chain A and name ca and resid 89, chain A and name ca and resid 45, label=0
set dash_radius, 0.046, ec55
set dash_gap, 0.075, ec55
set dash_length, 0.925, ec55
color green, ec55
disable ec55
dist ec56, chain A and name ca and resid 96, chain A and name ca and resid 73, label=0
set dash_radius, 0.045, ec56
set dash_gap, 0.075, ec56
set dash_length, 0.925, ec56
color pink, ec56
disable ec56
dist ec57, chain A and name ca and resid 96, chain A and name ca and resid 40, label=0
set dash_radius, 0.045, ec57
set dash_gap, 0.075, ec57
set dash_length, 0.925, ec57
color pink, ec57
disable ec57
dist ec58, chain A and name ca and resid 54, chain A and name ca and resid 41, label=0
set dash_radius, 0.044, ec58
set dash_gap, 0.075, ec58
set dash_length, 0.925, ec58
color green, ec58
disable ec58
dist ec59, chain A and name ca and resid 108, chain A and name ca and resid 101, label=0
set dash_radius, 0.044, ec59
set dash_gap, 0.075, ec59
set dash_length, 0.925, ec59
color pink, ec59
disable ec59
dist ec60, chain A and name ca and resid 74, chain A and name ca and resid 64, label=0
set dash_radius, 0.044, ec60
set dash_gap, 0.075, ec60
set dash_length, 0.925, ec60
color green, ec60
disable ec60
dist ec61, chain A and name ca and resid 112, chain A and name ca and resid 45, label=0
set dash_radius, 0.043, ec61
set dash_gap, 0.075, ec61
set dash_length, 0.925, ec61
color green, ec61
disable ec61
dist ec62, chain A and name ca and resid 53, chain A and name ca and resid 40, label=0
set dash_radius, 0.042, ec62
set dash_gap, 0.075, ec62
set dash_length, 0.925, ec62
color green, ec62
disable ec62
dist ec63, chain A and name ca and resid 70, chain A and name ca and resid 42, label=0
set dash_radius, 0.042, ec63
set dash_gap, 0.075, ec63
set dash_length, 0.925, ec63
color pink, ec63
disable ec63
dist ec64, chain A and name ca and resid 74, chain A and name ca and resid 66, label=0
set dash_radius, 0.041, ec64
set dash_gap, 0.075, ec64
set dash_length, 0.925, ec64
color green, ec64
disable ec64
dist ec65, chain A and name ca and resid 69, chain A and name ca and resid 42, label=0
set dash_radius, 0.041, ec65
set dash_gap, 0.075, ec65
set dash_length, 0.925, ec65
color red, ec65
disable ec65
dist ec66, chain A and name ca and resid 110, chain A and name ca and resid 45, label=0
set dash_radius, 0.041, ec66
set dash_gap, 0.075, ec66
set dash_length, 0.925, ec66
color green, ec66
disable ec66
dist ec67, chain A and name ca and resid 94, chain A and name ca and resid 72, label=0
set dash_radius, 0.040, ec67
set dash_gap, 0.075, ec67
set dash_length, 0.925, ec67
color green, ec67
disable ec67
dist ec68, chain A and name ca and resid 77, chain A and name ca and resid 62, label=0
set dash_radius, 0.039, ec68
set dash_gap, 0.075, ec68
set dash_length, 0.925, ec68
color green, ec68
disable ec68
dist ec69, chain A and name ca and resid 76, chain A and name ca and resid 69, label=0
set dash_radius, 0.038, ec69
set dash_gap, 0.075, ec69
set dash_length, 0.925, ec69
color pink, ec69
disable ec69
dist ec70, chain A and name ca and resid 92, chain A and name ca and resid 45, label=0
set dash_radius, 0.038, ec70
set dash_gap, 0.075, ec70
set dash_length, 0.925, ec70
color pink, ec70
disable ec70
dist ec71, chain A and name ca and resid 95, chain A and name ca and resid 70, label=0
set dash_radius, 0.037, ec71
set dash_gap, 0.075, ec71
set dash_length, 0.925, ec71
color green, ec71
disable ec71
dist ec72, chain A and name ca and resid 108, chain A and name ca and resid 98, label=0
set dash_radius, 0.037, ec72
set dash_gap, 0.075, ec72
set dash_length, 0.925, ec72
color green, ec72
disable ec72
dist ec73, chain A and name ca and resid 76, chain A and name ca and resid 64, label=0
set dash_radius, 0.037, ec73
set dash_gap, 0.075, ec73
set dash_length, 0.925, ec73
color green, ec73
disable ec73
dist ec74, chain A and name ca and resid 62, chain A and name ca and resid 38, label=0
set dash_radius, 0.037, ec74
set dash_gap, 0.075, ec74
set dash_length, 0.925, ec74
color green, ec74
disable ec74
dist ec75, chain A and name ca and resid 53, chain A and name ca and resid 45, label=0
set dash_radius, 0.037, ec75
set dash_gap, 0.075, ec75
set dash_length, 0.925, ec75
color red, ec75
disable ec75
dist ec76, chain A and name ca and resid 73, chain A and name ca and resid 60, label=0
set dash_radius, 0.036, ec76
set dash_gap, 0.075, ec76
set dash_length, 0.925, ec76
color green, ec76
disable ec76
dist ec77, chain A and name ca and resid 57, chain A and name ca and resid 39, label=0
set dash_radius, 0.036, ec77
set dash_gap, 0.075, ec77
set dash_length, 0.925, ec77
color green, ec77
disable ec77
dist ec78, chain A and name ca and resid 113, chain A and name ca and resid 96, label=0
set dash_radius, 0.036, ec78
set dash_gap, 0.075, ec78
set dash_length, 0.925, ec78
color green, ec78
disable ec78
dist ec79, chain A and name ca and resid 61, chain A and name ca and resid 35, label=0
set dash_radius, 0.036, ec79
set dash_gap, 0.075, ec79
set dash_length, 0.925, ec79
color green, ec79
disable ec79
dist ec80, chain A and name ca and resid 73, chain A and name ca and resid 42, label=0
set dash_radius, 0.035, ec80
set dash_gap, 0.075, ec80
set dash_length, 0.925, ec80
color green, ec80
disable ec80
dist ec81, chain A and name ca and resid 81, chain A and name ca and resid 29, label=0
set dash_radius, 0.035, ec81
set dash_gap, 0.075, ec81
set dash_length, 0.925, ec81
color green, ec81
disable ec81
dist ec82, chain A and name ca and resid 50, chain A and name ca and resid 44, label=0
set dash_radius, 0.035, ec82
set dash_gap, 0.075, ec82
set dash_length, 0.925, ec82
color green, ec82
disable ec82
dist ec83, chain A and name ca and resid 94, chain A and name ca and resid 79, label=0
set dash_radius, 0.034, ec83
set dash_gap, 0.075, ec83
set dash_length, 0.925, ec83
color pink, ec83
disable ec83
dist ec84, chain A and name ca and resid 94, chain A and name ca and resid 84, label=0
set dash_radius, 0.034, ec84
set dash_gap, 0.075, ec84
set dash_length, 0.925, ec84
color red, ec84
disable ec84
dist ec85, chain A and name ca and resid 79, chain A and name ca and resid 66, label=0
set dash_radius, 0.034, ec85
set dash_gap, 0.075, ec85
set dash_length, 0.925, ec85
color red, ec85
disable ec85
dist filt_ec86, chain A and name ca and resid 91, chain A and name ca and resid 84, label=0
set dash_radius, 0.05, filt_ec86
set dash_gap, 0.825, filt_ec86
set dash_length, 0.175, filt_ec86
color grey80, filt_ec86
disable filt_ec86
dist ec87, chain A and name ca and resid 116, chain A and name ca and resid 54, label=0
set dash_radius, 0.033, ec87
set dash_gap, 0.075, ec87
set dash_length, 0.925, ec87
color green, ec87
disable ec87
dist ec88, chain A and name ca and resid 113, chain A and name ca and resid 73, label=0
set dash_radius, 0.033, ec88
set dash_gap, 0.075, ec88
set dash_length, 0.925, ec88
color green, ec88
disable ec88
dist ec89, chain A and name ca and resid 110, chain A and name ca and resid 91, label=0
set dash_radius, 0.033, ec89
set dash_gap, 0.075, ec89
set dash_length, 0.925, ec89
color green, ec89
disable ec89
dist ec90, chain A and name ca and resid 101, chain A and name ca and resid 92, label=0
set dash_radius, 0.033, ec90
set dash_gap, 0.075, ec90
set dash_length, 0.925, ec90
color pink, ec90
disable ec90
dist ec91, chain A and name ca and resid 115, chain A and name ca and resid 73, label=0
set dash_radius, 0.032, ec91
set dash_gap, 0.075, ec91
set dash_length, 0.925, ec91
color green, ec91
disable ec91
dist ec92, chain A and name ca and resid 111, chain A and name ca and resid 95, label=0
set dash_radius, 0.032, ec92
set dash_gap, 0.075, ec92
set dash_length, 0.925, ec92
color green, ec92
disable ec92
dist ec93, chain A and name ca and resid 70, chain A and name ca and resid 51, label=0
set dash_radius, 0.032, ec93
set dash_gap, 0.075, ec93
set dash_length, 0.925, ec93
color pink, ec93
disable ec93
dist ec94, chain A and name ca and resid 64, chain A and name ca and resid 34, label=0
set dash_radius, 0.032, ec94
set dash_gap, 0.075, ec94
set dash_length, 0.925, ec94
color green, ec94
disable ec94
dist ec95, chain A and name ca and resid 114, chain A and name ca and resid 54, label=0
set dash_radius, 0.032, ec95
set dash_gap, 0.075, ec95
set dash_length, 0.925, ec95
color green, ec95
disable ec95
dist ec96, chain A and name ca and resid 69, chain A and name ca and resid 60, label=0
set dash_radius, 0.032, ec96
set dash_gap, 0.075, ec96
set dash_length, 0.925, ec96
color green, ec96
disable ec96
dist ec97, chain A and name ca and resid 74, chain A and name ca and resid 33, label=0
set dash_radius, 0.031, ec97
set dash_gap, 0.075, ec97
set dash_length, 0.925, ec97
color pink, ec97
disable ec97
dist ec98, chain A and name ca and resid 67, chain A and name ca and resid 33, label=0
set dash_radius, 0.031, ec98
set dash_gap, 0.075, ec98
set dash_length, 0.925, ec98
color pink, ec98
disable ec98
dist ec99, chain A and name ca and resid 66, chain A and name ca and resid 33, label=0
set dash_radius, 0.031, ec99
set dash_gap, 0.075, ec99
set dash_length, 0.925, ec99
color green, ec99
disable ec99
dist ec100, chain A and name ca and resid 86, chain A and name ca and resid 65, label=0
set dash_radius, 0.031, ec100
set dash_gap, 0.075, ec100
set dash_length, 0.925, ec100
color red, ec100
disable ec100
dist filt_ec101, chain A and name ca and resid 66, chain A and name ca and resid 60, label=0
set dash_radius, 0.05, filt_ec101
set dash_gap, 0.825, filt_ec101
set dash_length, 0.175, filt_ec101
color grey80, filt_ec101
disable filt_ec101
dist ec102, chain A and name ca and resid 74, chain A and name ca and resid 68, label=0
set dash_radius, 0.031, ec102
set dash_gap, 0.075, ec102
set dash_length, 0.925, ec102
color green, ec102
disable ec102
dist ec103, chain A and name ca and resid 80, chain A and name ca and resid 44, label=0
set dash_radius, 0.031, ec103
set dash_gap, 0.075, ec103
set dash_length, 0.925, ec103
color red, ec103
disable ec103
dist filt_ec104, chain A and name ca and resid 45, chain A and name ca and resid 39, label=0
set dash_radius, 0.05, filt_ec104
set dash_gap, 0.825, filt_ec104
set dash_length, 0.175, filt_ec104
color grey80, filt_ec104
disable filt_ec104
dist ec105, chain A and name ca and resid 77, chain A and name ca and resid 38, label=0
set dash_radius, 0.030, ec105
set dash_gap, 0.075, ec105
set dash_length, 0.925, ec105
color green, ec105
disable ec105
dist ec106, chain A and name ca and resid 109, chain A and name ca and resid 101, label=0
set dash_radius, 0.030, ec106
set dash_gap, 0.075, ec106
set dash_length, 0.925, ec106
color pink, ec106
disable ec106
hide lines
recolor
