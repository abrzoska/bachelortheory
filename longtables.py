import sys
import os
# call as prog evfolg matlab
#sort constraint files 
q = "sort -n -k1,1 -k2,2 "+sys.argv[2]+" > "+sys.argv[2]+"-sort"
os.system(q)
q = "sort -n -k1,1 -k2,2 "+sys.argv[3]+" > "+sys.argv[3]+"-sort"
os.system(q)
q = "sort -n -k1,1 -k2,2 "+sys.argv[4]+" > "+sys.argv[4]+"-sort"
os.system(q)
#open files
f = open(sys.argv[1],"r")
g = open(sys.argv[2]+"-sort","r")
h = open(sys.argv[3]+"-sort", "r")
j = open(sys.argv[4]+"-sort", "r")
fout = open(sys.argv[1]+'-table.tex', "w")

s = """\\begingroup
\setlength{\LTleft}{-20cm plus -1fill}
\setlength{\LTright}{\LTleft}
\\begin{longtable}{|c|c|c|c|c|c|}


\caption[Predicted Contacts (PROTEIN)]{"Contacts represent the contact found analyzing the PDB structure, Ev refers to EV\\_fold, Current to the current implementation, D is the actual distance in Angstr\\"om from the residues and Structure represents the secondary structure" \label{contacttablebpt1}} \\\\
\hline \multicolumn{1}{|c|}{\\textbf{Contacts}} & \multicolumn{1}{c|}{\\textbf{Current}} & \multicolumn{1}{c|}{\\textbf{EV}} & \multicolumn{1}{c|}{\\textbf{MATLAB}} & \multicolumn{1}{c|}{\\textbf{D}} & \multicolumn{1}{c|}{\\textbf{Structure}}\\\\ \hline 
\\endfirsthead
\multicolumn{5}{c}%
{{\\bfseries \\tablename\ \\thetable{} -- continued from previous page}} \\\\
\hline \multicolumn{1}{|c|}{\\textbf{Contacts}} &
\multicolumn{1}{c|}{\\textbf{Current}} &
\multicolumn{1}{c|}{\\textbf{EV}} &
\multicolumn{1}{c|}{\\textbf{MATLAB}} &
\multicolumn{1}{c|}{\\textbf{D}} &
\multicolumn{1}{c|}{\\textbf{Structure}} \\\\ \hline 
\\endhead
\hline \multicolumn{5}{|r|}{{Continued on next page}} \\\\ \hline
\\endfoot
\hline \hline
\\endlastfoot"""

t = """\\end{longtable}
\\endgroup"""

fout.write(s)
#prog
l = g.readline()
l = l.strip('\n')
l = l.split(' ')

#ev fold
m = h.readline()
m = m.strip('\n')
m = m.split(' ')

#matlab
n = j.readline()
n = n.strip('\n')
n = n.split(' ')


#make table 
for line in f:
  s = ""
  line = line.strip('\n')
  contacts = line.split('\t')
  for i in contacts:
    i = "".join(i.split())
    i = i.replace(" ","")
  print contacts
  if len(contacts) == 7:
     print contacts[1], contacts[1].isalpha() , contacts[3], contacts[3].isalpha()
  print "ssssssssssssssssssssssssssssssssssssssssssssssssss"
  if len(contacts) == 7 and contacts[1].isalpha() and contacts[3].isalpha():
     print l+'\n',m+'\n',n+'\n'
     
     #print normal contact // first column
     #print '\t',l,"."
     s += contacts[0]+" "+contacts[1]+" "+contacts[2]+" "+contacts[3]+" & "
     if l and len(l)==2 and int(l[0])==int(contacts[0]) and int(l[1]) == int(contacts[2]):
	#print '\t prog ',l
	s+= " X & "
	l = g.readline()
	l = l.strip('\n')
	l = l.split(' ')
     else:
	  s+=" & "
	  

     if m and len(m)==2 and int(m[0])==int(contacts[0]) and int(m[1]) == int(contacts[2]):
	#print '\t ev fold ',m
	s+= " X & "
	m = h.readline()
	m = m.strip('\n')
	m = m.split(' ')
     else:
	  s+=" & "
     print n
     if n and len(n)==2 and int(n[0])==int(contacts[0]) and int(n[1]) == int(contacts[2]):
	print '\t matlab ',n
	s+= " X & "
	n = j.readline()
	n = n.strip('\n')
	n = n.split(' ')
     else:
	  s+=" & "
	  
	  
     s+= contacts[4]+" & "+contacts[5]+" -"+contacts[6]+""" \\\\"""
     
     fout.write(s)
     print s, "  sdlfsdlfskldf\n"
fout.write(t)
#format of lines
# CONTACTS & EVFOLD & PROG & DIST & SEC STRUC
#1	 P	5 	L & & &	4.8698	 & L	 H  \\
#1	 P	55 	G & & &	4.57113	 & L	 H \\

  
print '-----------'

#prog
#for contacts in g:
#  contacts = contacts.strip('\n')
#  print contacts
#ev fold
#print '-----------'
#for constraints in h:
#  constraints = constraints.strip('\n')
#  print constraints


print '-----------'

    
