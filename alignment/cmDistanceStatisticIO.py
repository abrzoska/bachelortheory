__author__ = 'tseifert'

from BALL import *
from exceptions import *
import csv


def readMappingCSV(file):
    with open(file, 'r') as csvFile:
        csvreader = csv.reader(csvFile)
        csvreader.next()                            # skip column description
        content = [rows[:6] for rows in csvreader]  # read data, exclude column 7 to 9
        return content


def readIndextableplus(file):
    with open(file, 'r') as indextableplusFile:
        csvreader = csv.reader(indextableplusFile, delimiter='\t')
        csvreader.next()                            # skip column description
        content = [rows for rows in csvreader]
        return content


def readPDB(file):
    infile = PDBFile(file);
    s = System()

    infile.read(s)
    infile.close()
    return s


def readParameters(file, section):
    parameters = Parameters(file)
    ps = ParameterSection()
    importedParameters = {}

    ps.extractSection(parameters, section)

    # put all key:value pairs into dict
    for i in range(0, ps.getNumberOfKeys()):
        importedParameters[str(ps.getKey(i))] = num(ps.getValue(ps.getKey(i), 'value'))

    return importedParameters


def num(s):
    s = str(s)
    try:
        return int(s)
    except ValueError:
        return float(s)


def writeLog(filename, log):
    logfile = open(filename, 'w')
    for line in log:
        logfile.write(str(line) + '\n')
    logfile.close()


