__author__ = 'tseifert'

import os
from cmDistanceStatistic import run

# enter here the main folder where your data is located
path = '/home/angiebee/bt/webresults/bpt1-bovin_b2b3bd18-9a9d-4f2f-999f-20bfde81b3d4_evfold_result/data/'
# or simply use current working directory
if path == '':
    path = os.getcwd()

# enter here the distance cutoff for histograms
histogramCutoff = 25

# and start the script
run(path, histogramCutoff)