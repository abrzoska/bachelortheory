import matplotlib.pyplot as plt
import matplotlib.pylab as mlab
import numpy as np


def histogram(data, filename, cutoff, plotName=''):
    num_bins = 20

    mu = np.mean(data)      # mean of distribution
    sigma = np.std(data)    # standard deviation of distribution

    # the histogram of the data
    n, bins, patches = plt.hist(data, num_bins, normed=1, facecolor='green', alpha=0.5)

    # add a 'best fit' line
    y = mlab.normpdf(bins, mu, sigma)

    plt.plot(bins, y, 'r--')
    plt.xlabel('distance in angstrom')
    plt.xlim(right=cutoff)
    plt.ylabel('relative number of contacts')
    plt.title(r'EIC distance compared to PDB $\mu={0:.2f}$, $\sigma={1:.2f}$'.format(mu, sigma))

    if plotName != '':
        plt.figtext(.02, .02, "\n\n" + plotName)

    # Tweak spacing to prevent clipping of ylabel
    plt.subplots_adjust(left=0.15, bottom=0.2)
    plt.savefig(filename)
    plt.close()

    return mu, sigma