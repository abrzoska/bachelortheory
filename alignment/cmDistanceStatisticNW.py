__author__ = 'tseifert'


def needlemanWunsch(seqA, seqB, gapOpeningCost, gapExtensionCost):
    nwScore = [[0 for _ in range(len(seqB) + 1)] for _ in range(len(seqA) + 1)]

    gapsI = [[0 for _ in range(len(seqB) + 1)] for _ in range(len(seqA) + 1)]
    gapsJ = [[0 for _ in range(len(seqB) + 1)] for _ in range(len(seqA) + 1)]

    # initialize arrays
    for k in range(1, len(gapsI)):
        gapsJ[k][0] = gapOpeningCost + k * gapExtensionCost
        nwScore[k][0] = gapOpeningCost + k * gapExtensionCost
    for j in range(1, len(gapsJ[0])):
        gapsI[0][j] = gapOpeningCost + j * gapExtensionCost
        nwScore[0][j] = gapOpeningCost + j * gapExtensionCost

    nwPath = [[[0, 0] for _ in range(len(seqB) + 1)] for _ in range(len(seqA) + 1)]

    for k in range(1, len(nwScore)):
        for j in range(1, len(nwScore[k])):

            gapsI[k][j] = max([nwScore[k-1][j] + gapOpeningCost + gapExtensionCost, gapsI[k-1][j] + gapExtensionCost])
            gapsJ[k][j] = max([nwScore[k][j-1] + gapOpeningCost + gapExtensionCost, gapsJ[k][j-1] + gapExtensionCost])
            nwScore[k][j] = max([nwScore[k-1][j-1] + NWMatch(seqA[k-1], seqB[j-1]), gapsI[k][j], gapsJ[k][j]])

            if gapsI[k][j] == nwScore[k][j]:
                nwPath[k][j] = [k-1, j]
            elif gapsJ[k][j] == nwScore[k][j]:
                nwPath[k][j] = [k, j-1]
            else:
                nwPath[k][j] = [k-1, j-1]

    return nwBacktracking(seqA, seqB, nwPath, nwScore)


def NWMatch(a, b):
    if a == b:
        return 10
    return -1


def nwBacktracking(seqA, seqB, nwPath, nwScore):
    i = len(seqA)
    j = len(seqB)

    seqAAligned = ''
    seqBAligned = ''

    while i != 0 and j != 0 and (nwPath[i][j][0] != i or nwPath[i][j][1] != j):
        if nwPath[i][j][0] == i-1:
            if nwPath[i][j][1] == j-1:
                seqAAligned = seqA[i-1] + seqAAligned
                seqBAligned = seqB[j-1] + seqBAligned
                i -= 1
                j -= 1
            else:
                seqAAligned = seqA[i-1] + seqAAligned
                seqBAligned = '-' + seqBAligned
                i -= 1
        else:
            seqAAligned = '-' + seqAAligned
            seqBAligned = seqB[j-1] + seqBAligned
            j -= 1

    while i != 0:
        seqAAligned = seqA[i-1] + seqAAligned
        seqBAligned = '-' + seqBAligned
        i -= 1
    while j != 0:
        seqAAligned = '-' + seqAAligned
        seqBAligned = seqB[j-1] + seqBAligned
        j -= 1

    return [seqAAligned, seqBAligned, nwScore[-1][-1]]