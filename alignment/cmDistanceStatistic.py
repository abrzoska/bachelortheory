__author__ = 'tseifert'

import os

from BALL import *
from numpy import *
from cmDistanceStatisticIO import *
from cmDistanceStatisticNW import needlemanWunsch
from cmDistanceStatisticPlots import *
import re


def run(folder, cutoff):
    for root, dirs, files in os.walk(folder, topdown=True):
        if len(files) >= 3:

            # read the required files (csv, pdb and [indextablep]lus)
            data = dict([(x[-3:], x) for x in files])

            # output mapping name and pdb ID
            print '\n\n'
            print '-' * 60
            print root
            print data.get('csv')[:-4], ' (', data.get('pdb')[:-4], ') - contacts'
            print '-' * 60

            # read data
            fullSequenceData = readIndextableplus(root + '/' + data.get('lus'))
            pdbSequenceData = readPDB(root + '/' + data.get('pdb'))
            contactMappingData = readMappingCSV(root + '/' + data.get('csv'))

            # preprocess files
            residueMapping, referencePDBChain, preprocessLog = preprocessFiles(fullSequenceData, pdbSequenceData)

            # write preprocess report
            if preprocessLog:
                writeLog(root + '/' + 'mapping.log', preprocessLog)

            # compare calculated mapping with EVFold data (indextableplus with reference PDB positions)
            mappingOkay = compareMappingData(residueMapping, referencePDBChain, fullSequenceData, root)

            # if comparison with EVFold Alignment failed, check by comparing amino acids
            if not mappingOkay:
                mappingOkay = compareMappingData2(residueMapping, referencePDBChain, fullSequenceData, 0.1)

            # run statistics
            if mappingOkay:
                distances = calculateContactDistances(contactMappingData, residueMapping, referencePDBChain)

                statistic_data = histogram([distance[3] for distance in distances if distance[3] != 1000000],
                                           root + '/' + 'histogram_' + data.get('pdb')[:-4] + '.png',
                                           cutoff,
                                           'Mapping: ' + data.get('csv')[:-4] + ' / PDB ID: ' + data.get('pdb')[:-4])

                # add statistical data at the first line
                distances.insert(0, statistic_data)

                writeLog(root + '/' + 'mappingDistances.dat', distances)

                # remove statistical data thereafter
                distances.pop(0)

                # compare distances with EVFold data if provided (column 4 head not 0)
                if len(contactMappingData[0]) >= 4 and contactMappingData[0][3] != '0':
                    for contactRef, contact in zip(contactMappingData, distances):
                        if int(contactRef[0]) - 1 == contact[0] and int(contactRef[1]) - 1 == contact[1]:
                            dif = abs(float(contactRef[3]) - float(contact[3]))
                            if dif > 0.1:
                                print "Distances are different for Contact {0}:{1}: {2:.3f}".format(contactRef[0],
                                                                                                    contactRef[1],
                                                                                                    dif)
                                print float(contactRef[3]), float(contact[3])
                        else:
                            print 'WARNING: Problem with mapping!', contactRef[0], contact[0], contactRef[1], contact[1]


def compareMappingData2(residueMapping, referencePDBChain, fullSequenceData, tolerance):
    mismatches = 0

    for indextableplusRow, calculatedPDBPosition in residueMapping.iteritems():
        mismatches += int(fullSequenceData[indextableplusRow][1] !=
                          str(Peptides.OneLetterCode(referencePDBChain.getResidue(calculatedPDBPosition).getName())))

    if mismatches > len(residueMapping) * tolerance:
        print 'WARNING: Calculated mapping causes more than {0}% residue ' \
              'mismatches.\nPlease check the mapping_mismatch2.log !'.format(int(tolerance * 100))
        return False

    print "DEBUG: amino acid comparison with a tolerance of {0}% was successful.".format(int(tolerance * 100))

    return True


def compareMappingData(residueMapping, referencePDBChain, fullSequenceData, root):
    # check if column 10 exists
    if len(fullSequenceData[0]) < 11:
        return False

    mappingMismatchLog = []

    for indextableplusRow, calculatedPDBPosition in residueMapping.iteritems():
        residueIDCalculated = str(referencePDBChain.getResidue(calculatedPDBPosition).getID())

        # pdb position is contained in column ID 10 of indextableplus file
        residueIDIndextableplus = re.sub("[^0-9]", "", fullSequenceData[indextableplusRow][10])

        if residueIDCalculated != residueIDIndextableplus:
            mappingMismatchLog.append('Position does not match: {0:4} {1:4}'.format(residueIDCalculated,
                                                                                    residueIDIndextableplus))

    if mappingMismatchLog:
        mappingMismatchLog.insert(0, 'indextableplus_pdb_position calculated_pdb_position')
        writeLog(root + '/' + 'mapping_mismatch.log', mappingMismatchLog)
        print 'WARNING: Calculated mapping is not the same as provided by indextableplus file.\nPlease check the', \
            'mapping_mismatch.log !'
        return False

    return True


def calculateContactDistances(contactMap, residueMapping, referencePDBChain):

    # residue distances by C-alpha position
    distances = []

    # iterate over all contacts and calculate their C-alpha distances
    for contact in contactMap:
        # indextableplus index starts with 1, mapping is given starting by 0
        contactA = int(contact[0]) - 1
        contactB = int(contact[1]) - 1

        if contactA in residueMapping.keys() and contactB in residueMapping.keys():
            resA = referencePDBChain.getResidue(residueMapping.get(contactA))
            resB = referencePDBChain.getResidue(residueMapping.get(contactB))

            distances.append([contactA, contactB, float(contact[2]),
                              calculateMinimalAtomDistanceOfResidues(resA, resB)])
        else:
            print "WARNING: contact {0:4d},{1:4d}: no PDB sequence match in mapping - set Value to 1000000".\
                format(contactA, contactB)
            distances.append([contactA, contactB, float(contact[2]), 1000000])
    return distances


def nextChainSequenceInProtein(protein):
    for chain in chains(protein):
        sequence = getChainSequence(chain)  # 0 = sequence, 1 = offset for ACE cap
        if sequence[0]:
            yield sequence[0], chain, sequence[1]
        continue

    return


def iterateOverSequences(sequenceA, sequenceB):
    if len(sequenceA) == len(sequenceB):
        for i in range(len(sequenceA)):
            yield sequenceA[i], sequenceB[i], i
    else:
        return


def getChainSequence(chain):
    sequence = ''
    offsetACE = 0

    for residue in residues(chain):
        if residue.getName() == 'ACE':
            offsetACE = 1
            continue
        if not Peptides.IsThreeLetterCode(residue.getName()):
            return False, False
        sequence += Peptides.OneLetterCode(residue.getName())

    return sequence, offsetACE


def preprocessFiles(refSequenceFile, pdbFile):
    # log
    log = []

    # convert indextableplus column to sequence string
    refSequence = [a[1] for a in refSequenceFile]
    refSequence = ''.join(refSequence)

    protein = pdbFile.getProtein(0)

    # align both sequences and chose the chain with best score
    result = ['', '', float('-infinity'), None]
    for sequence, chain, offsetACE in nextChainSequenceInProtein(protein):
        temp = needlemanWunsch(refSequence, sequence, -2, -1)
        temp.append(chain)

        log.append(sequence)
        log.append('Alignment score: ' + str(temp[2]))

        if temp[2] > result[2]:
            result = temp

    log.append('Chose alignment with score: ' + str(result[2]))
    # map positions and output deviations between ref and pdb
    pdbToRefMapping = {}
    indexSeqA = 0
    indexSeqB = 0 + offsetACE
    firstCharacterInSeqB = False
    for seqA, seqB, indexSeqAligned in iterateOverSequences(result[0], result[1]):
        if seqA != '-':
            if seqB != '-':
                pdbToRefMapping[indexSeqA] = indexSeqB
                indexSeqB += 1
            indexSeqA += 1
        else:
            indexSeqB += 1
        if not firstCharacterInSeqB and seqB != '-':
            firstCharacterInSeqB = True

        # error output for gaps or residue mismatches
        if seqA != seqB and re.search("[A-Z]+", result[1][indexSeqAligned:]) and firstCharacterInSeqB:
            log.append('error@index ' + str(indexSeqAligned) + ': ' + seqA + ' ' + seqB)

    log.append('Alignment:')
    log.append(result[0])
    log.append(result[1])
    log.append('')
    log.append('Mapping (Indextableplus => PDB)')
    for i in range(0, len(pdbToRefMapping.items()), 15):
        log.append(str(pdbToRefMapping.items()[i:(i+15)]))

    # return position map
    return pdbToRefMapping, result[3], log


def calculateMinimalAtomDistanceOfResidues(residueA, residueB):
    minDistance = float('+infinity')
    for atomA in atoms(residueA):
        for atomB in atoms(residueB):
            minDistance = min(minDistance, atomA.getDistance(atomB))

    return minDistance

