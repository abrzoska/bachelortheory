import os
import sys
import re
from cmDistanceStatisticNW import needlemanWunsch
from cmDistanceStatistic import iterateOverSequences
 
#important 1st seq constraints, 2nd contacts to be aligned
refSequence = sys.argv[1]
sequence = sys.argv[2]
#TODO check if needed if only one seq present???
result = ['', '', float('-infinity'), None]
temp = needlemanWunsch(refSequence, sequence, -2, -1)
print temp
if temp[2] > result[2]:
	result = temp

    # map positions and output deviations between ref and pdb
pdbToRefMapping = {}
indexSeqA = 0
indexSeqB = 0
firstCharacterInSeqB = False
print "Alignment Sequence: " ,refSequence 
print "PDB Sequence: ",sequence 
for seqA, seqB, indexSeqAligned in iterateOverSequences(result[0], result[1]):
	if seqA != '-':
		if seqB != '-':
			pdbToRefMapping[indexSeqA] = indexSeqB
			indexSeqB += 1
		indexSeqA += 1
        else:
		indexSeqB += 1
        if not firstCharacterInSeqB and seqB != '-':
            firstCharacterInSeqB = True
            
#for i in pdbToRefMapping:
#	print str(pdbToRefMapping[i]) + '  ' + str(i) +' '+ str(pdbToRefMapping[i] - i)+'\n' 
outputFile = open("indexMapping.txt","w");
#for i in pdbToRefMapping:
#	outputFile.write(str(i))
#outputFile.write("\n")
for i in pdbToRefMapping:
	outputFile.write(str(pdbToRefMapping[i])+ "\t")  
outputFile.write("\n") 
for i in pdbToRefMapping:
	outputFile.write(str(pdbToRefMapping[i] - i)+ "\t") 
outputFile.close()

        # error output for gaps or residue mismatche
#if refSequence != sequence and re.search("[A-Z]+", result[1][indexSeqAligned:]) and firstCharacterInSeqB:
          #  log.append('error@index ' + str(indexSeqAligned) + ': ' + seqA + ' ' + seqB)
	#print 'error@index ' + str(indexSeqAligned) + ': ' + refSequence + ' ' + sequence

