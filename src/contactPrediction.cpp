 #include <iostream>
#include <math.h>
#include <sstream>

#include <boost/multi_array.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/io.hpp>

#include "Eigen/Dense"
#include "Eigen/LU"
#include "Eigen/SVD"

#include <BALL/DATATYPE/string.h>
#include <BALL/SYSTEM/file.h>
#include <BALL/DATATYPE/hashMap.h>
#include <BALL/DATATYPE/stringHashMap.h>

#include "contactPrediction.h"

#define DEBUG 0

using namespace Eigen;
using namespace std;

namespace BALL
{
  typedef Matrix<double, Dynamic, Dynamic> MatrixD;
  StringHashMap<int> accepted_aa;
  StringHashMap<int> presentAA;
  HashMap<int, char> decodeAA;
  vector<vector<int>> counter;
  vector<vector<int>> encoding;
  //for path stuff
  vector<String> p ;
  //Stores all sequences via their UNIPROT identifier
  StringHashMap<String> alignment;
  StringHashMap<String> ranges;
  //Length of the encoded sequence of interest
  int t = 0;
  //Single frequencies
  MatrixD fiA;
  //Pairwise frequencies
  MatrixD fijAB;
  //Correlation matrices
  MatrixD C;
  //Negative inverse of correlation matrices
  MatrixD E_oct;
  MatrixD E_red;
  MatrixD E_eig;
  //Global statistical model
  MatrixD P;
  MatrixD mu1;
  MatrixD mu2;
  //Mutual Information
  MatrixD MI;
  //Direct Information
  MatrixD dij;

  //Offsetmap, stores the indices of the sequence that are actually consideres
  vector<int> offsetmap;
  //Threshold for sequence similiarity in weight calculation
  double theta = 0.3;
  double lambda = 0.5;
  //weights
  vector<double> weights;
  //Number of sequences to output
  int nc;
  //Present aa's. Mostly 21, in small testcases mostly fewer
  int q = 21;
  //Start of the sequence as given in the Stockholm file
  int start;
  //Start of the sequence as given in the Stockholm file
  int stop;
  //The sequence of interest specified by seq
  String s = "";
  //Sequence identity
  String sid = "";
  //The shortened sequence, only those positions specified by offsetmap
  String seq_of_int = "";
  //Encoding of seq_of_int, see see presentAA
  vector<int> enc_seq_of_int;
  //Sum_i 1./weights[i]
  double Meff = 0;
  String algo = "";
  //run time
  double seconds = 0;
  double eps = 0;

  /**
   *
   *
   * */
  /// Default Constructor
  EvolutionaryConstraints::EvolutionaryConstraints()
  {
    accepted_aa['-'] = 0;
    accepted_aa['A'] = 1;
    accepted_aa['C'] = 2;
    accepted_aa['D'] = 3;
    accepted_aa['E'] = 4;
    accepted_aa['F'] = 5;
    accepted_aa['G'] = 6;
    accepted_aa['H'] = 7;
    accepted_aa['I'] = 8;
    accepted_aa['K'] = 9;
    accepted_aa['L'] = 10;
    accepted_aa['M'] = 11;
    accepted_aa['N'] = 12;
    accepted_aa['P'] = 13;
    accepted_aa['Q'] = 14;
    accepted_aa['R'] = 15;
    accepted_aa['S'] = 16;
    accepted_aa['T'] = 17;
    accepted_aa['V'] = 18;
    accepted_aa['W'] = 19;
    accepted_aa['Y'] = 20;
     decodeAA[1] = 'A';
    decodeAA[2] = 'C';
    decodeAA[3] = 'D';
    decodeAA[4] = 'E';
    decodeAA[5] = 'F';
    decodeAA[6] = 'G';
    decodeAA[7] = 'H';
    decodeAA[8] = 'I';
    decodeAA[9] = 'K';
    decodeAA[10] = 'L';
    decodeAA[11] = 'M';
    decodeAA[12] = 'N';
    decodeAA[13] = 'P';
    decodeAA[14] = 'Q';
    decodeAA[15] = 'R';
    decodeAA[16] = 'S';
    decodeAA[17] = 'T';
    decodeAA[18] = 'V';
    decodeAA[19] = 'W';
    decodeAA[20] = 'Y';

  }
  EvolutionaryConstraints::EvolutionaryConstraints(double t, double l)
  {
    accepted_aa['-'] = 0;
    accepted_aa['A'] = 1;
    accepted_aa['C'] = 2;
    accepted_aa['D'] = 3;
    accepted_aa['E'] = 4;
    accepted_aa['F'] = 5;
    accepted_aa['G'] = 6;
    accepted_aa['H'] = 7;
    accepted_aa['I'] = 8;
    accepted_aa['K'] = 9;
    accepted_aa['L'] = 10;
    accepted_aa['M'] = 11;
    accepted_aa['N'] = 12;
    accepted_aa['P'] = 13;
    accepted_aa['Q'] = 14;
    accepted_aa['R'] = 15;
    accepted_aa['S'] = 16;
    accepted_aa['T'] = 17;
    accepted_aa['V'] = 18;
    accepted_aa['W'] = 19;
    accepted_aa['Y'] = 20;
    theta = t;
    lambda = l;
     decodeAA[1] = 'A';
    decodeAA[2] = 'C';
    decodeAA[3] = 'D';
    decodeAA[4] = 'E';
    decodeAA[5] = 'F';
    decodeAA[6] = 'G';
    decodeAA[7] = 'H';
    decodeAA[8] = 'I';
    decodeAA[9] = 'K';
    decodeAA[10] = 'L';
    decodeAA[11] = 'M';
    decodeAA[12] = 'N';
    decodeAA[13] = 'P';
    decodeAA[14] = 'Q';
    decodeAA[15] = 'R';
    decodeAA[16] = 'S';
    decodeAA[17] = 'T';
    decodeAA[18] = 'V';
    decodeAA[19] = 'W';
    decodeAA[20] = 'Y';

  }

  /// Copy Constructor
  EvolutionaryConstraints::EvolutionaryConstraints(
      const EvolutionaryConstraints& ec)
  {
  }
  /// Destructor
  EvolutionaryConstraints::~EvolutionaryConstraints()
  {
  }
  /// Assignment
  EvolutionaryConstraints&
  EvolutionaryConstraints::operator=(const EvolutionaryConstraints& ec)
  {
  }
  int
  readFASTA(File& f)
  {
    cout << "Reading ... " << endl;
    //Holds new line while reading
    String line = "";
    String full = "";
    String full_seq = "";
    String str = "";
    //For split operations
    vector<String> ar;
    vector<String> ar2;
    // '/' denotes the end of a Stockholm file
    while (getline(f, line))
      {
        //If the current line is not a comment

        if (line.at(0) == '>')
          {
            if (full.size() == 0)

              {
                full = line;
              }
            if (full.size() != 0)
              {
                //Then there's a new sequence. Process "full"
                //Split the string into id+range and sequence
                // Then split id/range into id and range
                full.split(ar, String('/').c_str());
                ar[1].split(ar2, String('-').c_str());
                str = ar[0].substr(1);     // get from "live" to the end
                //~ cout << "test " <<  str<< " " <<full_seq << " >> " << ranges[ar[0]]<< endl;
                ranges[str] = ar[1];
                alignment[str] = full_seq;
                full_seq = "";
                full = line;
              }
          }
        else
          {
            full_seq.append(line);
          }
      }
    full.split(ar, String('/').c_str());
    ar[1].split(ar2, String('-').c_str());
    str = ar[0].substr(1);     // get from "live" to the end
    //~ cout << "test " <<  str<< " " <<full_seq << " >> " << ranges[ar[0]]<< endl;
    ranges[str] = ar[1];
    alignment[str] = full_seq;
    cout << "\t Alignment size: " << alignment.size() << endl;
    cout << "\tdone." << endl << endl;
    return 0;
  }
  /*****************************************************************/
  int
  readStockholm(File& f)
  {
    cout << "Reading ... " << endl;
    //Holds new line while reading
    String line;
    //For split operations
    vector<String> ar;
    vector<String> ar2;
    // '/' denotes the end of a Stockholm file
    while (getline(f, line) && line.at(0) != '/')
      {
        //If the current line is not a comment
        if (line.at(0) != '#')
          {
            //~ cout << line << endl;
            //Split the string into id+range and sequence
            line.split(ar, String(' ').c_str());
            // Then split id/range into id and range
            ar[0].split(ar2, String('/').c_str());
            ranges[ar2[0]] = ar2[1];
            alignment[ar2[0]] = ar[1];
          }
      }
    cout << "\t Alignment size: " << alignment.size() << endl;
    cout << "\tdone." << endl << endl;
    return 0;
  }
  /******************************************************************/
  int
  calculateEncoding(){
  //{python write to file
    /// Now we have the number of alignments and the length of each code
    StringHashMap<String>::Iterator si = alignment.begin();
    vector<int> code;
    code.resize(offsetmap.size());
    encoding.resize(alignment.size());
    char c;
    int n = 0;
    ///For every sequence
    cout << "Writing encoding ... " << endl;
    for (; si != alignment.end(); si++)
      {
        ///For every position present in the offset map
        for (int i = 0; i < offsetmap.size(); i++)
          {
            ///Check if it's an actual amino acid (see accepted_aa)
            ///Then also count the single residue frequencies already
            c = si->second[offsetmap[i]];
            if (accepted_aa.find(c) != accepted_aa.end())
              {
                code[i] = accepted_aa[c];
              }
            else
              {
                code[i] = -1;
              }
          }
        encoding[n] = code;
        n++;

      }
      //~ for (int i = 0; i < encoding.size(); i++)
      //~ {
	      //~ for (int j = 0; j<encoding[i].size(); j++)
		//~ {
			//~ cout << encoding[i][j] << " "; 
		//~ }
		//~ cout << endl;
	//~ }
    cout << "\tNumber of positions considered " << code.size() << endl;
    cout << "\tdone." << endl;
  }
  /******************************************************************/
  int
  calculateWeights()
  {
    cout << "Calculating weights ... " << endl;
    double w = 0;
    double k = 0;
    String id = "";
    int size = 0;
    int alsi = alignment.size();
    double b = theta * enc_seq_of_int.size();
    weights.resize(encoding.size());
    for (int i = 0; i < encoding.size(); i++)
      {
        for (int j = 0; j < encoding.size(); j++)
          {
            for (int L = 0; L < encoding[i].size(); L++)
              {
                if (encoding[i][L] == encoding[j][L])
                  {
 //~ cout << i << " " <<encoding[i][L]<< " " << encoding[j][L] << endl;			  
                    w++;
                  }
              }
            if (w >= b)
              {
                k++;
              }
            w = 0;
          }

        weights[i] = 1./k;
        Meff +=  1./k;
        k = 0;

      }
    cout << "\tdone." << endl;
  //  cout << "\t" << weights[0] << " _ " << weights[1]<< " _ " << weights[2]<< " _ " <<  endl;

    return 0;

  }
  /**************************************************************/
  int
  findSpecifiedSequence(String seq)
  {
    //If the current sequence is the wanted sequence, assign s, start, stop
    cout << seq << endl;
    vector<String> ar3;
    sid = seq;
    s = alignment[seq];
    ranges[seq].split(ar3, String('-').c_str());
    start = atoi(ar3[0].c_str());
    stop = atoi(ar3[1].c_str());
    //Now that we have the ID, range and sequence of the sequence of interest.
    //we can compute the encoding of the columns of interest w r t the sequence of interest
    int off = 0;
    //s is the sequence of interest
    for (string::size_type i = 0; i < s.size(); i++)
      {
        //Skip ambiguous residues or gaps, only encode the 20 'normal' amino acids
        //~ if(accepted_aa.find(c) != accepted_aa.end() && c != '-')
        char c = s[i];
        if (accepted_aa.find(c) != accepted_aa.end() && c != '-')
          {
            //Save offset
            offsetmap.push_back(off);
            //add letter to encoded sequence of interest
            enc_seq_of_int.push_back(accepted_aa[c]);
            seq_of_int += c;
          }
        off++;
      }
    //t holds the number of sequences considered
    t = enc_seq_of_int.size();
  }
  /******************************************************************/
  int
  copySubmatrixFromMatrix(MatrixD& big, MatrixD& small, int x, int y)
  {
    for (int i = 0; i < small.rows(); i++)
      {
        for (int j = 0; j < small.cols(); j++)
          {
            small(i, j) = big(x + i, y + j);
          }
      }
    return 0;
  }
  /*****************************************************************/
  int
  copySubmatrixToMatrix(MatrixD& big, MatrixD& small, int x, int y)
  {
    for (int i = 0; i < small.rows(); i++)
      {
        for (int j = 0; j < small.cols(); j++)
          {
            big(x + i, y + j) = small(i, j);
          }
      }
    return 0;
  }
  /*****************************************************************/
  int
  calculateSingleFrequencies()
  {
    cout << "Calculating single residue frequencies ... " << endl;
    fiA.resize(t, q);
    fiA.setZero();
    for (int i = 0; i < encoding.size(); i++)
      {
        for (int j = 0; j < t; j++)
          {
            for (int A = 0; A < q; A++)
              {
                ///since A is never equal to -1 unaccepted amino acids dont mess up this part
                if (encoding[i][j] == A)
                  {
		//~ cout << "deb	" << decodeAA[A] << endl;
                    fiA(j, A) = fiA(j, A) + weights[i];
                  }
              }
          }
      }
    fiA = fiA / Meff;

    for (int i = 0; i < t; i++)
      {
        for (int A = 0; A < q; A++)
          {
            //  				fiA(i,A) = ((lambda/q)+fiA(i,A))/(lambda+Meff);
            //this is how its done in the paper
            fiA(i, A) = ((lambda / q) + (1. - lambda) * fiA(i, A));
          }
      }

    cout << "\tdone." << endl;
    return 0;
  }
  /*****************************************************************/
  int
  calculatePairwiseFrequencies()
  {
    cout << "Calculating pairwise residue frequencies ... " << endl;
    fijAB.resize(t * q, q * t);
    fijAB.setZero();
    for (int i = 0; i < encoding.size(); i++)
      {
        for (int j = 0; j < (t - 1); j++)
      //  for (int j = 0; j < (t ); j++)	      
	{
		for (int k = j + 1; k < t; k++)
		//for (int k = 0; k < t; k++)		
               {
			fijAB(encoding[i][j] + (q * j), encoding[i][k] + (q * k)) += weights[i];
			fijAB(encoding[i][k] + (q * k), encoding[i][j] + (q * j)) =  fijAB(encoding[i][j] + (q * j), encoding[i][k] + (q * k));
              }
          }
      }
      //~ MatrixD fg;
      //~ fg.resize(q,q);
      //~ copySubmatrixFromMatrix(fijAB,fg, 0,0);
//~ cout << "\n\n1,1\n" <<  fg   << endl;
      //~ copySubmatrixFromMatrix(fijAB,fg, 0,1*q);
//~ cout << "\n\n1,2\n" <<  fg   << endl;
            //~ copySubmatrixFromMatrix(fijAB,fg, 1*q,3*q);
//~ cout << "\n\n2,4\n" <<  fg   << endl;    
      fijAB = fijAB / Meff;
      
	for (int i = 0; i < t; i++)
	{
		for (int A = 0; A < q; A++)
		{
			fijAB(A + (q * i), A + (q * i)) = fiA(i, A);
		}
	}
    MatrixD fij_tmp(fijAB);

    for (int i = 0; i < t; i++)
      {
        for (int j = 0; j < t; j++)
          {
            for (int A = 0; A < q; A++)
              {
                for (int B = 0; B < q; B++)
                  {
                    if (i != j)
                      {
                        fijAB(A + (q * i), B + (q * j)) = lambda / q / q     + (1. - lambda) * fij_tmp(A + (q * i), B + (q * j));
                        fijAB(B + (q * j), A + (q * i)) = fijAB(A + (q * i),  B + (q * j));
                      }
                  }
              }
          }
      }

    // 		cout << "\tdone." << endl;

    for (int i = 0; i < t; i++)
      {
		for (int A = 0; A < q; A++)
		{
			fijAB(A + (q * i), A + (q * i)) = fiA(i, A);
		}
      }
    cout << "\tdone." << endl;
    //~ cout << fijAB << endl;
    return 0;
  }
  /*****************************************************************/
  int
  calculateCorrelationMatrix()
  {
    C.resize(t * (q - 1), t * (q - 1));
    cout << "Calculating correlation matrix ... " << endl;
    C.setZero();
    ///calculate C
    int a, b, c, d, e, f, g, h;
    for (int i = 0; i < t; i++)
      {
        for (int j = 0; j < t; j++)
          {
            for (int A = 1; A < q; A++)
              {
                for (int B = 1; B < q; B++)
                  {
                    a = (q - 1) * (i) + (A - 1);
                    b = (j) * (q - 1) + (B - 1);
                    c = (q) * (i) + (A);
                    d = j * (q) + (B);
                    e = (i);
                    f = (A);
                    g = (j);
                    h = (B);
                    C(a, b) = fijAB(c, d) - (fiA(e, f) * fiA(g, h));
                  }
              }

          }
      }
    //~ if(algo.compare("octave") == 0 || algo.compare("redsvd") == 0)
    //~ {
    ofstream myfile;
    myfile.open("./correlation.mat");
    myfile << C << endl;
    myfile.close();
    //~ }
    cout << "\tdone." << endl;
    return 0;
    //~ }
  }
  /*****************************************************************/

  int
  calculateCouplings()
  {
    cout << "Calculating Couplings ... " << endl;
    ifstream myfile;
    system("../octave/callToOctave.sh");
    String line = "";
    vector<String> ar;
    ///will hold all inverted matrices
    E_red.resize(t * (q - 1), t * (q - 1));
    E_oct.resize(t * (q - 1), t * (q - 1));
    E_eig.resize(t * (q - 1), t * (q - 1));
    E_red.setZero();
    E_oct.setZero();
    E_eig.setZero();
    myfile.open("../octave/inverse.mat");
    int j = 0;
    while (getline(myfile, line))
      {
        if (line.at(0) != '#')
          {
            line.split(ar, String(' ').c_str());
            for (int i = 0; i < ar.size(); i++)
              {
                E_oct(i, j) = ar[i].toDouble();
              }
            j++;
          }
      }
    myfile.close();
      /************************/
    system("../redsvd/redsvd.sh");
    ifstream myfile2;
    String line2 = "";
    int rank = 0;
    vector<double> test(t * (q - 1));
    j = 0;
    myfile.open("../redsvd/svd.S");
    j = 0;
    while (getline(myfile, line))
      {
        test[j] = line.toDouble();
        j++;
      }
    rank = j;
    myfile.close();
    //~ vector<String> ar;
    vector<String> ar2;
    MatrixD U;
    MatrixD V;
    MatrixD S(1, rank);
    S.setZero();
    U.resize(t * (q - 1), rank);
    U.setZero();
    myfile.open("../redsvd/svd.U");
    j = 0;
    while (getline(myfile, line))
      {
        line.split(ar, String(' ').c_str());
        for (int i = 0; i < ar.size(); i++)
          {
            U(j, i) = ar[i].toDouble();
          }
        j++;
      }
    myfile.close();
    myfile.open("../redsvd/svd.V");
    j = 0;
    V.resize(t * (q - 1), rank);
    while (getline(myfile, line))
      {
        line.split(ar, String(' ').c_str());
        for (int i = 0; i < ar.size(); i++)
          {
            V(j, i) = ar[i].toDouble();
          }
        j++;
      }
    myfile.close();
    E_red = U * S.asDiagonal() * V.transpose();
/******************/
    if (algo.compare("eigen") == 0)
      {
	      //TODO replace w epsilon
        if (C.determinant() != 0)
          E_eig = C.inverse();
        else
          cout << "ERROR, C not invertible" << endl;
        return 1;
      }
    cout << "\tdone." << endl;
  }
  /*****************************************************************/
  int
  calculateGlobalModel(MatrixD E)
  {
    cout << "Calculating global statistical model ... " << endl;
    P.resize(t * (q - 1), t * (q - 1));
    P.setZero();
    for (int i = 0; i < t; i++)
      {
        for (int j = 0; j < t; j++)
          {
            for (int A = 0; A < q - 1; A++)
              {
                for (int B = 0; B < q - 1; B++)
                  {
                    P(i * (q - 1) + A, j * (q - 1) + B) = exp(E(i * (q - 1) + A, j * (q - 1) + B));
                  }
              }
          }
      }
    //	cout <<"P\n" << P << "________________" <<endl;
    //Sander normalization
    double epsilon = 1e-4;
    double diff = 1.0;
    mu1.resize(1, q - 1);
    mu2.resize(1, q - 1);
    MatrixD pi(1, q - 1);
    MatrixD pj(1, q - 1);
    MatrixD W(q - 1, q - 1);
    MatrixD scra1(1, q - 1);
    MatrixD scra2(1, q - 1);
    MatrixD new1(1, q - 1);
    MatrixD new2(1, q - 1);
    MatrixD diff1(1, q - 1);
    MatrixD diff2(1, q - 1);

    time_t start;
    time_t now;

    dij.resize(t, t);
    //~ time(&start);
    for (int i = 0; i < t - 1; i++)
      {
        for (int j = i + 1; j < t; j++)
          {
            copySubmatrixFromMatrix(P, W, i * (q - 1), j * (q - 1));
            for (int A = 0; A < q - 1; A++)
              {
                mu1(0, A) = 1. / (double) q;
                mu2(0, A) = 1. / (double) q;
              }
            scra1.setZero();
            scra2.setZero();
            pi = fiA.row(j);
            pj = fiA.row(j);
            while (diff > epsilon)
              {
                scra1 = mu2 * W.transpose();
                scra2 = mu1 * W;
                for (int A = 0; A < q - 1; A++)
                  {
                    new1(0, A) = pi(0, A) / scra1(0, A);
                    new2(0, A) = pj(0, A) / scra2(0, A);
                  }
                double sum1 = new1.sum();
                double sum2 = new2.sum();
                for (int A = 0; A < q - 1; A++)
                  {
                    new1(0, A) = new1(0, A) / sum1;
                    new2(0, A) = new2(0, A) / sum2;
                  }
                diff1 = new1 - mu1;
                diff2 = new2 - mu2;
                diff = max(diff1.cwiseAbs().maxCoeff(),
                    diff2.cwiseAbs().maxCoeff());
                mu1 = new1;
                mu2 = new2;
              }
            diff = 1.0;
            MatrixD Pdir = W * (mu1.transpose() * mu2);
            Pdir = Pdir / Pdir.sum();
            MatrixD Pfac = fiA.row(i).transpose() * fiA.row(j);
            MatrixD tmp(q - 1, q - 1);
            for (int A = 0; A < q - 1; A++)
              {
                for (int B = 0; B < q - 1; B++)
                  {
                    tmp(A, B) = log(Pdir(A, B) / Pfac(A, B));
                  }
              }
            MatrixD d(q - 1, q - 1);
            d = Pdir.transpose() * tmp;
            dij(i, j) = d.trace();

            /****************************/
            //dij(i,j) = W.trace();
          }
      }
    cout << "\tdone." << endl;
    return 0;
  }
  /*****************************************************************/
  int
  calculateMutualInformation()
  {
    MI.resize(t, t);
    ///initialize to zero
    MI.setZero();

    ///calculate mutual information
    for (int i = 0; i < t - 1; i++)
      {
        for (int j = i + 1; j < t; j++)
          {
            for (int A = 0; A < q; A++)
              {
                //TODO can this be symmetrical?
                for (int B = 0; B < q; B++)
                  {
                    double x = fijAB(i + (A * t), j + (B * t))
                            / (fiA(i, A) * fiA(j, B));
                    double y = fiA(i, A) * fiA(j, B);
                    if (x != 0 && y != 0) ///to avoid nan or infinity
                      {
                        MI(i, j) += fijAB(i + (A * t), j + (B * t)) * log(x);
                      }
                  }
              }
          }
      }
  }
  /*****************************************************************/
  int
  calculateDirectInformation()
  {
    dij.resize(t, t);
    dij.setZero();
    for (int i = 0; i < t; i++)
      {
        for (int j = i; j < t; j++)
          {
            for (int A = 0; A < q - 1; A++)
              {
                for (int B = 0; B < q - 1; B++)
                  {
                    double x = P(i + (A * t), j + (B * t));
                    double y = fiA(i, A + 1) * fiA(j, B + 1);
                    if (x != 0 && y != 0)
                      {
                        dij(i, j) += P(i + (A * t), j + (B * t)) * log(x / y);
                      }
                  }
              }
          }
      }
  }
  /*****************************************************************/
  int
  outputTopCouplings(String algo)
  {
    String filename = "../data/"+sid+"/results/ranked-"+sid+"-"+algo;
	MatrixD::Index maxR, maxC;
		double max;
		ofstream of;		
		of.open(filename);
    of << "#Sequence " << seq_of_int << endl;
  		of << "#\t calculated in " << time << " seconds" << endl;		
  		of << "#\t from " << alignment.size() << " sequences" << endl;			
		max = dij.maxCoeff(&maxR, &maxC);

		while(max > 0)
		{
			of << maxR+1<< "\t"<< seq_of_int[maxR+1]<< "\t" << maxC+1<< "\t" << seq_of_int[maxC+1] << "\t" << max << endl;
			dij(maxR, maxC) = 0;
			max = dij.maxCoeff(&maxR, &maxC);
			
			}
    of << "################" << endl;
    of << dij << endl;
    of.close();
  }
  /*****************************************************************/
  int
  outputSander(double time, String algo)
  {
    ofstream of;
    String filename = "../data/"+sid+"/results/sander-"+sid+"-"+algo;
    of.open(filename);
    of << "#Sequence " << seq_of_int << endl;
    of << "#\t calculated in " << time << " seconds" << endl;
    of << "#\t from " << alignment.size() << " sequences" << endl;
    //~ int sz = (z*z + z)/2 - z;
    for (int i = 0; i < t - 1; i++)
	      {
		for (int j = i + 1; j < t; j++)
		  {
		    of << i + 1 << "\t" << seq_of_int[i] << "\t" << j + 1 << "\t"
			<< seq_of_int[j] << "\t" << dij(i, j) << endl;
		  }
	      }
//~ of << "\n\n\n" <<    fiA << endl;
      //~ MatrixD test;
      //~ test.resize(q,q);
      //~ copySubmatrixFromMatrix(fijAB,test, 0,0);
//~ of << "\n\n1,1\n" <<  test   << endl;
      //~ copySubmatrixFromMatrix(fijAB,test, 0,1*q);
//~ of << "\n\n1,2\n" <<  test   << endl;
            //~ copySubmatrixFromMatrix(fijAB,test, 1*q,3*q);
//~ of << "\n\n2,4\n" <<  test   << endl;
    of.close();
  }
  /**************************************************************/
  int
  EvolutionaryConstraints::readData(File& f)
  {
    //readStockholm(f);
    readFASTA(f);
    return 0;
  }
  /**************************************************************/
  int
  EvolutionaryConstraints::calculateEvolutionaryCouplings(String s, String seq)
  {

	s.split(p, String('/').c_str());
	  for (int i = 0; i < p.size(); i++)
	  {
		 cout << p[i] << "  &  " <<endl; 
	}

    File f(s); 
    readData(f);
    buildModelForSequence(seq);
    int n = 100;
    computeConstraints(n);
    return 0;
  }
  /**************************************************************/
  int
  EvolutionaryConstraints::buildModelForSequence(String seq)
  {
    findSpecifiedSequence(seq);
    calculateEncoding();
    calculateWeights();
    return 0;
  }
  /**************************************************************/
  int
  EvolutionaryConstraints::computeConstraints(int n)
  {
    time_t start_t;
    time_t end_t;
    time(&start_t);
    nc = (((double) n) / 100) * t;
    calculateSingleFrequencies();
    calculatePairwiseFrequencies();
    calculateCorrelationMatrix();
    calculateCouplings();
    if (calculateGlobalModel(E_oct) == 1)
      {
        ofstream of;
        String filename = "../results/sander-" + sid + "-octave-FAILED";
        of.open(filename);
        of.close();
      }
    else
      {
        time(&end_t);
        outputSander(difftime(end_t, start_t), "octave");
        outputTopCouplings("octave");
      }
    dij.setZero();
    /************/
    if (calculateGlobalModel(E_red) == 1)
      {
        ofstream of;
        String filename = "../results/sander-" + sid + "-redsvd-FAILED";
        of.open(filename);
        of.close();
      }
    else
      {
        time(&end_t);
        outputSander(difftime(end_t, start_t), "redsvd");
        outputTopCouplings("redsvd");
      }
    dij.setZero();
    /**********/
    if (calculateGlobalModel(E_eig) == 1)
      {
        ofstream of;
        String filename = "../results/sander-" + sid + "-eigen-FAILED";
        of.open(filename);
        of.close();
      }
    else
      {
        time(&end_t);
        outputSander(difftime(end_t, start_t), "eigen");
        outputTopCouplings("eigen");
      return 0;
  }

}
}

