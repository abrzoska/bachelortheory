#include <iostream>
#include <sstream>

#include <BALL/SYSTEM/file.h>
#include <BALL/DATATYPE/string.h>

#include "contactPrediction.h"

using namespace std;
using namespace BALL;

int
main(int argc, char* argv[])
{
  /**
   * Call as program file sequence-id:
   * ./cEC ../data/BPT1_BOVIN/ALIGN.ann H0WP28_OTOGA
   */
  // theta lambda
  EvolutionaryConstraints ec(0.3, 0.5);
  ec.calculateEvolutionaryCouplings(argv[1], argv[2]);
}
