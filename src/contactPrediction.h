#ifndef EVOLUTIONARY_CONSTRAINTS
#define EVOLUTIONARY_CONSTRAINTS

#include <BALL/SYSTEM/file.h>
#include <BALL/DATATYPE/hashMap.h>

namespace BALL
{
  class EvolutionaryConstraints
  {
  public:
    //CREATE(EvolutionaryConstraints)
    /// Default constructor
    EvolutionaryConstraints();
    EvolutionaryConstraints(double t, double l);
    /// Copy constructor
    EvolutionaryConstraints(const EvolutionaryConstraints& ec);
    /// Destructor
    virtual
    ~EvolutionaryConstraints();
    /// Assignment operator
    EvolutionaryConstraints&
    operator =(const EvolutionaryConstraints& ec);

    int
    calculateEvolutionaryCouplings(String s, String seq);
    int
    readData(File& f);
    int
    buildModelForSequence(String seq);
    int
    computeConstraints(int nc);

  };
}

#endif
