center
hide all
color grey80
set cartoon_flat_sheets, 0
set cartoon_smooth_loops, 0
show cartoon
set dash_width, 0
set dash_round_ends, off
dist ec1, name ca and resid 73, name ca and resid 49, label=0
set dash_radius, 0.500, ec1
set dash_gap, 0.075, ec1
set dash_length, 0.925, ec1
color green, ec1
dist ec2, name ca and resid 86, name ca and resid 65, label=0
set dash_radius, 0.282, ec2
set dash_gap, 0.075, ec2
set dash_length, 0.925, ec2
color green, ec2
dist filt_ec3, name ca and resid 90, name ca and resid 65, label=0
set dash_radius, 0.05, filt_ec3
set dash_gap, 0.825, filt_ec3
set dash_length, 0.175, filt_ec3
color grey80, filt_ec3
disable filt_ec3
dist filt_ec4, name ca and resid 65, name ca and resid 40, label=0
set dash_radius, 0.05, filt_ec4
set dash_gap, 0.825, filt_ec4
set dash_length, 0.175, filt_ec4
color grey80, filt_ec4
disable filt_ec4
dist ec5, name ca and resid 79, name ca and resid 55, label=0
set dash_radius, 0.210, ec5
set dash_gap, 0.075, ec5
set dash_length, 0.925, ec5
color green, ec5
dist ec6, name ca and resid 86, name ca and resid 80, label=0
set dash_radius, 0.198, ec6
set dash_gap, 0.075, ec6
set dash_length, 0.925, ec6
color green, ec6
dist filt_ec7, name ca and resid 86, name ca and resid 40, label=0
set dash_radius, 0.05, filt_ec7
set dash_gap, 0.825, filt_ec7
set dash_length, 0.175, filt_ec7
color grey80, filt_ec7
disable filt_ec7
dist ec8, name ca and resid 80, name ca and resid 65, label=0
set dash_radius, 0.167, ec8
set dash_gap, 0.075, ec8
set dash_length, 0.925, ec8
color green, ec8
dist ec9, name ca and resid 90, name ca and resid 40, label=0
set dash_radius, 0.165, ec9
set dash_gap, 0.075, ec9
set dash_length, 0.925, ec9
color green, ec9
dist ec10, name ca and resid 66, name ca and resid 59, label=0
set dash_radius, 0.164, ec10
set dash_gap, 0.075, ec10
set dash_length, 0.925, ec10
color green, ec10
dist ec11, name ca and resid 86, name ca and resid 78, label=0
set dash_radius, 0.162, ec11
set dash_gap, 0.075, ec11
set dash_length, 0.925, ec11
color green, ec11
dist ec12, name ca and resid 90, name ca and resid 80, label=0
set dash_radius, 0.145, ec12
set dash_gap, 0.075, ec12
set dash_length, 0.925, ec12
color green, ec12
dist ec13, name ca and resid 78, name ca and resid 65, label=0
set dash_radius, 0.142, ec13
set dash_gap, 0.075, ec13
set dash_length, 0.925, ec13
color green, ec13
dist ec14, name ca and resid 90, name ca and resid 78, label=0
set dash_radius, 0.126, ec14
set dash_gap, 0.075, ec14
set dash_length, 0.925, ec14
color green, ec14
dist ec15, name ca and resid 68, name ca and resid 47, label=0
set dash_radius, 0.105, ec15
set dash_gap, 0.075, ec15
set dash_length, 0.925, ec15
color green, ec15
dist ec16, name ca and resid 65, name ca and resid 58, label=0
set dash_radius, 0.102, ec16
set dash_gap, 0.075, ec16
set dash_length, 0.925, ec16
color green, ec16
dist ec17, name ca and resid 75, name ca and resid 47, label=0
set dash_radius, 0.098, ec17
set dash_gap, 0.075, ec17
set dash_length, 0.925, ec17
color green, ec17
dist ec18, name ca and resid 80, name ca and resid 40, label=0
set dash_radius, 0.096, ec18
set dash_gap, 0.075, ec18
set dash_length, 0.925, ec18
color green, ec18
dist ec19, name ca and resid 71, name ca and resid 48, label=0
set dash_radius, 0.089, ec19
set dash_gap, 0.075, ec19
set dash_length, 0.925, ec19
color green, ec19
dist ec20, name ca and resid 78, name ca and resid 40, label=0
set dash_radius, 0.088, ec20
set dash_gap, 0.075, ec20
set dash_length, 0.925, ec20
color green, ec20
dist ec21, name ca and resid 81, name ca and resid 55, label=0
set dash_radius, 0.086, ec21
set dash_gap, 0.075, ec21
set dash_length, 0.925, ec21
color green, ec21
dist ec22, name ca and resid 76, name ca and resid 68, label=0
set dash_radius, 0.076, ec22
set dash_gap, 0.075, ec22
set dash_length, 0.925, ec22
color green, ec22
dist ec23, name ca and resid 57, name ca and resid 44, label=0
set dash_radius, 0.075, ec23
set dash_gap, 0.075, ec23
set dash_length, 0.925, ec23
color green, ec23
dist filt_ec24, name ca and resid 65, name ca and resid 49, label=0
set dash_radius, 0.05, filt_ec24
set dash_gap, 0.825, filt_ec24
set dash_length, 0.175, filt_ec24
color grey80, filt_ec24
disable filt_ec24
dist ec25, name ca and resid 72, name ca and resid 47, label=0
set dash_radius, 0.066, ec25
set dash_gap, 0.075, ec25
set dash_length, 0.925, ec25
color green, ec25
dist filt_ec26, name ca and resid 86, name ca and resid 73, label=0
set dash_radius, 0.05, filt_ec26
set dash_gap, 0.825, filt_ec26
set dash_length, 0.175, filt_ec26
color grey80, filt_ec26
disable filt_ec26
dist ec27, name ca and resid 90, name ca and resid 58, label=0
set dash_radius, 0.054, ec27
set dash_gap, 0.075, ec27
set dash_length, 0.925, ec27
color green, ec27
disable ec27
dist ec28, name ca and resid 86, name ca and resid 58, label=0
set dash_radius, 0.053, ec28
set dash_gap, 0.075, ec28
set dash_length, 0.925, ec28
color green, ec28
disable ec28
dist ec29, name ca and resid 58, name ca and resid 40, label=0
set dash_radius, 0.052, ec29
set dash_gap, 0.075, ec29
set dash_length, 0.925, ec29
color green, ec29
disable ec29
dist ec30, name ca and resid 75, name ca and resid 68, label=0
set dash_radius, 0.052, ec30
set dash_gap, 0.075, ec30
set dash_length, 0.925, ec30
color green, ec30
disable ec30
dist ec31, name ca and resid 79, name ca and resid 68, label=0
set dash_radius, 0.052, ec31
set dash_gap, 0.075, ec31
set dash_length, 0.925, ec31
color green, ec31
disable ec31
dist ec32, name ca and resid 69, name ca and resid 52, label=0
set dash_radius, 0.048, ec32
set dash_gap, 0.075, ec32
set dash_length, 0.925, ec32
color green, ec32
disable ec32
dist ec33, name ca and resid 71, name ca and resid 51, label=0
set dash_radius, 0.048, ec33
set dash_gap, 0.075, ec33
set dash_length, 0.925, ec33
color green, ec33
disable ec33
dist ec34, name ca and resid 66, name ca and resid 57, label=0
set dash_radius, 0.048, ec34
set dash_gap, 0.075, ec34
set dash_length, 0.925, ec34
color green, ec34
disable ec34
dist ec35, name ca and resid 65, name ca and resid 56, label=0
set dash_radius, 0.047, ec35
set dash_gap, 0.075, ec35
set dash_length, 0.925, ec35
color green, ec35
disable ec35
dist filt_ec36, name ca and resid 49, name ca and resid 40, label=0
set dash_radius, 0.05, filt_ec36
set dash_gap, 0.825, filt_ec36
set dash_length, 0.175, filt_ec36
color grey80, filt_ec36
disable filt_ec36
dist filt_ec37, name ca and resid 86, name ca and resid 49, label=0
set dash_radius, 0.05, filt_ec37
set dash_gap, 0.825, filt_ec37
set dash_length, 0.175, filt_ec37
color grey80, filt_ec37
disable filt_ec37
dist filt_ec38, name ca and resid 73, name ca and resid 65, label=0
set dash_radius, 0.05, filt_ec38
set dash_gap, 0.825, filt_ec38
set dash_length, 0.175, filt_ec38
color grey80, filt_ec38
disable filt_ec38
dist ec39, name ca and resid 65, name ca and resid 59, label=0
set dash_radius, 0.045, ec39
set dash_gap, 0.075, ec39
set dash_length, 0.925, ec39
color green, ec39
disable ec39
dist ec40, name ca and resid 79, name ca and resid 47, label=0
set dash_radius, 0.043, ec40
set dash_gap, 0.075, ec40
set dash_length, 0.925, ec40
color green, ec40
disable ec40
dist ec41, name ca and resid 77, name ca and resid 39, label=0
set dash_radius, 0.042, ec41
set dash_gap, 0.075, ec41
set dash_length, 0.925, ec41
color green, ec41
disable ec41
dist ec42, name ca and resid 77, name ca and resid 42, label=0
set dash_radius, 0.042, ec42
set dash_gap, 0.075, ec42
set dash_length, 0.925, ec42
color green, ec42
disable ec42
dist ec43, name ca and resid 71, name ca and resid 52, label=0
set dash_radius, 0.042, ec43
set dash_gap, 0.075, ec43
set dash_length, 0.925, ec43
color green, ec43
disable ec43
dist ec44, name ca and resid 69, name ca and resid 54, label=0
set dash_radius, 0.041, ec44
set dash_gap, 0.075, ec44
set dash_length, 0.925, ec44
color green, ec44
disable ec44
dist ec45, name ca and resid 71, name ca and resid 50, label=0
set dash_radius, 0.040, ec45
set dash_gap, 0.075, ec45
set dash_length, 0.925, ec45
color green, ec45
disable ec45
dist ec46, name ca and resid 86, name ca and resid 56, label=0
set dash_radius, 0.040, ec46
set dash_gap, 0.075, ec46
set dash_length, 0.925, ec46
color green, ec46
disable ec46
dist ec47, name ca and resid 74, name ca and resid 48, label=0
set dash_radius, 0.039, ec47
set dash_gap, 0.075, ec47
set dash_length, 0.925, ec47
color green, ec47
disable ec47
dist ec48, name ca and resid 76, name ca and resid 70, label=0
set dash_radius, 0.038, ec48
set dash_gap, 0.075, ec48
set dash_length, 0.925, ec48
color green, ec48
disable ec48
dist ec49, name ca and resid 78, name ca and resid 56, label=0
set dash_radius, 0.037, ec49
set dash_gap, 0.075, ec49
set dash_length, 0.925, ec49
color green, ec49
disable ec49
dist ec50, name ca and resid 76, name ca and resid 42, label=0
set dash_radius, 0.037, ec50
set dash_gap, 0.075, ec50
set dash_length, 0.925, ec50
color green, ec50
disable ec50
dist ec51, name ca and resid 68, name ca and resid 55, label=0
set dash_radius, 0.036, ec51
set dash_gap, 0.075, ec51
set dash_length, 0.925, ec51
color green, ec51
disable ec51
dist ec52, name ca and resid 69, name ca and resid 53, label=0
set dash_radius, 0.036, ec52
set dash_gap, 0.075, ec52
set dash_length, 0.925, ec52
color green, ec52
disable ec52
dist ec53, name ca and resid 83, name ca and resid 56, label=0
set dash_radius, 0.035, ec53
set dash_gap, 0.075, ec53
set dash_length, 0.925, ec53
color green, ec53
disable ec53
dist ec54, name ca and resid 76, name ca and resid 47, label=0
set dash_radius, 0.035, ec54
set dash_gap, 0.075, ec54
set dash_length, 0.925, ec54
color green, ec54
disable ec54
dist ec55, name ca and resid 76, name ca and resid 57, label=0
set dash_radius, 0.034, ec55
set dash_gap, 0.075, ec55
set dash_length, 0.925, ec55
color green, ec55
disable ec55
dist ec56, name ca and resid 70, name ca and resid 47, label=0
set dash_radius, 0.034, ec56
set dash_gap, 0.075, ec56
set dash_length, 0.925, ec56
color green, ec56
disable ec56
dist ec57, name ca and resid 69, name ca and resid 51, label=0
set dash_radius, 0.034, ec57
set dash_gap, 0.075, ec57
set dash_length, 0.925, ec57
color green, ec57
disable ec57
dist ec58, name ca and resid 89, name ca and resid 39, label=0
set dash_radius, 0.034, ec58
set dash_gap, 0.075, ec58
set dash_length, 0.925, ec58
color green, ec58
disable ec58
dist ec59, name ca and resid 75, name ca and resid 45, label=0
set dash_radius, 0.033, ec59
set dash_gap, 0.075, ec59
set dash_length, 0.925, ec59
color green, ec59
disable ec59
dist ec60, name ca and resid 71, name ca and resid 53, label=0
set dash_radius, 0.032, ec60
set dash_gap, 0.075, ec60
set dash_length, 0.925, ec60
color green, ec60
disable ec60
dist ec61, name ca and resid 80, name ca and resid 42, label=0
set dash_radius, 0.031, ec61
set dash_gap, 0.075, ec61
set dash_length, 0.925, ec61
color green, ec61
disable ec61
hide lines
recolor
