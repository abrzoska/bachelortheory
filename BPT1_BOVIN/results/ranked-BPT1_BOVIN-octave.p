# Gnuplot script for../../data/BPT1_BOVIN/5PTI-nowater.pdb_5.0.txt with 2 offset 
set   autoscale
unset log
 set ylabel 'Ratio'
set xlabel 'Threshold'
unset label
set xtic auto
set ytic auto                          # set ytics automatically
set title "Plot of program constraints versus PDB contacts with 2 offset" 
plot '../../data/BPT1_BOVIN/results/ranked-BPT1_BOVIN-octave/plots/plot.dat' using 1:3 title 'Perfect Matches/Contacts(PDB)' w l  , '../../data/BPT1_BOVIN/results/ranked-BPT1_BOVIN-octave/plots/plot.dat' using 1:4 title 'Perfect Matches/Constraints' w l   , '../../data/BPT1_BOVIN/results/ranked-BPT1_BOVIN-octave/plots/plot.dat' using 1:5 title 'Perfect Matches + 1/Contacts(PDB)' w l   ,'../../data/BPT1_BOVIN/results/ranked-BPT1_BOVIN-octave/plots/plot.dat' using 1:6 title 'Perfect Matches +2/Contacts(PDB)' w l   ../../data/BPT1_BOVIN/results/ranked-BPT1_BOVIN-octave/plots/plot.dat' using 1: title 'Constraints/Contacts(PDB)' w l ,  ../../data/BPT1_BOVIN/results/ranked-BPT1_BOVIN-octave/plots/plot.dat' using 1:3 title 'Constraints/Contacts(PDB)' w l ,  ../../data/BPT1_BOVIN/results/ranked-BPT1_BOVIN-octave/plots/plot.dat' using 1:3 title 'Constraints/Contacts(PDB)' w l  
