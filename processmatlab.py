import sys
import os
#filter
f = open(sys.argv[1],"r") 
g = open(sys.argv[1]+"-filter","w") 

first = True
offset = 0
s = ""
seq = "#Sequence "
for line in f:
  l = line
  l = l.strip('\n')
  l = l.split(' ')
  
  if len(l) < 10 and len(l) > 1 :
     if first:
	print l[0]
	offset = l[0]
	first = False
	seq+=l[1]
     if l[0] == offset:
       seq+=l[3]
     s+=str(int(l[0])-int(offset))+" "+l[1]+" "+str(int(l[2])-int(offset))+" "+l[3]+" "+l[4]+"\n"
     g.write(s)
     s = ""
seq+="\n"
print seq
#sort constraint files 
q = "sort -n -r -k5 "+sys.argv[1]+"-filter"+" > "+sys.argv[1]+"-sort"
os.system(q)

#open files
f = open(sys.argv[1]+"-sort","r") 
g = open(sys.argv[1]+"-processed","w") 

g.write(seq)
for line in f:
  g.write(line)

