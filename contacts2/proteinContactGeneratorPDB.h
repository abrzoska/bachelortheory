// -*- Mode: C++; tab-width: 2; -*- vi: set ts=2:
/* ----------------------------------------------------------------------------
  proteinContactGeneratorPDB.h
  
  Depends on libraries: Eigen and BALL
  
 DESCRIPTION:
  The header file for the proteinContactGeneratorPDB class generating contact 
  matrix from a given PDBFile.
---------------------------------------------------------------------------- */

#ifndef PROTEINCONTACTGENERATORPDB_H
#define PROTEINCONTACTGENERATORPDB_H

#ifndef BALL_DATATYPE_STRING_H
#include <BALL/DATATYPE/string.h>    // BALL::String
#endif

#ifndef BALL_KERNEL_SYSTEM_H
#include <BALL/KERNEL/system.h>      // BALL::System
#endif

#ifndef BALL_DATATYPE_HASHGRID_H
#include <BALL/DATATYPE/hashGrid.h> // BALL::HashGrid3
#endif

#define EIGEN_YES_I_KNOW_SPARSE_MODULE_IS_NOT_STABLE_YET
#include <Eigen/Sparse>            // Eigen::SparceMatrix
#include <vector>                  // std::vector
#include <map>                     // std::map
#include <utility>                 // std::pair
#include <fstream>                 // std::ostream


class ProteinContactGeneratorPDB
{
public:
	
	// Type definitions 
	typedef std::map<std::pair<unsigned int,unsigned int>,float> ContactMapType;
	typedef std::map<std::pair<unsigned int,unsigned int>,float>::const_iterator ContactMapConstIteartorType;
	
	// Friends
	friend std::ostream& operator<<(std::ostream&, const ProteinContactGeneratorPDB&); //output
	 //~ void output(const ProteinContactGeneratorPDB& arg);
	
	//Constructor
	ProteinContactGeneratorPDB(const BALL::String&, const float treshold = 4.0); 
		
	// Destructor 
	virtual ~ProteinContactGeneratorPDB();
	
	// Getters 
	BALL::String getPDBFileName() const;
	float getAtomDistanceTreshold() const;
	Eigen::SparseMatrix<double> getMatrix() const;
	ContactMapType getContactMap() const;
	std::vector<unsigned int> getResidueIDs() const;
	BALL::String getSequence() const;
	
	//Modifiers 
	void generateContactMatrixFromPDB();
	void clear();
	void reset(const BALL::String& fullPdbFilePath, const float treshold);

		
private:	
	// Methods
	void readPDBFile(const BALL::String&, BALL::System&) const;
	void fillGrid(const BALL::System&, BALL::HashGrid3<BALL::Atom*>&, std::vector<BALL::Atom*>&);
	void findContacts(BALL::HashGrid3<BALL::Atom*>&, const std::vector<BALL::Atom*>&);
	// Variables
	BALL::String                    PDB_file_name_;          // full path of the PDB file 
	BALL::String              seq;
	float                           atom_distance_treshold_; // the threshold in Angstroms
	ContactMapType                  contact_map_;            // keeps the pair of IDs of the resiudes in contact, including their distance 
	Eigen::SparseMatrix<double>     contact_matrix_;         // matrix of 1 (contact) and 0 (no contact)
	std::vector<unsigned int>       resPos_resID_;           // for every resiude (position in the sequence) keeps its original ID (in order to keep track of missing resiudes)

	
};
#endif // PROTEINCONTACTGENERATORPDB_H
