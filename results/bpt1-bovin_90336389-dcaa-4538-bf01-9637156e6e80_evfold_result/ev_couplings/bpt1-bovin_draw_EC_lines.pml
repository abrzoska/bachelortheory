fetch 5pti, async=0
center chain A
hide all
color grey80, chain A
set cartoon_flat_sheets, 0
set cartoon_smooth_loops, 0
show cartoon, chain A
set dash_width, 0
set dash_round_ends, off
dist ec1, chain A and name ca and resid 38, chain A and name ca and resid 14, label=0
set dash_radius, 0.500, ec1
set dash_gap, 0.075, ec1
set dash_length, 0.925, ec1
color green, ec1
dist ec2, chain A and name ca and resid 51, chain A and name ca and resid 30, label=0
set dash_radius, 0.282, ec2
set dash_gap, 0.075, ec2
set dash_length, 0.925, ec2
color green, ec2
dist filt_ec3, chain A and name ca and resid 55, chain A and name ca and resid 30, label=0
set dash_radius, 0.05, filt_ec3
set dash_gap, 0.825, filt_ec3
set dash_length, 0.175, filt_ec3
color grey80, filt_ec3
disable filt_ec3
dist filt_ec4, chain A and name ca and resid 30, chain A and name ca and resid 5, label=0
set dash_radius, 0.05, filt_ec4
set dash_gap, 0.825, filt_ec4
set dash_length, 0.175, filt_ec4
color grey80, filt_ec4
disable filt_ec4
dist ec5, chain A and name ca and resid 44, chain A and name ca and resid 20, label=0
set dash_radius, 0.210, ec5
set dash_gap, 0.075, ec5
set dash_length, 0.925, ec5
color green, ec5
dist ec6, chain A and name ca and resid 51, chain A and name ca and resid 45, label=0
set dash_radius, 0.198, ec6
set dash_gap, 0.075, ec6
set dash_length, 0.925, ec6
color green, ec6
dist filt_ec7, chain A and name ca and resid 51, chain A and name ca and resid 5, label=0
set dash_radius, 0.05, filt_ec7
set dash_gap, 0.825, filt_ec7
set dash_length, 0.175, filt_ec7
color grey80, filt_ec7
disable filt_ec7
dist ec8, chain A and name ca and resid 45, chain A and name ca and resid 30, label=0
set dash_radius, 0.167, ec8
set dash_gap, 0.075, ec8
set dash_length, 0.925, ec8
color green, ec8
dist ec9, chain A and name ca and resid 55, chain A and name ca and resid 5, label=0
set dash_radius, 0.165, ec9
set dash_gap, 0.075, ec9
set dash_length, 0.925, ec9
color green, ec9
dist ec10, chain A and name ca and resid 31, chain A and name ca and resid 24, label=0
set dash_radius, 0.164, ec10
set dash_gap, 0.075, ec10
set dash_length, 0.925, ec10
color green, ec10
dist ec11, chain A and name ca and resid 51, chain A and name ca and resid 43, label=0
set dash_radius, 0.162, ec11
set dash_gap, 0.075, ec11
set dash_length, 0.925, ec11
color green, ec11
dist ec12, chain A and name ca and resid 55, chain A and name ca and resid 45, label=0
set dash_radius, 0.145, ec12
set dash_gap, 0.075, ec12
set dash_length, 0.925, ec12
color green, ec12
dist ec13, chain A and name ca and resid 43, chain A and name ca and resid 30, label=0
set dash_radius, 0.142, ec13
set dash_gap, 0.075, ec13
set dash_length, 0.925, ec13
color green, ec13
dist ec14, chain A and name ca and resid 55, chain A and name ca and resid 43, label=0
set dash_radius, 0.126, ec14
set dash_gap, 0.075, ec14
set dash_length, 0.925, ec14
color green, ec14
dist ec15, chain A and name ca and resid 33, chain A and name ca and resid 12, label=0
set dash_radius, 0.105, ec15
set dash_gap, 0.075, ec15
set dash_length, 0.925, ec15
color green, ec15
dist ec16, chain A and name ca and resid 30, chain A and name ca and resid 23, label=0
set dash_radius, 0.102, ec16
set dash_gap, 0.075, ec16
set dash_length, 0.925, ec16
color green, ec16
dist ec17, chain A and name ca and resid 40, chain A and name ca and resid 12, label=0
set dash_radius, 0.098, ec17
set dash_gap, 0.075, ec17
set dash_length, 0.925, ec17
color green, ec17
dist ec18, chain A and name ca and resid 45, chain A and name ca and resid 5, label=0
set dash_radius, 0.096, ec18
set dash_gap, 0.075, ec18
set dash_length, 0.925, ec18
color green, ec18
dist ec19, chain A and name ca and resid 36, chain A and name ca and resid 13, label=0
set dash_radius, 0.089, ec19
set dash_gap, 0.075, ec19
set dash_length, 0.925, ec19
color green, ec19
dist ec20, chain A and name ca and resid 43, chain A and name ca and resid 5, label=0
set dash_radius, 0.088, ec20
set dash_gap, 0.075, ec20
set dash_length, 0.925, ec20
color green, ec20
dist ec21, chain A and name ca and resid 46, chain A and name ca and resid 20, label=0
set dash_radius, 0.086, ec21
set dash_gap, 0.075, ec21
set dash_length, 0.925, ec21
color green, ec21
dist ec22, chain A and name ca and resid 41, chain A and name ca and resid 33, label=0
set dash_radius, 0.076, ec22
set dash_gap, 0.075, ec22
set dash_length, 0.925, ec22
color green, ec22
dist ec23, chain A and name ca and resid 22, chain A and name ca and resid 9, label=0
set dash_radius, 0.075, ec23
set dash_gap, 0.075, ec23
set dash_length, 0.925, ec23
color green, ec23
dist filt_ec24, chain A and name ca and resid 30, chain A and name ca and resid 14, label=0
set dash_radius, 0.05, filt_ec24
set dash_gap, 0.825, filt_ec24
set dash_length, 0.175, filt_ec24
color grey80, filt_ec24
disable filt_ec24
dist ec25, chain A and name ca and resid 37, chain A and name ca and resid 12, label=0
set dash_radius, 0.066, ec25
set dash_gap, 0.075, ec25
set dash_length, 0.925, ec25
color green, ec25
dist filt_ec26, chain A and name ca and resid 51, chain A and name ca and resid 38, label=0
set dash_radius, 0.05, filt_ec26
set dash_gap, 0.825, filt_ec26
set dash_length, 0.175, filt_ec26
color grey80, filt_ec26
disable filt_ec26
dist ec27, chain A and name ca and resid 55, chain A and name ca and resid 23, label=0
set dash_radius, 0.054, ec27
set dash_gap, 0.075, ec27
set dash_length, 0.925, ec27
color green, ec27
dist ec28, chain A and name ca and resid 51, chain A and name ca and resid 23, label=0
set dash_radius, 0.053, ec28
set dash_gap, 0.075, ec28
set dash_length, 0.925, ec28
color green, ec28
dist ec29, chain A and name ca and resid 23, chain A and name ca and resid 5, label=0
set dash_radius, 0.052, ec29
set dash_gap, 0.075, ec29
set dash_length, 0.925, ec29
color green, ec29
dist ec30, chain A and name ca and resid 40, chain A and name ca and resid 33, label=0
set dash_radius, 0.052, ec30
set dash_gap, 0.075, ec30
set dash_length, 0.925, ec30
color green, ec30
dist ec31, chain A and name ca and resid 44, chain A and name ca and resid 33, label=0
set dash_radius, 0.052, ec31
set dash_gap, 0.075, ec31
set dash_length, 0.925, ec31
color green, ec31
disable ec31
dist ec32, chain A and name ca and resid 34, chain A and name ca and resid 17, label=0
set dash_radius, 0.048, ec32
set dash_gap, 0.075, ec32
set dash_length, 0.925, ec32
color green, ec32
disable ec32
dist ec33, chain A and name ca and resid 36, chain A and name ca and resid 16, label=0
set dash_radius, 0.048, ec33
set dash_gap, 0.075, ec33
set dash_length, 0.925, ec33
color green, ec33
disable ec33
dist ec34, chain A and name ca and resid 31, chain A and name ca and resid 22, label=0
set dash_radius, 0.048, ec34
set dash_gap, 0.075, ec34
set dash_length, 0.925, ec34
color green, ec34
disable ec34
dist ec35, chain A and name ca and resid 30, chain A and name ca and resid 21, label=0
set dash_radius, 0.047, ec35
set dash_gap, 0.075, ec35
set dash_length, 0.925, ec35
color green, ec35
disable ec35
dist filt_ec36, chain A and name ca and resid 14, chain A and name ca and resid 5, label=0
set dash_radius, 0.05, filt_ec36
set dash_gap, 0.825, filt_ec36
set dash_length, 0.175, filt_ec36
color grey80, filt_ec36
disable filt_ec36
dist filt_ec37, chain A and name ca and resid 51, chain A and name ca and resid 14, label=0
set dash_radius, 0.05, filt_ec37
set dash_gap, 0.825, filt_ec37
set dash_length, 0.175, filt_ec37
color grey80, filt_ec37
disable filt_ec37
dist filt_ec38, chain A and name ca and resid 38, chain A and name ca and resid 30, label=0
set dash_radius, 0.05, filt_ec38
set dash_gap, 0.825, filt_ec38
set dash_length, 0.175, filt_ec38
color grey80, filt_ec38
disable filt_ec38
dist ec39, chain A and name ca and resid 30, chain A and name ca and resid 24, label=0
set dash_radius, 0.045, ec39
set dash_gap, 0.075, ec39
set dash_length, 0.925, ec39
color green, ec39
disable ec39
dist ec40, chain A and name ca and resid 44, chain A and name ca and resid 12, label=0
set dash_radius, 0.043, ec40
set dash_gap, 0.075, ec40
set dash_length, 0.925, ec40
color green, ec40
disable ec40
dist ec41, chain A and name ca and resid 42, chain A and name ca and resid 4, label=0
set dash_radius, 0.042, ec41
set dash_gap, 0.075, ec41
set dash_length, 0.925, ec41
color green, ec41
disable ec41
dist ec42, chain A and name ca and resid 42, chain A and name ca and resid 7, label=0
set dash_radius, 0.042, ec42
set dash_gap, 0.075, ec42
set dash_length, 0.925, ec42
color green, ec42
disable ec42
dist ec43, chain A and name ca and resid 36, chain A and name ca and resid 17, label=0
set dash_radius, 0.042, ec43
set dash_gap, 0.075, ec43
set dash_length, 0.925, ec43
color green, ec43
disable ec43
dist ec44, chain A and name ca and resid 34, chain A and name ca and resid 19, label=0
set dash_radius, 0.041, ec44
set dash_gap, 0.075, ec44
set dash_length, 0.925, ec44
color green, ec44
disable ec44
dist ec45, chain A and name ca and resid 36, chain A and name ca and resid 15, label=0
set dash_radius, 0.040, ec45
set dash_gap, 0.075, ec45
set dash_length, 0.925, ec45
color green, ec45
disable ec45
dist ec46, chain A and name ca and resid 51, chain A and name ca and resid 21, label=0
set dash_radius, 0.040, ec46
set dash_gap, 0.075, ec46
set dash_length, 0.925, ec46
color green, ec46
disable ec46
dist ec47, chain A and name ca and resid 39, chain A and name ca and resid 13, label=0
set dash_radius, 0.039, ec47
set dash_gap, 0.075, ec47
set dash_length, 0.925, ec47
color green, ec47
disable ec47
dist ec48, chain A and name ca and resid 41, chain A and name ca and resid 35, label=0
set dash_radius, 0.038, ec48
set dash_gap, 0.075, ec48
set dash_length, 0.925, ec48
color green, ec48
disable ec48
dist ec49, chain A and name ca and resid 43, chain A and name ca and resid 21, label=0
set dash_radius, 0.037, ec49
set dash_gap, 0.075, ec49
set dash_length, 0.925, ec49
color green, ec49
disable ec49
dist ec50, chain A and name ca and resid 41, chain A and name ca and resid 7, label=0
set dash_radius, 0.037, ec50
set dash_gap, 0.075, ec50
set dash_length, 0.925, ec50
color green, ec50
disable ec50
dist ec51, chain A and name ca and resid 33, chain A and name ca and resid 20, label=0
set dash_radius, 0.036, ec51
set dash_gap, 0.075, ec51
set dash_length, 0.925, ec51
color green, ec51
disable ec51
dist ec52, chain A and name ca and resid 34, chain A and name ca and resid 18, label=0
set dash_radius, 0.036, ec52
set dash_gap, 0.075, ec52
set dash_length, 0.925, ec52
color green, ec52
disable ec52
dist ec53, chain A and name ca and resid 48, chain A and name ca and resid 21, label=0
set dash_radius, 0.035, ec53
set dash_gap, 0.075, ec53
set dash_length, 0.925, ec53
color green, ec53
disable ec53
dist ec54, chain A and name ca and resid 41, chain A and name ca and resid 12, label=0
set dash_radius, 0.035, ec54
set dash_gap, 0.075, ec54
set dash_length, 0.925, ec54
color green, ec54
disable ec54
dist ec55, chain A and name ca and resid 41, chain A and name ca and resid 22, label=0
set dash_radius, 0.034, ec55
set dash_gap, 0.075, ec55
set dash_length, 0.925, ec55
color pink, ec55
disable ec55
dist ec56, chain A and name ca and resid 35, chain A and name ca and resid 12, label=0
set dash_radius, 0.034, ec56
set dash_gap, 0.075, ec56
set dash_length, 0.925, ec56
color green, ec56
disable ec56
dist ec57, chain A and name ca and resid 34, chain A and name ca and resid 16, label=0
set dash_radius, 0.034, ec57
set dash_gap, 0.075, ec57
set dash_length, 0.925, ec57
color green, ec57
disable ec57
dist ec58, chain A and name ca and resid 54, chain A and name ca and resid 4, label=0
set dash_radius, 0.034, ec58
set dash_gap, 0.075, ec58
set dash_length, 0.925, ec58
color green, ec58
disable ec58
dist ec59, chain A and name ca and resid 40, chain A and name ca and resid 10, label=0
set dash_radius, 0.033, ec59
set dash_gap, 0.075, ec59
set dash_length, 0.925, ec59
color green, ec59
disable ec59
dist ec60, chain A and name ca and resid 36, chain A and name ca and resid 18, label=0
set dash_radius, 0.032, ec60
set dash_gap, 0.075, ec60
set dash_length, 0.925, ec60
color green, ec60
disable ec60
dist ec61, chain A and name ca and resid 45, chain A and name ca and resid 7, label=0
set dash_radius, 0.031, ec61
set dash_gap, 0.075, ec61
set dash_length, 0.925, ec61
color green, ec61
disable ec61
dist ec62, chain A and name ca and resid 44, chain A and name ca and resid 35, label=0
set dash_radius, 0.031, ec62
set dash_gap, 0.075, ec62
set dash_length, 0.925, ec62
color green, ec62
disable ec62
dist ec63, chain A and name ca and resid 32, chain A and name ca and resid 19, label=0
set dash_radius, 0.031, ec63
set dash_gap, 0.075, ec63
set dash_length, 0.925, ec63
color green, ec63
disable ec63
dist ec64, chain A and name ca and resid 25, chain A and name ca and resid 6, label=0
set dash_radius, 0.031, ec64
set dash_gap, 0.075, ec64
set dash_length, 0.925, ec64
color green, ec64
disable ec64
dist ec65, chain A and name ca and resid 24, chain A and name ca and resid 5, label=0
set dash_radius, 0.031, ec65
set dash_gap, 0.075, ec65
set dash_length, 0.925, ec65
color green, ec65
disable ec65
dist ec66, chain A and name ca and resid 51, chain A and name ca and resid 4, label=0
set dash_radius, 0.031, ec66
set dash_gap, 0.075, ec66
set dash_length, 0.925, ec66
color green, ec66
disable ec66
dist ec67, chain A and name ca and resid 36, chain A and name ca and resid 14, label=0
set dash_radius, 0.031, ec67
set dash_gap, 0.075, ec67
set dash_length, 0.925, ec67
color green, ec67
disable ec67
dist ec68, chain A and name ca and resid 37, chain A and name ca and resid 14, label=0
set dash_radius, 0.030, ec68
set dash_gap, 0.075, ec68
set dash_length, 0.925, ec68
color green, ec68
disable ec68
dist ec69, chain A and name ca and resid 45, chain A and name ca and resid 38, label=0
set dash_radius, 0.030, ec69
set dash_gap, 0.075, ec69
set dash_length, 0.925, ec69
color red, ec69
disable ec69
dist ec70, chain A and name ca and resid 36, chain A and name ca and resid 11, label=0
set dash_radius, 0.030, ec70
set dash_gap, 0.075, ec70
set dash_length, 0.925, ec70
color green, ec70
disable ec70
dist ec71, chain A and name ca and resid 30, chain A and name ca and resid 4, label=0
set dash_radius, 0.029, ec71
set dash_gap, 0.075, ec71
set dash_length, 0.925, ec71
color pink, ec71
disable ec71
hide lines
recolor
