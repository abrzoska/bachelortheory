#!/bin/bash
#Called from C++ to calculate the SVD of the correlation matrix
cd ../redsvd
redsvd -i ../build/correlation.mat -o svd
exit 0
