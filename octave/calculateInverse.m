function r = calculateInverse()
  format short e
  load ../build/correlation.mat
%    cond(correlation)
    c = pinv(correlation);
    save inverse.mat c
%    cond(c)s
endfunction
