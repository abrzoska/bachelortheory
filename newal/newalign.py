from Bio import SeqIO
from sys import argv
input_file = argv[1]

def iterateOverSequences(sequenceA, sequenceB):
    if len(sequenceA) == len(sequenceB):
        for i in range(len(sequenceA)):
            yield sequenceA[i], sequenceB[i], i
    else:
        return

fasta_sequences = SeqIO.parse(open(input_file),'fasta')

counter = 1

for fasta in fasta_sequences:
    name, sequence = fasta.id, fasta.seq.tostring()
    print fasta.seq
    if counter == 1:
      refSequence = fasta.seq 
      counter= counter +1
    else:
      sequence = fasta.seq 
    
    
print "Alignment Sequence: " ,refSequence 
print "PDB Sequence: ",sequence 

# map positions and output deviations between ref and pdb
pdbToRefMapping = {}
indexSeqA = 0
indexSeqB = 0
firstCharacterInSeqB = False


for seqA, seqB, indexSeqAligned in iterateOverSequences(refSequence,sequence):
	if seqA != '-':
		if seqB != '-':
			pdbToRefMapping[indexSeqA] = indexSeqB
			indexSeqB += 1
		indexSeqA += 1
        else:
		indexSeqB += 1
        if not firstCharacterInSeqB and seqB != '-':
            firstCharacterInSeqB = True
            
#for i in pdbToRefMapping:
#	print str(pdbToRefMapping[i]) + '  ' + str(i) +' '+ str(pdbToRefMapping[i] - i)+'\n' 
outputFile = open("newal/indexMapping.txt","w");
#for i in pdbToRefMapping:
#	outputFile.write(str(i))
#outputFile.write("\n")
for i in pdbToRefMapping:
	outputFile.write(str(pdbToRefMapping[i])+ "\t")  
outputFile.write("\n") 
for i in pdbToRefMapping:
	outputFile.write(str(pdbToRefMapping[i] - i)+ "\t") 
outputFile.close()



