#!/bin/bash
IN=$1
OUT="$1-processed"
echo $OUT
#add nr=2 to add the sequence automatically
awk '{
       if(NR == 1) {
           shift = $1
       }
       print ($1 - shift) " " $2 " " ($3 - shift) " " $4 " " $6  
}' $IN  | sort -nk5 -r > $OUT
